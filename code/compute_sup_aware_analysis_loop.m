%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

function []=compute_sup_aware_analysis_loop(topicSet)
    if nargin==0
        topicSet=[];
    end
    common_parameters
    trackID=EXPERIMENT.fast.trackID;
    gap = EXPERIMENT.taxonomy.sup.gap.list;
    weight = EXPERIMENT.taxonomy.sup.weight.list;
    
    name = @(g,w) ...
        sprintf('aw_sup_mono_%s_%s', g, w);
    parpool('local',3)
    for gp = 1:length(gap)
        parfor w=1:length(weight)
            tag = name(gap{gp},weight{w});
            analyse_aware_supervised_measures_set(trackID, tag, topicSet);
        end
    end
end
