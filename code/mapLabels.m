%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



function [mapped] =mapLabels(toMap)
    old={'sgl_fro_msd';'sgl_rmse_msd';'sgl_apc_msd';'sgl_tau_msd';'sgl_kld_msd';...
         'tpc_fro_msd';'tpc_rmse_msd';'tpc_apc_msd';'tpc_tau_msd';'tpc_kld_msd';...
         'sup_fro_d';'sup_rmse_d';'sup_apc_d';'sup_tau_d';'sup_kld_d';...
         'sup_fro_sd';'sup_rmse_sd';'sup_apc_sd';'sup_tau_sd';'sup_kld_sd';...
         'sup_fro_cd';'sup_rmse_cd';'sup_apc_cd';'sup_tau_cd';'sup_kld_cd';
         'uni';'mv';'emmv';'emsemi';'emsemi2';'emzhu'};
     new={'unsup_fro_sgl';'unsup_rmse_sgl';'unsup_apc_sgl';'unsup_tau_sgl';'unsup_kld_sgl';...
         'unsup_fro_tpc';'unsup_rmse_tpc';'unsup_apc_tpc';'unsup_tau_tpc';'unsup_kld_tpc';...
         'sup_fro';'sup_rmse';'sup_apc';'sup_tau';'sup_kld';...
         'sup_fro_squared';'sup_rmse_squared';'sup_apc_squared';'sup_tau_squared';'sup_kld_squared';...
         'sup_fro_cubed';'sup_rmse_cubed';'sup_apc_cubed';'sup_tau_cubed';'sup_kld_cubed';
         'uniform';'mv';'emmv';'emsemi_unfair';'emsemi';'emGZ'};
    mapped=cell(1,size(toMap,2));
    for i=1:size(toMap,2)
        %disp(toMap{i})
        pos=strcmp(old,toMap{i});
        pos=find(pos==1);
        %disp(pos)
        %disp(new{pos})
        mapped{i}=new{pos};
    end
    
end