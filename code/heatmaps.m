%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



fprintf('##### computing heatmap table #####\n');
% setup common parameters
common_parameters;
trackID=EXPERIMENT.fast.trackID;
% local version of the general configuration parameters
shortTrackID = EXPERIMENT.(trackID).shortID;
originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
filename='';
fprintf('+ Loading analyses\n');

% total number of elements in the list
APCorrelations=cell(EXPERIMENT.kuples.number,EXPERIMENT.taxonomy.approaches,EXPERIMENT.measure.number,originalTrackNumber);
currentApproach=1;
appr=cell(1,EXPERIMENT.taxonomy.approaches);
% for each runset
for r = 1:originalTrackNumber
    
    originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    filename=strcat(filename,originalTrackShortID{r});
    
    fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});

    for m = 1:EXPERIMENT.measure.number

        fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});

        for k = 1:EXPERIMENT.kuples.number

            fprintf('      # k-uple: %s \n', EXPERIMENT.pattern.identifier.kuple(k));
            for w=1:EXPERIMENT.taxonomy.sup.weight.number
                for gp = 1:EXPERIMENT.taxonomy.sup.gap.number

                    e = EXPERIMENT.taxonomy.sup.tag('aw', 'sup', 'mono', EXPERIMENT.taxonomy.sup.gap.list{gp},EXPERIMENT.taxonomy.sup.weight.list{w});

                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},e, EXPERIMENT.pattern.identifier.kuple(k), ...
                        trackID,  originalTrackShortID{r},[]);

                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                        trackID,  originalTrackShortID{r},[]);

                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                        trackID,  originalTrackShortID{r},[]);

                    serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID, []), ...
                        {apcID, 'apcTmp';
                        rmseID, 'rmseTmp'});
                    APCorrelations{k,currentApproach,m,r}=apcTmp.mean;

                    appr{currentApproach}=mapLabels({EXPERIMENT.tag.(e).matlab.label});
                    currentApproach=currentApproach+1;
                    clear apcTmp rmseTmp
                end    
            end % supervised tags
            for g = 1:EXPERIMENT.taxonomy.us.granularity.number
                for a = 1:EXPERIMENT.taxonomy.us.aggregation.number
                    for gp = 1:EXPERIMENT.taxonomy.us.gap.number
                        for w = 1:EXPERIMENT.taxonomy.us.weight.number

                            e = EXPERIMENT.taxonomy.us.tag('aw', 'us', 'mono', ...
                                EXPERIMENT.taxonomy.us.granularity.list{g}, ...
                                EXPERIMENT.taxonomy.us.aggregation.list{a}, ...
                                EXPERIMENT.taxonomy.us.gap.list{gp}, ...
                                EXPERIMENT.taxonomy.us.weight.list{w});

                            measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                trackID,  originalTrackShortID{r},[]);

                            apcID = trunc(EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                trackID,  originalTrackShortID{r},[]));

                            rmseID = trunc(EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                trackID,  originalTrackShortID{r},[]));

                            serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID, []), ...
                                {apcID, 'apcTmp';
                                rmseID, 'rmseTmp'});
                            APCorrelations{k,currentApproach,m,r}=apcTmp.mean;
                            appr{currentApproach}=mapLabels({EXPERIMENT.tag.(e).matlab.label});
                            currentApproach=currentApproach+1;
                            clear apcTmp rmseTmp

                        end % weight
                    end % gap
                end % aggregation
            end % granularity

            %supervised tags
            % add the baselines
            for e = 1:EXPERIMENT.taxonomy.baseline.number


                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                    trackID,  originalTrackShortID{r},[]);

                apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                    trackID,  originalTrackShortID{r},[]);

                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                    trackID,  originalTrackShortID{r},[]);


                serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).relativePath,  EXPERIMENT.taxonomy.baseline.list{e}, measureID,[]), ...
                    {apcID, 'apcTmp';
                    rmseID, 'rmseTmp'});
                APCorrelations{k,currentApproach,m,r}=apcTmp.mean;
                appr{currentApproach}=mapLabels({EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).matlab.label});
                currentApproach=currentApproach+1;
                clear apcTmp rmseTmp;

            end % additional tags
            currentApproach=1;
        end % kuple
    end % measures
end % for each runset

d=cell2mat(APCorrelations(:,:,1,1));
%disp(d)
mi=0.30;%min(min(d));
%disp(mi)
ma=0.80;%max(max(d));
%disp(ma);

filename=strcat(filename,'_',EXPERIMENT.fast.assessorsType);
[path,~]=fileparts(EXPERIMENT.pattern.file.heatmap(trackID, filename));
        if (exist (path)~=7)
            mkdir(path)
        end
fid = fopen(EXPERIMENT.pattern.file.heatmap(trackID, filename), 'w');

fprintf(fid, '\\documentclass[11pt]{article} \n\n');

fprintf(fid, '\\usepackage{amsmath}\n');
fprintf(fid, '\\usepackage{multirow}\n');
fprintf(fid, '\\usepackage{longtable}\n');
fprintf(fid, '\\usepackage{colortbl}\n');
fprintf(fid, '\\usepackage{lscape}\n');
fprintf(fid, '\\usepackage{pdflscape}\n');
fprintf(fid, '\\usepackage{rotating}\n');
fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
fprintf(fid, '\\usepackage{xcolor}\n');
fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
fprintf(fid, '\\definecolor{blue}{RGB}{100, 150, 255}\n');
fprintf(fid, '\\definecolor{green}{RGB}{100,210,100}\n');
fprintf(fid, '\\definecolor{orange}{RGB}{255,150,0}\n');
fprintf(fid, '\\renewcommand{\\tabcolsep}{1pt} \n');
fprintf(fid, '\\begin{document}\n\n');
fprintf(fid, '\\begin{footnotesize} \n \\begin{tabular}{|c|c|');
for i=1:EXPERIMENT.taxonomy.approaches
    fprintf(fid, 'c|');
end
fprintf(fid, '} \n \\hline \n');
fprintf(fid, '&');
for j=1:EXPERIMENT.taxonomy.approaches
    fprintf(fid, strcat('&\\rotatebox{90}{',strrep(char(appr{1,j}),'_','\\_'),'}'));
end
fprintf(fid, ' \\\\\n \\hline \n');
for m=1:EXPERIMENT.measure.number
    for r=1:originalTrackNumber
        fprintf(fid,'\\multirow{6}{*}{\\rotatebox{90}{%s}}',strcat(EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r},'-',EXPERIMENT.fast.assessorsType));
        d=cell2mat(APCorrelations(:,:,m,r));
        for i=1:EXPERIMENT.kuples.number
        fprintf(fid, '&k%02d',i+1);
        sup=EXPERIMENT.taxonomy.sup.gap.number*EXPERIMENT.taxonomy.sup.weight.number;
        for j=1:sup
            if(d(i,j)<mi)
                color=0;
            elseif (d(i,j)>ma)
                color=100;
            else
                color=(d(i,j)-mi)*100/(ma-mi);
            end
            if(d(i,j)==max(d(i,:)))
                fprintf(fid, '&\\textbf{\\cellcolor{orange! %3.2f} %5.4f}',color,d(i,j));
            else
                fprintf(fid, '&\\cellcolor{orange! %3.2f} %5.4f',color,d(i,j));
            end
        end
        uns=EXPERIMENT.taxonomy.us.granularity.number*EXPERIMENT.taxonomy.us.aggregation.number*EXPERIMENT.taxonomy.us.gap.number*EXPERIMENT.taxonomy.us.weight.number;
        for j=sup+1:uns+sup+1
            if(d(i,j)<mi)
                color=0;
            elseif (d(i,j)>ma)
                color=100;
            else
                color=(d(i,j)-mi)*100/(ma-mi);
            end
            if(d(i,j)==max(d(i,:)))
                fprintf(fid, '&\\textbf{\\cellcolor{green! %3.2f} %5.4f}',color,d(i,j));
            else
                fprintf(fid, '&\\cellcolor{green! %3.2f} %5.4f',color,d(i,j));
            end
        end
        
        for j=uns+sup+2:EXPERIMENT.taxonomy.approaches
            if(d(i,j)<mi)
                color=0;
            elseif (d(i,j)>ma)
                color=100;
            else
                color=(d(i,j)-mi)*100/(ma-mi);
            end
            if(d(i,j)==max(d(i,:)))
                fprintf(fid, '&\\textbf{\\cellcolor{blue! %3.2f} %5.4f}',color,d(i,j));
            else
                fprintf(fid, '&\\cellcolor{blue! %3.2f} %5.4f',color,d(i,j));
            end
        end
        fprintf(fid, '\\\\ \n');
        end 
        fprintf(fid, '\\hline \n');
    end
end
fprintf(fid, '\\end{tabular} \n \\end{footnotesize}');
fprintf(fid, '\\end{document} \n\n');
fclose(fid);
fprintf('##### heatmap saved in %s #####\n',strcat('heatmap_',filename,'.tex'));