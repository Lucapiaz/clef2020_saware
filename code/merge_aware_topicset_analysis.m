%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



function []=merge_aware_topicset_analysis
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    nass=EXPERIMENT.fast.assessors;
    shortTrack=EXPERIMENT.(trackID).shortID;
    startComputation = tic;
    fprintf('\n\n######## Merging unsupervised AWARE topicset analysis on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    serload(EXPERIMENT.pattern.file.topicset(trackID,shortTrack));
    ntopicset=EXPERIMENT.topicset.number;
    %disp(topicSets);
    granularity = EXPERIMENT.taxonomy.us.granularity.list;
    aggregation = EXPERIMENT.taxonomy.us.aggregation.list;
    gap = EXPERIMENT.taxonomy.us.gap.list;
    weight = EXPERIMENT.taxonomy.us.weight.list;

    name = @(granularity, aggregation, gap, weight, left_rnd, right_rnd) ...
            sprintf('aw_us_mono_%s_%s_%s_%s', granularity, aggregation, gap, weight);

    for grt = 1:length(granularity)
        for agg = 1:length(aggregation)
            for gp = 1:length(gap)
                for w = 1:length(weight)

                    tag = name(granularity{grt}, aggregation{agg}, gap{gp}, weight{w});
                    fprintf('Merging %s \n', name(granularity{grt}, aggregation{agg}, gap{gp}, weight{w}));
                    for m = 1:EXPERIMENT.measure.number
                    
                        mid=EXPERIMENT.measure.id{m};
                        fprintf('  - %s\n',mid);
                        for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number

                            shortRunset=EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
                            fprintf('    - %s\n',shortRunset);
                            for k = 1:EXPERIMENT.kuples.number
                                currentKuple=EXPERIMENT.pattern.identifier.kuple(k);
                                fprintf('      %s:',currentKuple);
                                taut=NaN(ntopicset,min(EXPERIMENT.kuples.samples,nchoosek(nass, EXPERIMENT.kuples.sizes(k))));
                                rmset=NaN(ntopicset,min(EXPERIMENT.kuples.samples,nchoosek(nass, EXPERIMENT.kuples.sizes(k))));
                                apct=NaN(ntopicset,min(EXPERIMENT.kuples.samples,nchoosek(nass, EXPERIMENT.kuples.sizes(k))));


                                for t=1:ntopicset
                                    measureID = EXPERIMENT.pattern.identifier.measure(mid, tag,currentKuple , trackID,  shortRunset,topicSets(t,:));
                                    tauID = trunc(EXPERIMENT.pattern.identifier.pm('tau', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSets(t,:)));   
                                    apcID = trunc(EXPERIMENT.pattern.identifier.pm('apc', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSets(t,:)));              
                                    rmseID = trunc(EXPERIMENT.pattern.identifier.pm('rmse', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSets(t,:)));
                                    fprintf('.');
                                    serload2(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID,topicSets(t,:)),'FileVarNames',{apcID,tauID,rmseID},'WorkspaceVarNames',{'apcTemp','tauTemp','rmseTemp'});
                                    apct(t,:)=apcTemp.data;
                                    rmset(t,:)=rmseTemp.data;
                                    taut(t,:)=tauTemp.data;
                                    clear tauTemp apcTemp rmseTemp tauID apcID rmseID measureID;
                                end
                                
                                tau.data = nanmean(taut,1);
                                tau.mean = nanmean(tau.data);
                                tau.ci = confidenceIntervalDelta(tau.data, 0.05, 2);
                                tau.max = max(tau.data);
                                tau.min = min(tau.data);

                                apc.data = nanmean(apct,1);
                                apc.mean = nanmean(apc.data);
                                apc.ci = confidenceIntervalDelta(apc.data, 0.05, 2);
                                apc.max = max(apc.data);
                                apc.min = min(apc.data);

                                rmse.data = nanmean(rmset,1);
                                rmse.mean = nanmean(rmse.data);
                                rmse.ci = confidenceIntervalDelta(rmse.data, 0.05, 2);
                                rmse.max = max(rmse.data);
                                rmse.min = min(rmse.data);

                                measureIDtot = EXPERIMENT.pattern.identifier.measure(mid, tag,currentKuple , trackID,  shortRunset,[]);
                                tauIDtot = EXPERIMENT.pattern.identifier.pm('tau', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,[]);   
                                apcIDtot = EXPERIMENT.pattern.identifier.pm('apc', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,[]);              
                                rmseIDtot = EXPERIMENT.pattern.identifier.pm('rmse', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,[]);

                                [path,~]=fileparts(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureIDtot,[]));
                                if (exist (path)~=7)
                                    mkdir(path)
                                end 

                                sersave2(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureIDtot,[]),'FileVarNames',{apcIDtot,tauIDtot,rmseIDtot},'WorkspaceVarNames',{'apc','tau','rmse'});
                                fprintf('saved\n');
                                clear measureIDtot tauIDtot apcIDtot rmseIDtot tau apc rmse;
                            end

                            clear shortRunset;
                        end

                        clear mid;
                    end
                clear tag;
                end
            end
        end
    end
    fprintf('\n\n######## Total elapsed time for merging AWARE topicset analysis on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    clear;
    
end






