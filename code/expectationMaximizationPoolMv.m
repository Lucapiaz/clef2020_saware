%% expectationMaximizationPoolMv
% 
% Generates a pool from input ones by using an expectation maximization
% algorithm seeded by MV pools

%% Synopsis
%
%   [emPool, accuracy] = expectationMaximizationPoolMv(tolerance, maxIterations, identifier, varargin)
%  
%
%   It assumes binary relevance, i.e. only NotRelevant and Relevant degrees
%   are allowed.
%
%
% *Parameters*
%
% * *|tolerance|* tolerance to decide when the Expectation Maximization
% converges;
% * *|maxIterations|* maximum number of iterations;
% * *|identifier|* - the identifier to be assigned to the expectation
% maximization pool;
% * *|varargin|* - the pools to be merged into the expectation maximization 
% one.
%
% *Returns*
%
% * *|emPool|* - the expectation maximization pool.
% * *|accuracy|* - the accuracy of the assessors.

%% References
% 
% Please refer to:
%
% * Hosseini, M., Cox, I. J., MilicFrayling, N., Kazai, G., and Vinay, V. (2012).
% On Aggregating Labels from Multiple Crowd Workers to Infer Relevance of Documents.
% In Baeza-Yaetes, R., de Vries, A. P., Zaragoza, H., Cambazoglu, B. B., Murdock,
% V., Lempel, R., and Silvestri, F., editors, 
% _Advances in Information Retrieval. Proc. 32nd European Conference on IR Research (ECIR 2012)_, 
% pages 182-194. Lecture Notes in Computer Science (LNCS) 7224, Springer, Heidelberg, Germany.
% 
%% Information
% 
% * *Author*: <mailto:maistro@dei.unipd.it Maria Maistro>,
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [emPool, accuracy] = expectationMaximizationPoolMv(tolerance, maxIterations, identifier, varargin)

    % check that tolerance is a nonempty "real" scalar greater than 0 
    validateattributes(tolerance, {'numeric'}, ...
        {'nonempty', 'real', '>', 0}, '', 'tolerance');

    % check that the maximum number of iterations is a nonempty  
    % integer value greater than 0
    validateattributes(maxIterations, {'numeric'}, ...
            {'nonempty', 'integer', '>', 0}, '', 'maxIterations');

    % check that identifier is a non-empty string
    validateattributes(identifier,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'Identifier');

     if iscell(identifier)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(identifier) && numel(identifier) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    identifier = char(strtrim(identifier));
    identifier = identifier(:).';

    % check that the identifier is ok according to the matlab rules
    if ~isempty(regexp(identifier, '(^[0-9_])?\W*', 'once'))
        error('MATTERS:IllegalArgument', 'Identifier %s is not valid: identifiers can contain only letters, numbers, and the underscore character and they must start with a letter.', ...
            identifier);
    end  

    % number of pools to be merged, i.e. number of workers
    % P = length(varargin);
    M = length(varargin);

    % number of topics
    T = height(varargin{1});

    % use one of the input pools as a template for the expectation maximization one
    emPool = varargin{1};
    emPool.Properties.UserData.identifier = identifier;
    emPool.Properties.VariableNames = {identifier};

    
    % initialize the cell array that contains the accuracies
    accuracy = varargin{1};
    accuracy.Properties.UserData.identifier = identifier;
    accuracy.Properties.VariableNames = {identifier};
    
    for t = 1:T
        
        currentTopic = emPool{t, 1}{1, 1};
        
        % merge the documents of all the pools
        % for p = 2:P
        for j = 2:M
            
            % check that all the input pools have the same number of topics
            assert(height(varargin{j}) == T, 'All the input pools must have the same number of topics.');
            
            % merge the documents. 
            % N.B. if a document has different relevance degrees in
            % different pools, it will appear more than once since the
            % pairs (documents, relevance degree) are different.
            currentTopic = union(currentTopic, varargin{j}{t, 1}{1, 1}, 'rows');
            
        end; % for each pool
        
        % remove duplicate documents due to union of the same document with
        % different relevance degrees
        [~, ia] = unique(currentTopic(:, 1));        
        currentTopic = currentTopic(ia, :);
        
        % a matrix DxP where each row is a document in the merged pool and
        % each column is the relevance degree (0 for NotRelevant, 1 for
        % Relevant) in one of the input pools
        em = NaN(height(currentTopic), M);
                
        % Number of documents
        N = height(currentTopic);        
                
        % Number of relevance degrees, not relevant are not considered
        G = 1;        
        
        % n (G+1)xNxM matrix, n_gij = 1 if the worker j labels the document 
        % i as g
        n = zeros(G+1, N, M);
        
        % collect the data about the pools
        for j = 1:M
            
            % find where the documents of the p-th pool are in the
            % currentTopic (locb)
            [lia, locb] = ismember(varargin{j}{t, 1}{1, 1}{:, 1}, currentTopic{:, 1});
            
            % find the relevant documents of the p-th pool
            rel = varargin{j}{t, 1}{1, 1}.RelevanceDegree == 'Relevant';
            
            % set rows of the current topic corresponding to the documents
            % of the p-th pool to 0 for NotRelevant and 1 for Relevant 
            em(locb, j) = double(rel(lia));
            
            % definition of the matrix n, only for two relevance grades
            n(1, :, j) = ones(1, N) - em(:, j).';
            n(2, :, j) = em(:, j).';            
                        
        end; % for each pool
        
        em(isnan(em)) = 0;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Mv = zeros(G+1, N);
        
        % compute the number of votes for each relevance degree for each
        % document
        em = histc(em.', 0:1).';
        
        % find where not relevant wins
        v = (em(:, 1) > em(:, 2));
        Mv(1, v) = 1;
                   
        % find where relevant wins
        v = (em(:, 1) < em(:, 2));
        Mv(2, v) = 1;
                
        % find the positions where relevant and not relevant tie
        v = find(em(:, 1) == em(:, 2));
        
        % generate random relevant and not relevant assessments for the
        % tied documents
        tmp = randi(2, size(v, 1), 1) - 1;
        
        % find where the relevant documents are
        rel = (tmp == 1);
                
        % assign relevant and not relevant degrees to the tied documents
        Mv(2, v(rel)) = 1;
        Mv(1, v(~rel)) = 1;        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % substitute NaN values with zeros, because if the worker j does 
        % not judge the document i n(:, i, j) = 0
        n(isnan(n)) = 0;
        
        % Permute the dimensions of n for coputations in while loop
        nPerm = permute(n, [3 1 2]);
        
        % NaN indicates that some pool did not contain a vote for a given
        % document
        if sum(sum(isnan(em)))
            fprintf('There are documents without votes for topic %s\n', emPool.Properties.RowNames{t});
        end;
        
        % STEP 1
        % Generate random relevance judgements
        % rowIndex = randi(G + 1, 1, N);
        % R = zeros(G+1, N);
        % linearInd = sub2ind(size(R), rowIndex, 1:N);
        % R (G+1)xN matrix of all the relevance vectors, where R_ik = 1 if 
        % the relevance of document i is k
        % R(linearInd) = ones(1, N);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        R = Mv;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % EM Algorithm
        iteration = 0;
        convergence = 100;
        while ( (convergence > tolerance) && (iteration < maxIterations) )
            iteration = iteration + 1;
    
            % STEP 2
            % Estimate P and p
            % P (G+1)x(G+1)xM matrix, P_klj is the probability that the 
            % worker j provides a label l given that k is the true 
            % relevance label            
            P = zeros(G + 1, G + 1, M);
            for j = 1 : M
                for k = 1:G+1
                    P(k, :, j) = R(k, :) * n(:, :, j).';
                    if (sum(P(k, :, j)) ~= 0)
                        P(k, :, j) = P(k, :, j) ./ sum(P(k, :, j));
                    end
                end
            end
            % p 1x(G+1) vector, p_k is the probability that a document 
            % drawn at random has a true relevance grade of k
            p = (sum(R, 2) ./ N).';
    
            % STEP 3
            % New estimate of R based on P and p
            oldR = R;
            PPerm = permute(P, [3 2 1]);
            for i = 1:N
                for g = 1:G+1
                    R(g, i) = p(g) * prod(prod(PPerm(:, :, g) .^ nPerm(:, :, i)));
                end
            end
            % Normalize the rows of R
            R = bsxfun(@rdivide, R, sum(R));
            % Compute the difference with the ,atrix computed at the
            % previous step
            absoluteErrorr = abs(oldR - R);
            % Set the convergenge parameter to the maximal error
            convergence = max(absoluteErrorr(:));
        end % end of while

       % fprintf('Topic: %d\n', t);
       % fprintf('Number of iterations: %d\n', iteration);
       % fprintf('Convergence parameter: %g\n', convergence);
        if (iteration >= maxIterations)
            fprintf('While loop terminates before reaching the tolerance threshold for topic %s\n', emPool.Properties.RowNames{t});
            fprintf('Convergence parameter: %g\n', convergence);
        end;
            

        % STEP 5
        % for each document i, set R_gi = 1 for the g with the maximum 
        % probability
        [~, I] = max(R, [], 1);
        idx = sub2ind(size(R), I, 1:N);
        
        % Note that if there is a document i such that R_0i = R_1i = 0.5 
        % the documents is cosidered of relevance 0 (not relevant), indeed
        % max(R, [], 1) returns the index of the first maximal entry
        
        % Relevance Judgements obtained with the EM Methods
        RelevanceJudgements = zeros(G + 1, N);
        RelevanceJudgements(idx) = ones(1, N);        
        
        % Compute the accuracies
        idx = cumsum([1:(G + 2):(G + 1)^2; (G + 1)^2.*ones(M - 1, G + 1)]);
        % A 1xM accuracy vector, a_j is the accuracy of the worker j
        A = sum(P(idx')) ./ reshape(sum(sum(P)), 1, M);
           
        % find not relevant documents
        v = logical(RelevanceJudgements(1, :));
        currentTopic{v, 'RelevanceDegree'} = {'NotRelevant'};     
           
        % find relevant documents
        v = logical(RelevanceJudgements(2, :));
        currentTopic{v, 'RelevanceDegree'} = {'Relevant'};
                
        % substitute the current topic into the majority vote pool
        emPool{t, 1}{1, 1} = currentTopic;
        
        % store the accuracy weights
        accuracy{t, 1}{1, 1} = A;
        
    end; % for each topic

end

