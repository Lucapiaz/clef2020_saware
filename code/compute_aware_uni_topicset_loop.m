%% compute_aware_uni_topicset_loop
% 
% Computes aware uni loop (measures and analyses) for each topicset
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



function []=compute_aware_uni_topicset_loop(startTopicset, endTopicset)
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    serload(EXPERIMENT.pattern.file.topicset(EXPERIMENT.(trackID).id, EXPERIMENT.(trackID).shortID));
    serload(EXPERIMENT.pattern.file.dataset(EXPERIMENT.(trackID).id, EXPERIMENT.(trackID).shortID), 'T');
    for t= startTopicset:endTopicset

        currentTopicset=topicSets(t,:);
        fprintf('##### computing uni loop for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));

        fprintf('# computing aware uni loop for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
        compute_aware_unsupervised_mono_measures(trackID,'aw_uni',currentTopicset);
        fprintf('# computing aware uni analysis loop for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
        analyse_aware_unsupervised_measures_set(trackID,'aw_uni',currentTopicset);

    end
end