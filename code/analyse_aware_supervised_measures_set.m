%% analyse_aware_unsupervised_measures_set
% 
% Analyses AWARE unsupervised measures at different k-uples sizes and saves 
% them to a |.mat| file.

%% Synopsis
%
%   [] = analyse_aware_unsupervised_measures_set(trackID, tag, topicset,startKupleSet, endKupleSet)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|topicset| - set of considered topic indexes
% * *|startKupleSet|* - the index of the start kuples set. Optional.
% * *|endKupleSet|* - the index of the end kuples set. Optional.
%
%
% *Returns*
%
% Nothing
%


%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = analyse_aware_supervised_measures_set(trackID, tag, topicSet, startKupleSet, endKupleSet)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
    
    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that identifier is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');
    
    if iscell(tag)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % setup common parameters
    common_parameters;
        
    if nargin == 5
        validateattributes(startKupleSet, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.kuples.number }, '', 'startKupleSet');
        
        validateattributes(endKupleSet, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startKupleSet, '<=', EXPERIMENT.kuples.number }, '', 'endKupleSet');

    else 
        startKupleSet = 1;
        endKupleSet = EXPERIMENT.kuples.number;
    end
    
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.analyseMeasures(trackID, tag, startKupleSet, endKupleSet,topicSet));
    diary(EXPERIMENT.pattern.logFile.analyseMeasures(trackID, tag, startKupleSet, endKupleSet,topicSet));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Analysing %s aware supervised measures on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - confidence interval alpha %f\n', EXPERIMENT.analysis.ciAlpha);
    fprintf('  - AP correlation ties samples %d\n', EXPERIMENT.analysis.apcorrTiesSamples);
    fprintf('  - slice \n');
    fprintf('    * start k-uple set %d\n', startKupleSet);
    fprintf('    * end k-uple set %d\n', endKupleSet);
    shortTrack = EXPERIMENT.(trackID).shortID;
    tagBase= EXPERIMENT.tag.base.id;
    relativePath=EXPERIMENT.tag.base.relativePath;
    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
        shortRunset= EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            mid=EXPERIMENT.measure.id{m};
            startMeasure = tic;
            
            fprintf('  - analysing %s %s\n', tag, EXPERIMENT.measure.name{m});
            
            kp=1;
            measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, sprintf('gold_%04d',kp) , trackID, shortRunset,[]);
            serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]), ...
                        'WorkspaceVarNames', {'test'},'FileVarNames', {measureID});
            goldmeasuresTot=NaN(size(test,1),size(test,2),EXPERIMENT.kuples.samples);
            goldmeasuresTot(:,:,kp)=test{:,:};
            
            for kp = 2:EXPERIMENT.kuples.samples
                measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, sprintf('gold_%04d',kp) , trackID, shortRunset,[]);
                if (exist(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]), 'file') == 2)
                    serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]), ...
                        'WorkspaceVarNames', {'test'},'FileVarNames', {measureID});
                    goldmeasuresTot(:,:,kp)=test{:,:};
                end
            end
            
            % for each k-uple
            for k = startKupleSet:endKupleSet
                
                start = tic;
                
                fprintf('    * analysing k-uples: k%02d \n', EXPERIMENT.kuples.sizes(k));
                
                measureID = EXPERIMENT.pattern.identifier.measure(mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);
                serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID,topicSet), measureID);
                
                tauID = EXPERIMENT.pattern.identifier.pm('tau', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);   
                apcID = EXPERIMENT.pattern.identifier.pm('apc', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);              
                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);
                                
                evalf(EXPERIMENT.command.analyse_set_aware, {strcat('goldmeasuresTot(:,:,[1:size(',measureID,', 3)])'), measureID,'topicSet'}, {rmseID, tauID, apcID});
                                
                
                [path,~]=fileparts(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID,topicSet));
                if (exist (path)~=7)
                    mkdir(path)
                end    
                sersave(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID,topicSet), tauID(:), apcID(:), rmseID(:));
                
                clear(measureID, tauID, apcID, rmseID);
                
                fprintf('      # elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            end % k-uple size
            
            clear('goldmeasuresTot');
            
            fprintf('    * elapsed time for measure %s %s: %s\n', tag, EXPERIMENT.measure.name{m}, elapsedToHourMinutesSeconds(toc(startMeasure)));
            
        end % for each measure
        
    end % for runset
    
    
    fprintf('\n\n######## Total elapsed time for analysing %s aware unsupervised measures on collection %s (%s): %s ########\n\n', ...
        tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;
    

end
