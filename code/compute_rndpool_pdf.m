%% compute_rndpool_pdf
% 
% Compute the KDE of PDF for measures on random pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_rndpool_pdf(trackID, tag, granularity, aggregation,  topicSet)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|granularity|* - the granularity of the coefficients, either sgl or tpc.
% * *|aggregation|* - the aggregation of the coefficients, either msr or gp.
% * *|topicSet|* - list of considered topic indexes
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rndpool_pdf(trackID, tag, granularity, aggregation,  topicSet)

% check that trackID is a non-empty string
validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

if iscell(trackID)
    % check that trackID is a cell array of strings with one element
    assert(iscellstr(trackID) && numel(trackID) == 1, ...
        'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
end

% remove useless white spaces, if any, and ensure it is a char row
trackID = char(strtrim(trackID));
trackID = trackID(:).';

% check that tag is a non-empty string
validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

if iscell(tag)
    % check that tag is a cell array of strings with one element
    assert(iscellstr(tag) && numel(tag) == 1, ...
        'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
end

% remove useless white spaces, if any, and ensure it is a char row
tag = char(strtrim(tag));
tag = tag(:).';

% check that granularity is a non-empty string
validateattributes(granularity,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'granularity');

if iscell(granularity)
    % check that granularity is a cell array of strings with one element
    assert(iscellstr(granularity) && numel(granularity) == 1, ...
        'MATTERS:IllegalArgument', 'Expected granularity to be a cell array of strings containing just one string.');
end

% remove useless white spaces, if any, and ensure it is a char row
granularity = char(strtrim(granularity));
granularity = granularity(:).';

% check that aggregation is a non-empty string
validateattributes(aggregation,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'aggregation');

if iscell(aggregation)
    % check that aggregation is a cell array of strings with one element
    assert(iscellstr(aggregation) && numel(aggregation) == 1, ...
        'MATTERS:IllegalArgument', 'Expected aggregation to be a cell array of strings containing just one string.');
end

% remove useless white spaces, if any, and ensure it is a char row
aggregation = char(strtrim(aggregation));
aggregation = aggregation(:).';

% setup common parameters
common_parameters;

% Log file
delete(EXPERIMENT.pattern.logFile.computeMeasuresPdf(tag, trackID, topicSet));
diary(EXPERIMENT.pattern.logFile.computeMeasuresPdf(tag, trackID, topicSet));

shortTrack=EXPERIMENT.(trackID).shortID;
serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'T');

if isempty(topicSet)
    excludedTopics=[];
else
    excludedTopics=1:T;
    excludedTopics(ismember(excludedTopics,topicSet))=[];
    %disp(excludedTopics)
end
%disp(excludedTopics);
%% start of overall computations
startComputation = tic;


fprintf('\n\n######## Computing %s %s %s measures PDF on collection %s (%s) ########\n\n', granularity, aggregation, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);


fprintf('+ Settings\n');
fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
fprintf('  - granularity %s\n', granularity);
fprintf('  - aggregation %s\n\n', aggregation);

% local version of the general configuration parameters

originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);

% for each runset
for r = 1:originalTrackNumber
    originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
end

% for each runset
for r = 1:originalTrackNumber
    
    fprintf('\n+ Original track of the runs: %s\n', originalTrackShortID{r});
    
    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{1}, EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, ...
        trackID,  originalTrackShortID{r},[]);
    
    serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]),'FileVarNames',{ measureID},'WorkspaceVarNames',{'temp'});
    
    % number of topics
    T=height(temp);
    % number of runs
    R=width(temp);
    
    clear temp;
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number
        
        fprintf('  - %s\n', EXPERIMENT.measure.id{m});
        
        
        % average random measures before further processing
        if strcmpi(aggregation, 'msr')
            
            % Each plane is a TxR measure, there are rndPoolSamples
            % planes
            random = NaN(T, R, EXPERIMENT.analysis.rndPoolsSamples);
            % for each random pool
            for p = 1:EXPERIMENT.analysis.rndPoolsSamples
                sampledPool = EXPERIMENT.pattern.identifier.sampledRndPool(p);
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, sampledPool, trackID, originalTrackShortID{r},[]);
                serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID,[]),'FileVarNames',{measureID},'WorkspaceVarNames',{'measure'});
                %disp(size(random(:, :, p)));
                %disp(size(measure{:,:}));
                random(:, :, p) = measure{:,:};
                clear measure;
            end
            
            random(excludedTopics,:,:)=[];
            T=T-length(excludedTopics);

            % average over all the planes
            random = mean(random, 3);
            % now I have a Topic/Run Matrix for random measures
            %disp(random);    
            % single gap for all the topics
            %disp(random);
            if strcmpi(granularity, 'sgl')
                
                pdfValues=EXPERIMENT.command.rndPools.gap.(granularity).kde(random);
                
            else
                
                % one gap for each topic
                pdfValues= NaN(T, length(EXPERIMENT.analysis.kdeBins));
                
                for t = 1:T
                    
                    randomTopic = random(t, :);
                    
                    pdfValues(t,:)=EXPERIMENT.command.rndPools.gap.(granularity).kde(randomTopic);
                    
                end
                
                clear randomTopic;
                
            end

            pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, tag, aggregation, trackID, originalTrackShortID{r},topicSet);
            
            [path,~]=fileparts(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, 'pdf', tag, pdfID,topicSet));
            if (exist (path)~=7)
                mkdir(path);
            end
            
            sersave2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, 'pdf', tag, pdfID,topicSet),...
                'WorkspaceVarNames', {'pdfValues'},'FileVarNames', {pdfID});
            
            clear pdfValues random
            
        else
            
            % for each random pool
            T=T-length(excludedTopics);
            for p = 1:EXPERIMENT.analysis.rndPoolsSamples
                
                sampledPool = EXPERIMENT.pattern.identifier.sampledRndPool(p);
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, sampledPool, trackID, originalTrackShortID{r},[]);
                serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID,[]),'FileVarNames',{measureID},'WorkspaceVarNames',{'measure'});
                random = measure{:,:};
                random(excludedTopics,:,:)=[];
                %disp(random);
                clear measure;
                
                % single gap for all the topics
                if strcmpi(granularity, 'sgl')
                    
                    
                    pdfValues=EXPERIMENT.command.rndPools.gap.(granularity).kde(random);
                    
                else
                    
                    % one gap for each topic
                    pdfValues = NaN(T, length(EXPERIMENT.analysis.kdeBins));
                    
                    for t = 1:T
                        
                        randomTopic = random(t, :);
                        
                        pdfValues(t,:)=EXPERIMENT.command.rndPools.gap.(granularity).kde(randomTopic);
                        
                    end
                    
                    clear randomTopic;
                    
                end
                
                pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, tag, sampledPool, trackID, originalTrackShortID{r},topicSet);
                
                [path,~]=fileparts(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, 'pdf', tag, pdfID,topicSet));
                if (exist (path)~=7)
                    mkdir(path);
                end

                sersave2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, 'pdf', tag, pdfID,topicSet),...
                    'WorkspaceVarNames', {'pdfValues'},'FileVarNames', {pdfID});
     
                clear pdfValues random;
                
            end % for each random pool
                        
        end
        T=T+length(excludedTopics);
    end % for each measure
    
end % for runset

fprintf('\n\n######## Total elapsed time for computing %s %s %s measures PDF on collection %s (%s): %s ########\n\n', ...
    granularity, aggregation, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

diary off;

end
