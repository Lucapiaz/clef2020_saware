%% compute_rnd_pools
% 
% Compute the random pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_rnd_pools(tag)
%  
%
% *Parameters*
%
% * *|tag|* - the tag of the conducted experiment.
%
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rnd_pools(tag)    
    % check that tag is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

     if iscell(tag)
        % check that tag is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';    
    
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.computePools(tag, trackID));
    diary(EXPERIMENT.pattern.logFile.computePools(tag, trackID));


    % Load the primary pool as a template
    goldPoolID = EXPERIMENT.pattern.identifier.goldPool(EXPERIMENT.(trackID).shortID);
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), goldPoolID);
    

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s pools on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - random pools to be generated %d\n', EXPERIMENT.analysis.rndPoolsSamples);


    fprintf('\n+ Generating random pools\n');


    for p = 1:EXPERIMENT.analysis.rndPoolsSamples
        
        poolID = EXPERIMENT.pattern.identifier.pool(tag, EXPERIMENT.pattern.identifier.sampledRndPool(p), EXPERIMENT.(trackID).shortID);
        
        evalf(EXPERIMENT.command.rndPools.compute, ...
                [{'tag', 'poolID'}, goldPoolID], ...
                {poolID});
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.pool(trackID, tag, poolID));
            if (exist (path)~=7)
                mkdir(path)
            end
        sersave(EXPERIMENT.pattern.file.pool(trackID, tag, poolID), poolID(:));
        
        clear(poolID);
        
    end
    
    
    fprintf('\n\n######## Total elapsed time for computing %s pools on collection %s (%s): %s ########\n\n', ...
            tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
