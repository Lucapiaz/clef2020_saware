%% compute_rndpool_gap
% 
% Compute a gap on random pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_rndpool_gap(trackID, tag, gap, granularity, aggregation, topicSet)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|gap|* - the gap to be computed.
% * *|granularity|* - the granularity of the coefficients, either sgl or tpc.
% * *|aggregation|* - the aggregation of the coefficients, either msr or gp.
% * *|topicSet| - list of considered topic indexes.
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rndpool_gap(trackID, tag, gap, granularity, aggregation, topicSet)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that tag is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

    if iscell(tag)
        % check that tag is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';

    % check that gap is a non-empty string
    validateattributes(gap,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'gap');

    if iscell(gap)
        % check that gap is a cell array of strings with one element
        assert(iscellstr(gap) && numel(gap) == 1, ...
            'MATTERS:IllegalArgument', 'Expected gap to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    gap = char(strtrim(gap));
    gap = gap(:).';
    
    % check that granularity is a non-empty string
    validateattributes(granularity,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'granularity');

    if iscell(granularity)
        % check that granularity is a cell array of strings with one element
        assert(iscellstr(granularity) && numel(granularity) == 1, ...
            'MATTERS:IllegalArgument', 'Expected granularity to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    granularity = char(strtrim(granularity));
    granularity = granularity(:).';
    
    % check that aggregation is a non-empty string
    validateattributes(aggregation,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'aggregation');

    if iscell(aggregation)
        % check that aggregation is a cell array of strings with one element
        assert(iscellstr(aggregation) && numel(aggregation) == 1, ...
            'MATTERS:IllegalArgument', 'Expected aggregation to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    aggregation = char(strtrim(aggregation));
    aggregation = aggregation(:).';
    
    
    % setup common parameters
    common_parameters;          
    
    % Log file
    delete(EXPERIMENT.pattern.logFile.computePoolMeasuresGap(gap, tag, trackID,topicSet));
    diary(EXPERIMENT.pattern.logFile.computePoolMeasuresGap(gap, tag, trackID,topicSet));
    
    
    shortTrack=EXPERIMENT.(trackID).shortID;
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'T');
    
    if isempty(topicSet)
        excludedTopics=[];
    else
        excludedTopics=1:T;
        excludedTopics(ismember(excludedTopics,topicSet))=[];
    end
    %% start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Computing %s %s %s gap to %s on collection %s (%s) ########\n\n', granularity, aggregation, gap, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - granularity %s\n', granularity);
    fprintf('  - aggregation %s\n', aggregation);
    
    % local version of the general configuration parameters
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    % for each runset
    for r = 1:originalTrackNumber        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    end
  
    %% load assessor labels and assessor number
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'poolLabels', 'P');
    
    fprintf('+ Loading assessor measures\n');
    
    %% load the assessor measures 
    assessorMeasures = cell(EXPERIMENT.measure.number, P, originalTrackNumber); %measure-assessor-runset
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
                
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            % for each assessor
            for p = 1:P
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, poolLabels{p}, ...
                    trackID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r},[]);
                                
                serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]),'FileVarNames',{measureID},'WorkspaceVarNames',{'measure'});
                assessorMeasures{m, p, r}=measure{:, :};
                %disp(assessorMeasures{m, p, r});
                assessorMeasures{m, p, r}(excludedTopics,:)=[]; %remove topics not incuded in the set
                %disp(assessorMeasures{m, p, r});

                clear measure;
                
            end % for assessor
        end % for measure
    end % for runset
    
    clear r m p measureID
    
    %% load the random measures
    
    fprintf('\n+ Loading %s measures\n', tag);
    
    randomMeasures = cell(EXPERIMENT.measure.number, EXPERIMENT.analysis.rndPoolsSamples, originalTrackNumber);
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        fprintf('    ');
        
        % for each random pool
        for p = 1:EXPERIMENT.analysis.rndPoolsSamples
            
            sampledPool = EXPERIMENT.pattern.identifier.sampledRndPool(p);
            
            % for each measure
            for m = 1:EXPERIMENT.measure.number
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, sampledPool, trackID, originalTrackShortID{r},[]);
                
                serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID,[]),'FileVarNames',{measureID},'WorkspaceVarNames',{'measure'});
                randomMeasures{m, p, r}=measure{:, :};
                %disp(randomMeasures{m, p, r});
                randomMeasures{m, p, r}(excludedTopics,:)=[]; %remove topics not incuded in the set
                %disp(randomMeasures{m, p, r});

                clear measure;
            end % for measure
            fprintf('.');
        end % for random pool
        fprintf('\n');
    end % for runset
    
    clear r m p measureID
    
    %% gap computation
    
    fprintf('\n+ Computing %s gap with %s measures\n', gap, tag);
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        % number of topics
        T = size(randomMeasures{1, 1, r}, 1);
        
        % number of runs
        R = size(randomMeasures{1, 1, r}, 2);
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            mid=EXPERIMENT.measure.id{m};
            fprintf('    * %s\n', mid);
            fprintf('      ');
            
            % average random measures before further processing
            if strcmpi(aggregation, 'msr')
                
                % Each plane is a TxR measure, there are rndPoolSamples
                % planes
                random = NaN(T, R, EXPERIMENT.analysis.rndPoolsSamples);
                
                % copy from the cell matrix in the corresponding plane
                for rp = 1:EXPERIMENT.analysis.rndPoolsSamples
                    random(:, :, rp) = randomMeasures{m, rp, r};
                end
                
                % average over all the planes
                random = mean(random, 3);
                % now I have a Topic/Run Matrix for random measures
                % for each assessor pool
                for ap = 1:P
                    
                    % the assessor measures
                    assessor = assessorMeasures{m, ap, r};
     
                    % single gap for all the topics
                    if strcmpi(granularity, 'sgl')
                        
                        gapValues=EXPERIMENT.command.rndPools.gap.(granularity).(gap)(assessor, random);
                        
                    else % different gap for each topic
                        
                        % one gap for each topic
                        gapValues=NaN(1, T);
                                                
                        for t = 1:T
                            assessorTopic = assessor(t, :);
                            randomTopic = random(t, :);
                            gapValues(t)=EXPERIMENT.command.rndPools.gap.(granularity).(gap)(assessorTopic,randomTopic);
                            
                        end
                        
                        clear assessorTopic randomTopic;
                        
                    end
                    
                    

                    gapID = EXPERIMENT.pattern.identifier.pm(gap, mid, tag, poolLabels{ap}, trackID, originalTrackShortID{r},topicSet);
                    
                    [path,~]=fileparts(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, gap, tag, gapID,topicSet));
                    if (exist (path)~=7)
                        mkdir(path);
                    end
                    
                    sersave2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, gap, tag, gapID,topicSet), ...
                        'WorkspaceVarNames', {'gapValues' },'FileVarNames', {gapID});
                              
                    fprintf('.');
                     
                    clear gapValues assessor;
                    
                end % for each assessor
                
                clear random;
                                
            else
                
                
                % for each assessor pool
                for ap = 1:P
                    
                    % the assessor measures
                    assessor = assessorMeasures{m, ap, r};
                                                      
                    % single gap for all the topics
                    if strcmpi(granularity, 'sgl')
                        
                        gapValues= NaN(1, EXPERIMENT.analysis.rndPoolsSamples);
                        
                        % for each random pool
                        for rp = 1:EXPERIMENT.analysis.rndPoolsSamples
                            
                            % the random pool measures
                            random = randomMeasures{m, rp, r};
                            gapValues(rp)=EXPERIMENT.command.rndPools.gap.(granularity).(gap)(assessor,random);
                            
                            clear random;
                        end % for each random pool
                                                
                    else % different gap for each topic
                        
                         
                        gapValues = NaN(T, EXPERIMENT.analysis.rndPoolsSamples);
                        
                        % for each random pool
                        for rp = 1:EXPERIMENT.analysis.rndPoolsSamples
                            
                            % the random pool measures
                            random = randomMeasures{m, rp, r};
                            
                            for t = 1:T
                                assessorTopic = assessor(t, :);
                                randomTopic = random(t, :);
                                gapValues(t,rp)=EXPERIMENT.command.rndPools.gap.(granularity).(gap)(assessorTopic, randomTopic);
                                
                            end
                            
                            clear random assessorTopic randomTopic;
                                                        
                        end % for each random pool
                        
                    end
                    
                    gapID = EXPERIMENT.pattern.identifier.pm(gap, mid, tag, poolLabels{ap}, trackID, originalTrackShortID{r},topicSet);
                    
                    [path,~]=fileparts(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, gap, tag, gapID,topicSet));
                    if (exist (path)~=7)
                        mkdir(path);
                    end
                    
                    sersave2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, gap, tag, gapID,topicSet), ...
                        'WorkspaceVarNames', {'gapValues' },'FileVarNames', {gapID});
                         
                    fprintf('.');
                    clear assessor gapValues;
                    
                end % for each assessor    
            end
            fprintf('\n');      
        end % for each measure
    end % for runset
    
    fprintf('\n\n######## Total elapsed time for computing %s %s %s gap to %s on collection %s (%s): %s ########\n\n', ...
        granularity, aggregation, gap, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;
    
end
