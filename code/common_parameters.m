%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%% FAST CONFIG
EXPERIMENT.fast.trackID='TREC_21_2012_Crowd';
EXPERIMENT.fast.assessorsType='whole';
EXPERIMENT.fast.numTopicsets=100;
EXPERIMENT.fast.totalTopics=10;
EXPERIMENT.fast.testSize=7;
EXPERIMENT.fast.assessors=31;
EXPERIMENT.fast.kuples=2:7;
EXPERIMENT.fast.unsupervisedGaps={'rmse','tau'};
EXPERIMENT.fast.unsupervisedGran={'tpc'};
EXPERIMENT.fast.supervisedGaps={'rmse','tau'};
EXPERIMENT.fast.measuresId={'ap'};%, 'ndcg20'};
EXPERIMENT.fast.measuresName={'AP'};%,'nDCG@20'};
EXPERIMENT.fast.measuresFullName={'Average Precision (AP)'};%,'Normalized Discounted Cumulated Gain at 20 Retrieved Documents (nDCG20)'};
%%
diary off;
[~,host]=system('hostname');
host=strtrim(host);

%% Configuration for collection TREC 21, 2012, Crowdsourcing

EXPERIMENT.TREC_21_2012_Crowd.id = 'TREC_21_2012_Crowd';
EXPERIMENT.TREC_21_2012_Crowd.shortID = 'T21';
EXPERIMENT.TREC_21_2012_Crowd.name =  'TREC 21, 2012, Crowdsourcing';

if (strcmpi(host, 'grace.dei.unipd.it'))
    EXPERIMENT.TREC_21_2012_Crowd.collection.path.base = fullfile(filesep,'ssd','data','piazzon','CIKM2','collections','TREC_21_2012_Crowd',filesep);
elseif (strcmpi(host, 'eva.dei.unipd.it'))
    EXPERIMENT.TREC_21_2012_Crowd.collection.path.base = fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','collections','TREC_21_2012_Crowd',filesep);
else
    EXPERIMENT.TREC_21_2012_Crowd.collection.path.base = fullfile(filesep,'Users','Work','Documents','CIKM2','collections','TREC_21_2012_Crowd',filesep);
end

EXPERIMENT.TREC_21_2012_Crowd.collection.path.crowd = sprintf('%1$s%2$s%3$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, strcat('crowd_T21_',EXPERIMENT.fast.assessorsType), filesep);
EXPERIMENT.TREC_21_2012_Crowd.collection.path.runSet = @(originalTrackID) sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'runs', filesep, originalTrackID);

EXPERIMENT.TREC_21_2012_Crowd.collection.file.goldPool = sprintf('%1$s%2$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'trat-adjudicated-qrels.txt');

EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.id = {'TREC_08_1999_AdHoc','TREC_13_2004_Robust'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.shortID = {'T08','T13'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.name = {'TREC_08_1999_AdHoc','TREC_13_2004_Robust'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.number = length(EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.id);

%% Configuration for collection TREC 26, 2017, Core Track + crowdsourced data

EXPERIMENT.TREC_26_2017_Core.id = 'TREC_26_2017_Core';
EXPERIMENT.TREC_26_2017_Core.shortID = 'T26';
EXPERIMENT.TREC_26_2017_Core.name =  'TREC 26, 2017, Core Track';

if (strcmpi(host, 'grace.dei.unipd.it'))
    EXPERIMENT.TREC_26_2017_Core.collection.path.base = fullfile(filesep,'ssd','data','piazzon','CIKM2','collections','TREC_26_2017_Core',filesep);
elseif (strcmpi(host, 'eva.dei.unipd.it'))
    EXPERIMENT.TREC_26_2017_Core.collection.path.base = fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','collections','TREC_26_2017_Core',filesep);
else
    EXPERIMENT.TREC_26_2017_Core.collection.path.base = fullfile(filesep,'Users','Work','Documents','CIKM2','collections','TREC_26_2017_Core',filesep);
end

EXPERIMENT.TREC_26_2017_Core.collection.path.crowd = sprintf('%1$s%2$s%3$s', EXPERIMENT.TREC_26_2017_Core.collection.path.base, strcat('crowd_T26_',EXPERIMENT.fast.assessorsType), filesep);
EXPERIMENT.TREC_26_2017_Core.collection.path.runSet = @(originalTrackID) sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.TREC_26_2017_Core.collection.path.base, 'runs', filesep, originalTrackID);

EXPERIMENT.TREC_26_2017_Core.collection.file.goldPool = sprintf('%1$s%2$s', EXPERIMENT.TREC_26_2017_Core.collection.path.base, 'nist_qrels.txt');

EXPERIMENT.TREC_26_2017_Core.collection.runSet.originalTrackID.id = {'TREC_26_2017_Core'};
EXPERIMENT.TREC_26_2017_Core.collection.runSet.originalTrackID.shortID = {'T26'};
EXPERIMENT.TREC_26_2017_Core.collection.runSet.originalTrackID.name = {'TREC 26, 2017, Core Track'};
EXPERIMENT.TREC_26_2017_Core.collection.runSet.originalTrackID.number = length(EXPERIMENT.TREC_26_2017_Core.collection.runSet.originalTrackID.id);


%% General configuration

% if we are running on the cluster

if (strcmpi(host, 'grace.dei.unipd.it'))
    addpath(fullfile(filesep,'ssd','data','piazzon','CIKM2','Matters',filesep));
    addpath(fullfile(filesep,'ssd','data','piazzon','CIKM2','Matters','core','analysis',filesep));
    addpath(fullfile(filesep,'ssd','data','piazzon','CIKM2','Matters','core','io',filesep));
    addpath(fullfile(filesep,'ssd','data','piazzon','CIKM2','Matters','core','measure',filesep));
    addpath(fullfile(filesep,'ssd','data','piazzon','CIKM2','Matters','core','plot',filesep));
    addpath(fullfile(filesep,'ssd','data','piazzon','CIKM2','Matters','core','util',filesep));
elseif (strcmpi(host, 'eva.dei.unipd.it'))
    addpath(fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','Matters',filesep));
    addpath(fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','Matters','core','analysis',filesep));
    addpath(fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','Matters','core','io',filesep));
    addpath(fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','Matters','core','measure',filesep));
    addpath(fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','Matters','core','plot',filesep));
    addpath(fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2','Matters','core','util',filesep));
else
    addpath(fullfile('C:','Users','Work','Documents','CIKM2','Matters',filesep))
    addpath(fullfile('C:','Users','Work','Documents','CIKM2','Matters','core','analysis',filesep))
    addpath(fullfile('C:','Users','Work','Documents','CIKM2','Matters','core','io',filesep))
    addpath(fullfile('C:','Users','Work','Documents','CIKM2','Matters','core','measure',filesep))
    addpath(fullfile('C:','Users','Work','Documents','CIKM2','Matters','core','plot',filesep))
    addpath(fullfile('C:','Users','Work','Documents','CIKM2','Matters','core','util',filesep))
end

% The base path
if (strcmpi(host, 'grace.dei.unipd.it'))
    EXPERIMENT.path.base = fullfile(filesep,'ssd','data','piazzon','CIKM2',strcat('experiment_',EXPERIMENT.(EXPERIMENT.fast.trackID).shortID,'_',int2str(EXPERIMENT.fast.testSize),'_',EXPERIMENT.fast.assessorsType),filesep);
elseif (strcmpi(host, 'eva.dei.unipd.it'))
    EXPERIMENT.path.base = fullfile(filesep,'nas1','promise','ims','piazzon','CIKM2',strcat('experiment_',EXPERIMENT.(EXPERIMENT.fast.trackID).shortID,'_',int2str(EXPERIMENT.fast.testSize),'_',EXPERIMENT.fast.assessorsType),filesep);
else
    EXPERIMENT.path.base = fullfile(filesep,'Users','Work','Documents','CIKM2',strcat('experiment_',EXPERIMENT.(EXPERIMENT.fast.trackID).shortID,'_',int2str(EXPERIMENT.fast.testSize),'_',EXPERIMENT.fast.assessorsType),filesep);
end

% The path for logs
EXPERIMENT.path.log = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'log', filesep);
if (exist (EXPERIMENT.path.log)~=7)
    mkdir(EXPERIMENT.path.log)
end

% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);
if (exist (EXPERIMENT.path.dataset)~=7)
    mkdir(EXPERIMENT.path.dataset)
end
% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'AWARE 2020 - PIAZZON';

% Label to be used for the gold standard pool
EXPERIMENT.label.goldPool = 'GOLD';


%% Overall Experiment Taxonomy

% Command to create the tag of an experiment from its components
EXPERIMENT.taxonomy.us.tag = @(family, learning, feature, granularity, aggregation, gap, weight) ...
    sprintf('%1$s_%2$s_%3$s_%4$s_%5$s_%6$s_%7$s', ...
        family, learning, feature, granularity, aggregation, gap, weight);
EXPERIMENT.taxonomy.sup.tag = @(family, learning, feature,gap,weight) ...
    sprintf('%1$s_%2$s_%3$s_%4$s_%5$s', ...
        family, learning, feature, gap, weight);
    
% The baseline approaches to confront with
EXPERIMENT.taxonomy.baseline.id = 'baseline';
EXPERIMENT.taxonomy.baseline.description = 'Baseline approaches';
EXPERIMENT.taxonomy.baseline.list = {'aw_uni', 'mv','emmv','emzhu','emsemi2'};   
EXPERIMENT.taxonomy.baseline.number = length(EXPERIMENT.taxonomy.baseline.list);

% The constituents of the AWARE approaches
EXPERIMENT.taxonomy.us.granularity.list = EXPERIMENT.fast.unsupervisedGran;
EXPERIMENT.taxonomy.us.granularity.number = length(EXPERIMENT.taxonomy.us.granularity.list);
    
EXPERIMENT.taxonomy.us.aggregation.list = {'msr'};
EXPERIMENT.taxonomy.us.aggregation.number = length(EXPERIMENT.taxonomy.us.aggregation.list);
    
EXPERIMENT.taxonomy.us.gap.list = EXPERIMENT.fast.unsupervisedGaps;
EXPERIMENT.taxonomy.us.gap.number = length(EXPERIMENT.taxonomy.us.gap.list);
    
EXPERIMENT.taxonomy.us.weight.list = {'msd'};
EXPERIMENT.taxonomy.us.weight.number = length(EXPERIMENT.taxonomy.us.weight.list);

EXPERIMENT.taxonomy.sup.gap.list = EXPERIMENT.fast.supervisedGaps;
EXPERIMENT.taxonomy.sup.gap.number = length(EXPERIMENT.taxonomy.sup.gap.list);
EXPERIMENT.taxonomy.sup.weight.list={'d','sd','cd'};
EXPERIMENT.taxonomy.sup.weight.number = length(EXPERIMENT.taxonomy.sup.weight.list);

EXPERIMENT.taxonomy.approaches = EXPERIMENT.taxonomy.us.granularity.number * EXPERIMENT.taxonomy.us.aggregation.number * ...
EXPERIMENT.taxonomy.us.gap.number * EXPERIMENT.taxonomy.us.weight.number + ...
EXPERIMENT.taxonomy.baseline.number + EXPERIMENT.taxonomy.sup.gap.number*EXPERIMENT.taxonomy.sup.weight.number;

EXPERIMENT.taxonomy.baseline.colors=[hex('#666666',1);hex('#000000',1);hex('#000099',1);hex('#999999',1),;hex('#009999',1)];
EXPERIMENT.taxonomy.unsup.colors=[hex('#228B33',EXPERIMENT.taxonomy.us.gap.number*EXPERIMENT.taxonomy.us.weight.number);hex('#32CD32',EXPERIMENT.taxonomy.us.gap.number*EXPERIMENT.taxonomy.us.weight.number)];
EXPERIMENT.taxonomy.sup.colors=[hex('#DC143C',EXPERIMENT.taxonomy.sup.gap.number);hex('#FF6000',EXPERIMENT.taxonomy.us.gap.number);hex('#FFA500',EXPERIMENT.taxonomy.us.gap.number)];
EXPERIMENT.taxonomy.colors = cat(1,EXPERIMENT.taxonomy.unsup.colors(1:EXPERIMENT.taxonomy.us.gap.number*EXPERIMENT.taxonomy.us.granularity.number*EXPERIMENT.taxonomy.us.weight.number,:),...
                                EXPERIMENT.taxonomy.sup.colors(1:EXPERIMENT.taxonomy.sup.gap.number*EXPERIMENT.taxonomy.sup.weight.number,:),...
                                EXPERIMENT.taxonomy.baseline.colors(1:EXPERIMENT.taxonomy.baseline.number,:));
                            
EXPERIMENT.taxonomy.pattern={'-';'-.s';':o';'--';':'};
EXPERIMENT.taxonomy.width=[1.5;1.5;1.5;1.5;1.5];
%% Configuration for measures

% the identifier of the measures under experimentation
EXPERIMENT.measure.id = EXPERIMENT.fast.measuresId;

% the number of measures under experimentation
EXPERIMENT.measure.number = length(EXPERIMENT.measure.id);

% the names of the measures under experimentation
EXPERIMENT.measure.name = EXPERIMENT.fast.measuresName;

% the full names of the measures under experimentation
EXPERIMENT.measure.fullName = EXPERIMENT.fast.measuresFullName;


%% Configuration for the kuples to be used

% Number of pools/assessors to be merged
EXPERIMENT.kuples.sizes = EXPERIMENT.fast.kuples;

EXPERIMENT.kuples.labelNames = strtrim(cellstr(num2str(EXPERIMENT.kuples.sizes(:), 'k = %d')));

% The total number of merges to performa
EXPERIMENT.kuples.number = length(EXPERIMENT.kuples.sizes);

% Maximum number of k-uples to be sampled
EXPERIMENT.kuples.samples = 100;

%% configuration for supervised experiments

EXPERIMENT.topicset.number=EXPERIMENT.fast.numTopicsets;
EXPERIMENT.topicset.size=EXPERIMENT.fast.testSize;
EXPERIMENT.totalTopics=EXPERIMENT.fast.totalTopics;
%% Configuration for analyses

% The confidence degree for computing the confidence interval
EXPERIMENT.analysis.ciAlpha = 0.05;

% The number of samples for ties in AP correlation
EXPERIMENT.analysis.apcorrTiesSamples = 500;

% The bandwidth for KDE
EXPERIMENT.analysis.kdeBandwidth = 0.015;

% The bins for kernel density estimation (exceed the range [0 1] to
% compensate for the bandwidth of KSD
EXPERIMENT.analysis.kdeBins = -0.1:0.01:1.1;

% The number of random pools to be generated
EXPERIMENT.analysis.rndPoolsSamples = 100;




%% Configuration for the Expectation-Maximization algorithm

% Maximum number of iterations for expectation maximization algorithm
EXPERIMENT.em.maxIterations = 1000;

% Tolerance for expectation maximization algorithm
EXPERIMENT.em.tolerance = 1e-3;


%% Baseline data for the different experiments

% Baseline data
EXPERIMENT.tag.base.id = 'base';
EXPERIMENT.tag.base.description = 'Baseline data';
EXPERIMENT.tag.base.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);

% Majority vote pools
EXPERIMENT.tag.mv.id = 'mv';
EXPERIMENT.tag.mv.description = 'Majority vote pools';
EXPERIMENT.tag.mv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.mv.latex.label = '\texttt{mv}';
EXPERIMENT.tag.mv.matlab.label = 'mv';

% Expectation-Maximization pools seeded by majority vote
EXPERIMENT.tag.emmv.id = 'emmv';
EXPERIMENT.tag.emmv.description = 'Expectation-Maximization pools seeded by majority vote';
EXPERIMENT.tag.emmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emmv.command = @expectationMaximizationPoolMv;
EXPERIMENT.tag.emmv.latex.label = '\texttt{emmv}';
EXPERIMENT.tag.emmv.matlab.label = 'emmv';

% Expectation-Maximization pools, North-Eastern University variant
EXPERIMENT.tag.emneu.id = 'emneu';
EXPERIMENT.tag.emneu.description = 'Expectation-Maximization pools, North-Eastern University variant';
EXPERIMENT.tag.emneu.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emneu.command = @expectationMaximizationPoolNeu;
EXPERIMENT.tag.emneu.latex.label = '\texttt{emneu}';
EXPERIMENT.tag.emneu.matlab.label = 'emneu';

% Expectation-Maximization pools, Georgescu-Zhu variant
EXPERIMENT.tag.emzhu.id = 'emneu';
EXPERIMENT.tag.emzhu.description = 'Expectation-Maximization pools, Georgescu-Zhu variant';
EXPERIMENT.tag.emzhu.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emzhu.command = @expectationMaximizationPoolZhu;
EXPERIMENT.tag.emzhu.latex.label = '\texttt{emzhu}';
EXPERIMENT.tag.emzhu.matlab.label = 'emzhu';

% Expectation-Maximization pools, Semi-supervised
EXPERIMENT.tag.emsemi.id = 'emsemi';
EXPERIMENT.tag.emsemi.description = 'Expectation-Maximization pools, Semi-supervised variant';
EXPERIMENT.tag.emsemi.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emsemi.command = @expectationMaximizationPoolSemi;
EXPERIMENT.tag.emsemi.latex.label = '\texttt{emsemi}';
EXPERIMENT.tag.emsemi.matlab.label = 'emsemi';

% Expectation-Maximization pools, Semi-supervised
EXPERIMENT.tag.emsemi2.id = 'emsemi2';
EXPERIMENT.tag.emsemi2.description = 'Expectation-Maximization pools, Semi-supervised variant - gold excluded';
EXPERIMENT.tag.emsemi2.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emsemi2.command = @expectationMaximizationPoolSemi2;
EXPERIMENT.tag.emsemi2.latex.label = '\texttt{emsemi2}';
EXPERIMENT.tag.emsemi2.matlab.label = 'emsemi2';

% Random uniform pools with assessors judging 5% of the documents as relevant
EXPERIMENT.tag.rnd005.id = 'rnd005';
EXPERIMENT.tag.rnd005.description = 'Random uniform pools with assessors judging 5% of the documents as relevant';
EXPERIMENT.tag.rnd005.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd005.ratio = [0.95 0.05];

% Random uniform pools with assessors judging 50% of the documents as relevant
EXPERIMENT.tag.rnd050.id = 'rnd050';
EXPERIMENT.tag.rnd050.description = 'Random uniform pools with assessors judging 50% of the documents as relevant';
EXPERIMENT.tag.rnd050.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd050.ratio = [0.50 0.50];

% Random uniform pools with assessors judging 95% of the documents as relevant
EXPERIMENT.tag.rnd095.id = 'rnd095';
EXPERIMENT.tag.rnd095.description = 'Random uniform pools with assessors judging 95% of the documents as relevant';
EXPERIMENT.tag.rnd095.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd095.ratio = [0.05 0.95];

% Baseline AWARE: uniform weights for all assessors
EXPERIMENT.tag.aw_uni.id = 'aw_uni';
EXPERIMENT.tag.aw_uni.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.aw_uni.score.command = @aw_uni;
EXPERIMENT.tag.aw_uni.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_uni.latex.label = '\texttt{uni}';
EXPERIMENT.tag.aw_uni.matlab.label = 'uni';

%% Gap normalization unsupervised

% Commands to be executed to map the different gaps in the range [0, 1] so
% that 0 means different from random assessor and 1 means equal to random
% assessor

% Frobenius norm is in [0, sqrt(T*R)], where 0 means equal to random
% assessor. Divide it by its max and reverse it so that 0 means different
% from random assessor
EXPERIMENT.command.normalize.fro = @(x, T, R) 1 - (x./sqrt(T*R));

% RMSE is in [0, 1] where 0 means equal to random assessor. Reverse it so 
% that 0 means different from random assessor
EXPERIMENT.command.normalize.rmse = @(x, T, R) 1 - x;

% AP correlation is in [-1, 1], where 0 means different from random
% assessor, 1 means equal to random assessor and -1 completely opposite to
% random assessor. Consider -1 as 1. 
EXPERIMENT.command.normalize.apc = @(x, T, R) abs(x);
                   
% Kendall's tau correlation is in [-1, 1], where 0 means different from random
% assessor, 1 means equal to random assessor and -1 completely opposite to
% random assessor. Consider -1 as 1. 
EXPERIMENT.command.normalize.tau = @(x, T, R) abs(x);

% KL divergence is in [0, +\infty), where 0 means equal to random
% assessor. Map it in the (0, 1] range by negative exponential so that 0  
% means different from random assessor
EXPERIMENT.command.normalize.kld = @(x, T, R) exp(-x);

%% Gap normalization supervised

EXPERIMENT.command.normalize.fro = @(x, T, R) 1 - (x./sqrt(T*R));
EXPERIMENT.command.normalize.rmse = @(x, T, R) 1 - x;
EXPERIMENT.command.normalize.apc = @(x, T, R) abs(x);       
EXPERIMENT.command.normalize.tau = @(x, T, R) abs(x);
EXPERIMENT.command.normalize.kld = @(x, T, R) exp(-x);

%% Commands to be executed to compute weights in unsupervised approach

EXPERIMENT.command.weight.sgl.msr.md = @(und, uni, ovr, T) repmat(min([und; uni; ovr]), T, 1);
EXPERIMENT.command.weight.sgl.msr.msd = @(und, uni, ovr, T) repmat(min([und.^2; uni.^2; ovr.^2]), T, 1);
EXPERIMENT.command.weight.sgl.msr.med = @(und, uni, ovr, T) repmat(sum([und; uni; ovr]), T, 1);

EXPERIMENT.command.weight.sgl.gp.md = @(und, uni, ovr, T) repmat(min([mean(und);mean(uni);mean(ovr)]), T, 1);
EXPERIMENT.command.weight.sgl.gp.msd = @(und, uni, ovr, T) repmat(min([mean(und).^2;mean(uni).^2;mean(ovr).^2]), T, 1);
EXPERIMENT.command.weight.sgl.gp.med = @(und, uni, ovr, T) repmat(sum([mean(und);mean(uni);mean(ovr)]), T, 1);

EXPERIMENT.command.weight.sgl.wgt.md = @(und, uni, ovr, T) repmat(mean(min(cat(3, und, uni, ovr), [], 3)), T, 1);
EXPERIMENT.command.weight.sgl.wgt.msd = @(und, uni, ovr, T) repmat(mean(min(cat(3, und.^2, uni.^2, ovr.^2), [], 3)), T, 1);
EXPERIMENT.command.weight.sgl.wgt.med = @(und, uni, ovr, T) repmat(mean(sum(cat(3, und, uni, ovr), 3)), T, 1);

EXPERIMENT.command.weight.tpc.msr.md = @(und, uni, ovr, T) min(cat(3, und, uni, ovr), [], 3);
EXPERIMENT.command.weight.tpc.msr.msd = @(und, uni, ovr, T) min(cat(3, und.^2, uni.^2, ovr.^2), [], 3);
EXPERIMENT.command.weight.tpc.msr.med = @(und, uni, ovr, T) sum(cat(3, und, uni, ovr), 3);

EXPERIMENT.command.weight.tpc.gp.md = @(und, uni, ovr, T) min(cat(3, mean(und, 3), mean(uni, 3), mean(ovr, 3)), [], 3);
EXPERIMENT.command.weight.tpc.gp.msd = @(und, uni, ovr, T) min(cat(3, mean(und, 3).^2, mean(uni, 3).^2, mean(ovr, 3).^2), [], 3);
EXPERIMENT.command.weight.tpc.gp.med = @(und, uni, ovr, T) sum(cat(3, mean(und, 3), mean(uni, 3), mean(ovr, 3)), 3);

EXPERIMENT.command.weight.tpc.wgt.md = @(und, uni, ovr, T) mean(min(cat(4, und, uni, ovr), [], 4), 3);
EXPERIMENT.command.weight.tpc.wgt.msd = @(und, uni, ovr, T) mean(min(cat(4, und.^2, uni.^2, ovr.^2), [], 4), 3);
EXPERIMENT.command.weight.tpc.wgt.med = @(und, uni, ovr, T) mean(sum(cat(4, und, uni, ovr), 4), 3);
%% Commands to be executed to compute weights in supervised approach
EXPERIMENT.command.weight.sup.d=@(x) x;     %normal dissimilarity
EXPERIMENT.command.weight.sup.sd=@(x) x.^2; %squared dissimilarity
EXPERIMENT.command.weight.sup.cd=@(x) x.^3; %cubed dissimilarity
%% aware unsupervised approaches

name = @(granularity, aggregation, gap, weight, left_rnd, right_rnd) sprintf('aw_us_mono_%s_%s_%s_%s', granularity, aggregation, gap, weight);
for grt = 1:length(EXPERIMENT.taxonomy.us.granularity.list)
    for agg = 1:length(EXPERIMENT.taxonomy.us.aggregation.list)
        for gp = 1:length(EXPERIMENT.taxonomy.us.gap.list)
            for w = 1:length(EXPERIMENT.taxonomy.us.weight.list)  
                tagid = name(EXPERIMENT.taxonomy.us.granularity.list{grt}, EXPERIMENT.taxonomy.us.aggregation.list{agg}, EXPERIMENT.taxonomy.us.gap.list{gp}, EXPERIMENT.taxonomy.us.weight.list{w});
                EXPERIMENT.tag.(tagid).id=tagid;
                EXPERIMENT.tag.(tagid).family.id = 'aw';
                EXPERIMENT.tag.(tagid).learning.id = 'us';
                EXPERIMENT.tag.(tagid).feature.id = 'mono';
                EXPERIMENT.tag.(tagid).granularity.id = EXPERIMENT.taxonomy.us.granularity.list{grt};
                EXPERIMENT.tag.(tagid).aggregation.id = EXPERIMENT.taxonomy.us.aggregation.list{agg};
                EXPERIMENT.tag.(tagid).gap.id = EXPERIMENT.taxonomy.us.gap.list{gp};
                EXPERIMENT.tag.(tagid).gap.normalization = EXPERIMENT.command.normalize.(EXPERIMENT.taxonomy.us.gap.list{gp});
                EXPERIMENT.tag.(tagid).weight.id = EXPERIMENT.taxonomy.us.weight.list{w};
                EXPERIMENT.tag.(tagid).weight.command = EXPERIMENT.command.weight.(EXPERIMENT.taxonomy.us.granularity.list{grt}).(EXPERIMENT.taxonomy.us.aggregation.list{agg}).(EXPERIMENT.taxonomy.us.weight.list{w});
                EXPERIMENT.tag.(tagid).score.command = str2func(sprintf('%1$s%2$s%3$s%4$s','@aw_us_mono_',EXPERIMENT.taxonomy.us.granularity.list{grt},'_',EXPERIMENT.taxonomy.us.aggregation.list{agg}));
                EXPERIMENT.tag.(tagid).score.callerFunction = 'compute_aware_unsupervised_mono_measures';
                EXPERIMENT.tag.(tagid).relativePath = sprintf('aware_unsupervised%2$s%1$s%2$s%3$s%2$s', EXPERIMENT.taxonomy.us.granularity.list{grt}, filesep, EXPERIMENT.taxonomy.us.aggregation.list{agg});
                EXPERIMENT.tag.(tagid).und = EXPERIMENT.tag.rnd005.id;
                EXPERIMENT.tag.(tagid).uni = EXPERIMENT.tag.rnd050.id;
                EXPERIMENT.tag.(tagid).ovr = EXPERIMENT.tag.rnd095.id;
                EXPERIMENT.tag.(tagid).latex.label = sprintf('%1$s%2$s%3$s%4$s%5$s%6$s%7$s', '\texttt{', EXPERIMENT.taxonomy.us.granularity.list{grt},'\_', EXPERIMENT.taxonomy.us.gap.list{gp},'\_',EXPERIMENT.taxonomy.us.weight.list{w},'}');
                EXPERIMENT.tag.(tagid).matlab.label = sprintf('%1$s%2$s%3$s%4$s%5$s', EXPERIMENT.taxonomy.us.granularity.list{grt},'_', EXPERIMENT.taxonomy.us.gap.list{gp},'_',EXPERIMENT.taxonomy.us.weight.list{w});
            end
        end
    end
end
clear('grt','gp','name','tagid','w','agg')

%% aware supervsed approach

name = @(gap, weight) sprintf('aw_sup_mono_%1$s_%2$s', gap, weight);
for gp = 1:length(EXPERIMENT.taxonomy.sup.gap.list)
    for w = 1:length(EXPERIMENT.taxonomy.sup.weight.list)  
        tagid = name(EXPERIMENT.taxonomy.sup.gap.list{gp},EXPERIMENT.taxonomy.sup.weight.list{w});
        EXPERIMENT.tag.(tagid).id=tagid;
        EXPERIMENT.tag.(tagid).family.id = 'aw';
        EXPERIMENT.tag.(tagid).learning.id = 'sup';
        EXPERIMENT.tag.(tagid).feature.id = 'mono';
        EXPERIMENT.tag.(tagid).gap.id = EXPERIMENT.taxonomy.sup.gap.list{gp};
        EXPERIMENT.tag.(tagid).weight.id = EXPERIMENT.taxonomy.sup.weight.list{w};
        EXPERIMENT.tag.(tagid).gap.normalization = EXPERIMENT.command.normalize.(EXPERIMENT.taxonomy.sup.gap.list{gp});
        EXPERIMENT.tag.(tagid).weight.command = EXPERIMENT.command.weight.sup.(EXPERIMENT.tag.(tagid).weight.id);
        EXPERIMENT.tag.(tagid).relativePath = sprintf('aware_supervised%1$s%2$s%1$s',filesep,EXPERIMENT.tag.(tagid).weight.id);
        EXPERIMENT.tag.(tagid).latex.label = sprintf('%1$s%2$s%3$s%4$s%5$s%6$s%7$s', '\texttt{', 'sup','\_', EXPERIMENT.taxonomy.sup.gap.list{gp},'\_', EXPERIMENT.taxonomy.sup.weight.list{w},'}');
        EXPERIMENT.tag.(tagid).matlab.label = sprintf('%1$s%2$s%3$s%4$s%5$s', 'sup','_', EXPERIMENT.taxonomy.sup.gap.list{gp},'_',EXPERIMENT.taxonomy.sup.weight.list{w});
    end
end
clear('gp','name','tagid','w')
%%
% Pattern EXPERIMENT.path.base/dataset/<trackID>/topicset_<trackShortID>.mat
EXPERIMENT.pattern.file.topicset = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$stopicset_%4$s.mat', EXPERIMENT.path.base, filesep, trackID, trackShortID);

EXPERIMENT.pattern.identifier.topicidx.folder=@(trackID,indexes)sprintf('t_%1$s', getIndex(EXPERIMENT.pattern.file.topicset(EXPERIMENT.(trackID).id, EXPERIMENT.(trackID).shortID),indexes,EXPERIMENT.totalTopics));


%% Patterns for LOG files

% Pattern EXPERIMENT.path.base/log/import_collection_<trackID>.log
EXPERIMENT.pattern.logFile.importCollection = @(trackID) sprintf('%1$simport_collection_%2$s.log', EXPERIMENT.path.log, trackID);

% Pattern EXPERIMENT.path.base/log/_topicset_compute_<tag>_measures_<trackID>.log
EXPERIMENT.pattern.logFile.computeMeasures = @(trackID, tag, topicset) sprintf('%1$s$4$scompute_%2$s_measures_%3$s.log', EXPERIMENT.path.log, tag, trackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));

% Pattern EXPERIMENT.path.base/log/generate_kuples_<trackID>.log
EXPERIMENT.pattern.logFile.generateKuples = @(trackID) sprintf('%1$sgenerate_kuples_%2$s.log', EXPERIMENT.path.log, trackID);

% Pattern EXPERIMENT.path.base/log/generate_topicsets_<trackID>.log
EXPERIMENT.pattern.logFile.generateTopicsets = @(trackID) sprintf('%1$sgenerate_topicsets_%2$s.log', EXPERIMENT.path.log, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<experimentTag>_poold_<trackID>.log
EXPERIMENT.pattern.logFile.computePools = @(experimentTag, trackID) sprintf('%1$scompute_%2$s_pools_%3$s.log', EXPERIMENT.path.log, experimentTag, trackID);

% Pattern EXPERIMENT.path.base/log/_topicset_compute_<experimentTag>_pool_measures_slice-<startKupleSet>-<endKupleSet>-<startKuple>-<endKuple>_<trackID>.log
EXPERIMENT.pattern.logFile.computePoolMeasures = @(experimentTag, startKupleSet, endKupleSet, startKuple, endKuple, trackID,topicset) ...
    sprintf('%1$s%8$scompute_%2$s_pool_measures_slice-%3$d-%4$d-%5$d-%6$d_%7$s.log', EXPERIMENT.path.log, experimentTag, ...
            startKupleSet, endKupleSet, startKuple, endKuple, trackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));

% Pattern EXPERIMENT.path.base/log/_topicset_compute_<gap>_<experimentTag>_pool_measures_gap_<trackID>.log
EXPERIMENT.pattern.logFile.computePoolMeasuresGap = @(gap, experimentTag, trackID,topicset) ...
    sprintf('%1$s%5$scompute_%2$s_%3$s_pool_measures_gap_%4$s.log', EXPERIMENT.path.log, gap, experimentTag, trackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));
        
% Pattern EXPERIMENT.path.base/log/_topicset_compute_<experimentTag>_measures_pdf_<trackID>.log
EXPERIMENT.pattern.logFile.computeMeasuresPdf = @(experimentTag, trackID,topicset) ...
    sprintf('%1$s%4$scompute_%2$s_measures_pdf_%3$s.log', EXPERIMENT.path.log, experimentTag, trackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));

% Pattern EXPERIMENT.path.base/log/_topicset_analyse_<tag>_measures_slice-<startKupleSet>-<endKupleSet>_<trackID>.log
EXPERIMENT.pattern.logFile.analyseMeasures = @(trackID, tag, startKupleSet, endKupleSet,topicset) sprintf('%1$s%6$sanalyse_%2$s_measures_slice-%3$d-%4$d_%5$s.log', EXPERIMENT.path.log, tag, startKupleSet, endKupleSet, trackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));

% Pattern EXPERIMENT.path.base/log/_topicset_print_report_<desc>_<trackID>.log
EXPERIMENT.pattern.logFile.printReport = @(trackID, desc, topicSet) sprintf('%1$s%4$sprint_report_%2$s_%3$s.log', EXPERIMENT.path.log, desc, trackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicSet));

%% Patterns for identifiers

% Pattern pool_<experimentTag>_<poolLabel>_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(experimentTag, poolLabel, trackID) sprintf('pool_%1$s_%2$s_%3$s', experimentTag, poolLabel, trackID);

% Pattern pool_base_GOLD_<trackID>
EXPERIMENT.pattern.identifier.goldPool = @(trackID) EXPERIMENT.pattern.identifier.pool(EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, trackID);


% Pattern runSet_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.runSet = @(trackID, originalTrackID) sprintf('runSet_p%1$sr%2$s', trackID, originalTrackID);

% Pattern k<size>
EXPERIMENT.pattern.identifier.kuple = @(k) sprintf('k%1$02d', EXPERIMENT.kuples.sizes(k));
EXPERIMENT.pattern.identifier.oldKuple = @(k) sprintf('k%1$02d', k);

% Pattern kuples_k<size>_<trackID>
EXPERIMENT.pattern.identifier.kuples = @(k, trackID) sprintf('kuples_k%1$02d_%2$s', k, trackID);

% Pattern k<size>_s<sampleNumber>
EXPERIMENT.pattern.identifier.sampledKuple = @(k, s) sprintf('k%1$02d_s%2$04d', k, s);


EXPERIMENT.pattern.identifier.goldPoolRed = @(trackID,sample) sprintf('goldpool_s%1$04d_%2$s', sample, trackID);

EXPERIMENT.pattern.identifier.assessorRed = @(poolid,sample) sprintf('%1$s_s%2$04d',poolid,sample);

% Pattern s<sampleNumber>
EXPERIMENT.pattern.identifier.sampledRndPool = @(s) sprintf('s%1$04d', s);

% Pattern [topicset]<measureID>_<experimentTag>_<experimentLabel>_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.measure = @(measureID, experimentTag, experimentLabel, trackID, originalTrackID,topicSet) ...
    sprintf('%6$s%1$s_%2$s_%3$s_p%4$sr%5$s', lower(measureID), experimentTag, experimentLabel, EXPERIMENT.(trackID).shortID, originalTrackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicSet));

% Pattern [topicset]<pm>_<measureID>_<experimentTag>_<experimentLabel>_<trackID>_<originalTrackID>

EXPERIMENT.pattern.identifier.pm = @(pm, measureID, experimentTag, experimentLabel, trackID, originalTrackID,topicSet) ...
    sprintf('%7$s%1$s_%2$s_%3$s_%4$sp%5$sr%6$s',pm, lower(measureID), experimentTag, experimentLabel, EXPERIMENT.(trackID).shortID, originalTrackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicSet));

% Pattern accuracy_<experimentTag>_<experimentLabel>_<trackID>
EXPERIMENT.pattern.identifier.accuracy =  @(experimentTag, experimentLabel, shortTrackID) ...
    sprintf('accuracy_%1$s_%2$s_%3$s', experimentTag, experimentLabel, shortTrackID);

% Pattern <factors>_<shortTrackID>
EXPERIMENT.pattern.identifier.anova.id = @(factors, shortTrackID) ...
    sprintf('%1$s_%2$s', factors, shortTrackID);

% Pattern <factors>_<metric>_<effects>_<shortTrackID>
EXPERIMENT.pattern.identifier.anova.figure = @(factors, metric, effects, shortTrackID) ...
    sprintf('%1$s_%2$s_%3$s_%4$s', factors, lower(metric), effects, shortTrackID);

% Pattern anovaTable_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.table = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('anovaTable_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);

% Pattern anovaStats_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.stats = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('anovaStats_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);

% Pattern soa_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.soa = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('soa_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);

%% Patterns for MAT files

% Pattern EXPERIMENT.path.base/dataset/<trackID>/EXPERIMENT.tag.base.id/dataset_<trackShortID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$s%4$s%2$sdataset_%5$s.mat', EXPERIMENT.path.base, filesep, trackID, EXPERIMENT.tag.base.id, trackShortID);

EXPERIMENT.pattern.file.goldPoolRed = @(trackID, trackShortID,sample) sprintf('%1$sdataset%2$s%3$s%2$sgoldpools%2$s%5$04d%2$s%4$s.mat', EXPERIMENT.path.base, filesep, trackID, EXPERIMENT.pattern.identifier.goldPoolRed(trackShortID,sample),sample);
EXPERIMENT.pattern.file.assessorRed = @(trackID, trackShortID,sample) sprintf('%1$sdataset%2$s%3$s%2$sassessors%2$s%4$04d%2$sassessors_%4$04d.mat', EXPERIMENT.path.base, filesep, trackID,sample);


% Pattern EXPERIMENT.path.base/dataset/<trackID>/kuples_<trackShortID>.mat
EXPERIMENT.pattern.file.kuples = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$skuples_%4$s.mat', EXPERIMENT.path.base, filesep, trackID, trackShortID);


% Pattern EXPERIMENT.path.base/dataset/<trackID>/<experimentTag>/<poolID>
EXPERIMENT.pattern.file.pool = @(trackID, experimentTag, poolID) ...
    sprintf('%1$sdataset%2$s%3$s%2$s%4$s%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, experimentTag, poolID);

% Pattern EXPERIMENT.path.base/measure/<trackID>/<topicidx>/<relativePath>/<experimentTag>/[topicset]<measureID>
EXPERIMENT.pattern.file.measure = @(trackID, relativePath, experimentTag, measureID,topicset) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%7$s%2$s%4$s%5$s%2$s%6$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, experimentTag, measureID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));

% Pattern EXPERIMENT.path.base/analysis/<trackID>/<topicidx>/<relativePath>/<experimentTag>/[topicset]<measureID>
EXPERIMENT.pattern.file.analysis = @(trackID, relativePath, experimentTag, measureID,topicset) ...
    sprintf('%1$sanalysis%2$s%3$s%2$s%7$s%2$s%4$s%5$s%2$s%6$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, experimentTag, measureID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));

% Pattern EXPERIMENT.path.base/pre-compute/<trackID>/relativepath/topicset/<granularity>/<aggregation>/<pm>/<topicidx>/<experimentTag>/[topicset]<preID>
EXPERIMENT.pattern.file.pre = @(trackID, relativePath, granularity, aggregation, pm, experimentTag, preID,topicset) ...
    sprintf('%1$spre-compute%2$s%3$s%2$s%9$s%2$s%10$s%2$s%4$s%2$s%5$s%2$s%6$s%2$s%7$s%2$s%8$s.mat', ...
    EXPERIMENT.path.base, filesep, trackID, granularity, aggregation, pm,  experimentTag, preID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset),relativePath);

EXPERIMENT.pattern.file.pre_sup = @(trackID, relativePath, preID,topicset) ...
    sprintf('%1$spre-compute%2$s%3$s%2$s%4$s%2$s%5$s%2$s%6$s.mat', ...
    EXPERIMENT.path.base, filesep, trackID, EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset),relativePath, preID);

% Pattern EXPERIMENT.path.base/report/<trackID>/[topicset]_<type>_report_<trackID>.mat
EXPERIMENT.pattern.file.report = @(type, trackID, topicset) sprintf('%1$sreport%2$s%4$s%2$s%5$s_%3$s_report_%4$s.tex', EXPERIMENT.path.base, filesep, type, trackID,EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));
EXPERIMENT.pattern.file.heatmap = @(trackID,name) sprintf('%1$sheatmap%2$s%4$s%2$sheatmap_%3$s.tex', EXPERIMENT.path.base, filesep, name,trackID);

% Pattern EXPERIMENT.path.base/figure/<trackID>/<relativePath>/[topicset]_<anovaID>
EXPERIMENT.pattern.file.anova = @(trackID, relativePath, figureID, topicset) sprintf('%1$sfigure%2$s%3$s%2$s%4$s%2$s%6$s_%5$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, figureID, EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));

% Pattern EXPERIMENT.path.base/figure/<trackID>/<relativePath>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, relativePath, figureID, topicset) sprintf('%1$sfigure%2$s%3$s%2$s%4$s%2$s%6$s_%5$s.png', EXPERIMENT.path.base, filesep, trackID, relativePath, figureID, EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset));
EXPERIMENT.pattern.file.figure2 = @(trackID, relativePath, name,topicset) sprintf('%1$sfigure%2$s%3$s%2$s%4$s%2$s%5$s%2$s%6$s%2$s%7$s.png', EXPERIMENT.path.base, filesep, trackID, relativePath, EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset),int2str(EXPERIMENT.topicset.size),name);
EXPERIMENT.pattern.file.figure3 = @(trackID, relativePath, name,topicset) sprintf('%1$sfigure%2$s%3$s%2$s%4$s%2$s%5$s%2$s%6$s%2$s%7$s.pdf', EXPERIMENT.path.base, filesep, trackID, relativePath, EXPERIMENT.pattern.identifier.topicidx.folder(trackID,topicset),int2str(EXPERIMENT.topicset.size),name);

%% Commands to be executed

% Utility commands
EXPERIMENT.command.util.varList2Cell = @(varargin)(deal(varargin));
EXPERIMENT.command.util.assignVars =  @(varargin)(varargin{:});

% Commands to be executed to import TREC_21_2012_Crowd
EXPERIMENT.command.TREC_21_2012_Crowd.importGoldPool = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'Delimiter', 'space',  'Verbose', false);
EXPERIMENT.command.TREC_21_2012_Crowd.importCrowdPool = @(fileName, id, requiredTopics) importCrowd2012Pool('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'RequiredTopics', requiredTopics, 'Delimiter', 'tab',  'Verbose', false);
EXPERIMENT.command.TREC_21_2012_Crowd.importRunSet = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', 'TrecEvalLexDesc', 'SinglePrecision', true, 'Verbose', false);

% Commands to be executed to import TREC_26_2017_Core
EXPERIMENT.command.TREC_26_2017_Core.importGoldPool = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'Delimiter', 'space',  'Verbose', false);
EXPERIMENT.command.TREC_26_2017_Core.importCrowdPool = @(fileName, id, requiredTopics) importCrowd2012Pool('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'RequiredTopics', requiredTopics, 'Delimiter', 'tab',  'Verbose', false);
EXPERIMENT.command.TREC_26_2017_Core.importRunSet = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', 'TrecEvalLexDesc', 'SinglePrecision', true, 'Verbose', false);


% Commands to be executed to compute the measures
EXPERIMENT.command.measure.ap = @(pool, runSet, shortNameSuffix) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix);
EXPERIMENT.command.measure.p10 = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', 10);
EXPERIMENT.command.measure.rbp = @(pool, runSet, shortNameSuffix) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', 0.80);
EXPERIMENT.command.measure.ndcg20 = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', 20, 'Normalize', true, 'FixNumberRetrievedDocuments', 1000, 'FixedNumberRetrievedDocumentsPaddingStrategy', 'NotRelevant');
EXPERIMENT.command.measure.err20 =  @(pool, runSet, shortNameSuffix) expectedReciprocalRank(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', 20, 'FixNumberRetrievedDocuments', 1000, 'FixedNumberRetrievedDocumentsPaddingStrategy', 'NotRelevant');

% Commands to be executed to generate topicsets
EXPERIMENT.command.topicset.generate = @(topicIdx, size) combnk(topicIdx, size);
EXPERIMENT.command.topicset.sample = @(topicsets, T, samples) topicsets(randperm(T, samples), :);

% Commands to be executed to generate kuples
EXPERIMENT.command.kuples.generate = @(poolsIdx, kupleSize) combnk(poolsIdx, kupleSize);
EXPERIMENT.command.kuples.sample = @(kuples, K, samples) kuples(randperm(K, samples), :);

% Commands to be executed to compute MV pools
EXPERIMENT.command.mvPools.compute = @(poolID, varargin) majorityVotePool(poolID, varargin{:});

% Commands to be executed to compute EM pools
EXPERIMENT.command.emPools.compute = @(tag, poolID,goldpool, varargin) EXPERIMENT.tag.(tag).command(EXPERIMENT.em.tolerance, EXPERIMENT.em.maxIterations, poolID,goldpool, varargin{:});

% Commands to be executed to compute RND pools
EXPERIMENT.command.rndPools.compute = @(tag, poolID, goldPool) randomPool(poolID, goldPool, EXPERIMENT.tag.(tag).ratio);
        
% Commands to be executed to compute pool measures
EXPERIMENT.command.computePoolMeasures.kupleSize = @(kuples) size(kuples, 1);

% Commands to be executed to compute gaps from random pools
EXPERIMENT.command.rndPools.gap.tpc.fro = @(random, assessor) norm(random - assessor, 'fro');
EXPERIMENT.command.rndPools.gap.tpc.apc = @(random, assessor) apCorr(random(:), assessor(:), 'Ties', true, 'TiesSamples', EXPERIMENT.analysis.apcorrTiesSamples);
EXPERIMENT.command.rndPools.gap.tpc.tau = @(random, assessor) corr(random(:), assessor(:), 'type', 'Kendall');
EXPERIMENT.command.rndPools.gap.tpc.rmse = @(random, assessor) rmseCoeff(random(:), assessor(:));
EXPERIMENT.command.rndPools.gap.tpc.kde = @(measure) kdEstimation(measure(:), EXPERIMENT.analysis.kdeBins, EXPERIMENT.analysis.kdeBandwidth);
EXPERIMENT.command.rndPools.gap.tpc.kld = @(q, p) klDivergence(EXPERIMENT.analysis.kdeBins, q, p{:});

EXPERIMENT.command.rndPools.gap.sgl.fro = @(random, assessor) norm(random - assessor, 'fro');
EXPERIMENT.command.rndPools.gap.sgl.apc = @(random, assessor) apCorr(nanmean(random,1).', nanmean(assessor,1).', 'Ties', true, 'TiesSamples', EXPERIMENT.analysis.apcorrTiesSamples);
EXPERIMENT.command.rndPools.gap.sgl.tau = @(random, assessor) corr(nanmean(random,1).', nanmean(assessor,1).', 'type', 'Kendall');
EXPERIMENT.command.rndPools.gap.sgl.rmse = @(random, assessor) rmseCoeff(nanmean(random,1).', nanmean(assessor,1).');
EXPERIMENT.command.rndPools.gap.sgl.kde = @(measure) kdEstimation(measure(:), EXPERIMENT.analysis.kdeBins, EXPERIMENT.analysis.kdeBandwidth);
EXPERIMENT.command.rndPools.gap.sgl.kld = @(q, p) klDivergence(EXPERIMENT.analysis.kdeBins, q, p{:});

% Commands to be executed to compute gaps from gold pool
EXPERIMENT.command.goldPool.gap.fro = @(gold, assessor) norm(gold - assessor, 'fro');
EXPERIMENT.command.goldPool.gap.apc = @(gold, assessor) apCorr(nanmean(gold,1).', nanmean(assessor,1).', 'Ties', true, 'TiesSamples', EXPERIMENT.analysis.apcorrTiesSamples);
EXPERIMENT.command.goldPool.gap.tau = @(gold, assessor) corr(nanmean(gold,1).', nanmean(assessor,1).', 'type', 'Kendall');
EXPERIMENT.command.goldPool.gap.rmse = @(gold, assessor) rmseCoeff(nanmean(gold,1).', nanmean(assessor,1).');
EXPERIMENT.command.goldPool.gap.kde = @(measure) kdEstimation(measure(:), EXPERIMENT.analysis.kdeBins, EXPERIMENT.analysis.kdeBandwidth);
EXPERIMENT.command.goldPool.gap.kld = @(q, p) klDivergence(EXPERIMENT.analysis.kdeBins, q, p);

% Commands to be executed to compute AWARE measures
EXPERIMENT.command.aware = @(a, kuples,mid, measures) aware(a, kuples,mid, measures{:});

% Commands to be executed to analyse AWARE measures
EXPERIMENT.command.analyse = @(goldMeasure, awareMeasure) analyseKuples(EXPERIMENT.analysis.ciAlpha, EXPERIMENT.analysis.apcorrTiesSamples, goldMeasure, awareMeasure);
EXPERIMENT.command.analyse_set = @(goldMeasure, awareMeasure,topicSet) analyseKuples_set(EXPERIMENT.analysis.ciAlpha, EXPERIMENT.analysis.apcorrTiesSamples, goldMeasure, awareMeasure, topicSet);
EXPERIMENT.command.analyse_set_aware = @(goldMeasure, awareMeasure,topicSet) analyseKuples_set_aware(EXPERIMENT.analysis.ciAlpha, EXPERIMENT.analysis.apcorrTiesSamples, goldMeasure, awareMeasure, topicSet);
EXPERIMENT.command.analyse_set_emsemi = @(goldMeasure, awareMeasure,topicSet) analyseKuples_set_emsemi(EXPERIMENT.analysis.ciAlpha, EXPERIMENT.analysis.apcorrTiesSamples, goldMeasure, awareMeasure, topicSet);
