%% analyseKuples_set_emsemi
% used for emsemi2 analysis
% Computes Kendall's tau correlation, AP correlation, and Root Mean Square
% Error (RMSE) among evaluation measures, using a subset of the topics.
% only topics in a specific topicset are used in analysis computation

%% Synopsis
%
%   [rmse, tau, apcorr] = analyseKuples_set(ciAlpha, apcorrTiesSamples, goldMeasure, awareMeasures, topicSet)
%  
%
% *Parameters*
%
% * *|ciAlpha|* - the confidence degree for computing the confidence interval;
% * *|apcorrTiesSamples|* - the number of samples to deal with ties in AP  
% correlation. 0 indicates no dealing with ties;
% * *|goldMeasure|* - the refernce measure against which aware measures
% are compared;
% * *|awareMeasures|* - the aware measures to be compared among themselves
% and against the the reference one.
%
% * *|topicSet|* - indexes of the topics to be used.
%
% *Returns*
%
% * |rmse|  - a struct containing the RMSE with respect to the gold standard 
% pool and all the k-uple pairs.
% * |tau|  - a struct containing the Kendall's tau with respect to the gold
% standard pool and all the k-uple pairs.
% * |apcorr|  - a struct containing the AP correlation with respect to the 
% gold standard pool and all the k-uple pairs.

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [rmse, tau, apcorr] = analyseKuples_set_emsemi(ciAlpha, apcorrTiesSamples, goldMeasure, awareMeasures, topicSet)

    excludedTopics=(1:size(goldMeasure, 1));
    excludedTopics(ismember(excludedTopics,topicSet))=[];

    % check that ciAlpha is a nonempty scalar "real" value
    % greater than 0 and less than or equal to 1
    validateattributes(ciAlpha, {'numeric'}, ...
        {'nonempty', 'scalar', 'real', '>', 0, '<=', 1}, '', 'ciAlpha');
    
    % check that the samples for apcorr ties are a nonempty scalar integer value
    % greater than 0
    validateattributes(apcorrTiesSamples, {'numeric'}, ...
        {'nonempty', 'scalar', 'integer', '>=', 0}, '', 'apcorrTiesSamples');
        
    % total number of runs
    R = size(awareMeasures, 2);
    
    % total number of kuples
    K = size(awareMeasures, 3);
    
   
    % 1) compute the mean aware measures for each plane (to rank the
    % systems) [mean(*, 1)  -> 1xRxK matrix]
    % 2) remove the useless third pland [reshape(*, R, K)]
    awareMeasures(excludedTopics,:,:)=[]; %remove topics not incuded in the set
    data = reshape(nanmean(awareMeasures, 1), R, K);
    goldMeasure(excludedTopics,:,:)=[]; %remove topics not incuded in the set
    golddata = reshape(nanmean(goldMeasure, 1), R, K);
    % the data for Kendall's tau
    tauData = NaN(1, K);
    
    % the data for AP correlation
    apcorrData = NaN(1, K);
    
    % the data for RMSE
    rmseData = NaN(1, K);
    
    % for each kuple
    for k = 1:K        
        
        % compute the Kendall's tau among all the kuples and the primary
        tauData(k) = corr(golddata(:, k), data(:, k), 'type', 'Kendall');
                
        % compute the AP correlation among all the kuples and the primary
        apcorrData(k) = apCorr(golddata(:, k), data(:, k), 'Ties', true, 'TiesSamples', apcorrTiesSamples);
                
        % compute the RMSE among all the kuples and the primary
        rmseData(k) = rmseCoeff(golddata(:, k), data(:, k));
    end
 
    % Kendall's tau summary
    tau.data = tauData;
    tau.mean = nanmean(tauData);
    tau.ci = confidenceIntervalDelta(tauData, ciAlpha, 2);
    tau.max = max(tauData);
    tau.min = min(tauData);
    
    % AP correlation summary
    apcorr.data = apcorrData;
    apcorr.mean = nanmean(apcorrData);
    apcorr.ci = confidenceIntervalDelta(apcorrData, ciAlpha, 2);
    apcorr.max = max(apcorrData);
    apcorr.min = min(apcorrData);
    
    % RMSE summary
    rmse.data = rmseData;
    rmse.mean = nanmean(rmseData);
    rmse.ci = confidenceIntervalDelta(rmseData, ciAlpha, 2);
    rmse.max = max(rmseData);
    rmse.min = min(rmseData);   
end

