%% compute_goldpool_pdf
% 
% Compute the KDE of PDF for measures on gold pool and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_goldpool_pdf(trackID, tag)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|granularity|* - the granularity of the coefficients, either sgl or tpc.
% * *|aggregation|* - the aggregation of the coefficients, either msr or gp.
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_goldpool_pdf(trackID, topicSet)

% check that trackID is a non-empty string
validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

if iscell(trackID)
    % check that trackID is a cell array of strings with one element
    assert(iscellstr(trackID) && numel(trackID) == 1, ...
        'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
end

% remove useless white spaces, if any, and ensure it is a char row
trackID = char(strtrim(trackID));
trackID = trackID(:).';

% setup common parameters
common_parameters;

% Log file
delete(EXPERIMENT.pattern.logFile.computeMeasuresPdf(EXPERIMENT.tag.base.id, trackID, topicSet));
diary(EXPERIMENT.pattern.logFile.computeMeasuresPdf(EXPERIMENT.tag.base.id, trackID, topicSet));

shortTrack=EXPERIMENT.(trackID).shortID;
serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'T');

if isempty(topicSet)
    excludedTopics=[];
else
    excludedTopics=1:T;
    excludedTopics(ismember(excludedTopics,topicSet))=[];
    %disp(excludedTopics)
end
%disp(excludedTopics);
%% start of overall computations
startComputation = tic;


fprintf('\n\n######## Computing gold measures PDF on collection %s (%s) ########\n\n',  EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);


fprintf('+ Settings\n');
fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


% local version of the general configuration parameters

originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);

% for each runset
for r = 1:originalTrackNumber
    originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
end

% for each runset
for r = 1:originalTrackNumber
    
    fprintf('\n+ Original track of the runs: %s\n', originalTrackShortID{r});
    
    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{1}, EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, trackID,  originalTrackShortID{r},[]);
    
    serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]),'FileVarNames',{ measureID},'WorkspaceVarNames',{'temp'});
    
    % number of topics
    T=height(temp);
    % number of runs
    R=width(temp);
    clear temp;
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number
        
        fprintf('  - %s\n', EXPERIMENT.measure.id{m});

        gold = NaN(T, R);
        measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, trackID,  originalTrackShortID{r},[]);
        serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]),'FileVarNames',{measureID},'WorkspaceVarNames',{'measure'});
        gold(:, :) = measure{:,:};
        clear measure;
        gold(excludedTopics,:)=[];
        T=T-length(excludedTopics);
                
        pdfValues=EXPERIMENT.command.goldPool.gap.kde(gold);
        pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, 'gold', 'ALL', trackID, originalTrackShortID{r},topicSet);
            
        [path,~]=fileparts(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', pdfID,topicSet));
        if (exist (path)~=7)
            mkdir(path);
        end
            
        sersave2(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', pdfID, topicSet),'WorkspaceVarNames', {'pdfValues'},'FileVarNames', {pdfID});

        clear pdfValues gold
            
        
        T=T+length(excludedTopics);
    end % for each measure
    
end % for runset

fprintf('\n\n######## Total elapsed time for computing gold measures PDF on collection %s (%s): %s ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

diary off;

end
