%% analyse_kams_anova
% 
% Computes a four factors, mixed effects, repeated measures GLMM
% considering kuplse as random factor (repeated measures/within-subject) 
% and EXPERIMENT.taxonomy.approaches, measures, and systems as fixed factors.
%
%% Synopsis
%
%   [] = analyse_kams_anova()
%
% *Returns*
%
% Nothing
%
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = analyse_kams_anova(trackID, topicSet)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % setup common parameters
    common_parameters;
                
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport(trackID,'analyse_kams_anova', topicSet));
    diary(EXPERIMENT.pattern.logFile.printReport(trackID,'analyse_kams_anova',topicSet));
    
    % the ANOVA factors label
    factors = 'kams';
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Computing %s ANOVA analyses on collection %s (%s) ########\n\n', upper(factors), EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    fprintf('+ Loading analyses\n');
    
    % total number of elements in the list
    N = (EXPERIMENT.taxonomy.approaches) * (originalTrackNumber * EXPERIMENT.kuples.number * EXPERIMENT.measure.number) ;
    
    % preallocate vectors for each measure
    data.apc = NaN(1, N);  % the apc data
    data.rmse = NaN(1, N);  % the apc data
    subject = cell(1, N);  % grouping variable for the subjects (kuple)
    factorA = cell(1, N);  % grouping variable for factorA (EXPERIMENT.taxonomy.approaches)
    factorB = cell(1, N);  % grouping variable for factorB (measure)
    factorC = cell(1, N);  % grouping variable for factorC (systems)
    
    % the current element in the list
    currentElement = 1;

    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});
        
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});
            
            measureStart = currentElement;
            
            for k = 1:EXPERIMENT.kuples.number
                
                fprintf('      # k-uple: %s \n', EXPERIMENT.pattern.identifier.kuple(k));
                
                for g = 1:EXPERIMENT.taxonomy.us.granularity.number
                    for a = 1:EXPERIMENT.taxonomy.us.aggregation.number
                        for gp = 1:EXPERIMENT.taxonomy.us.gap.number
                            for w = 1:EXPERIMENT.taxonomy.us.weight.number
                                
                                e = EXPERIMENT.taxonomy.us.tag('aw', 'us', 'mono', ...
                                    EXPERIMENT.taxonomy.us.granularity.list{g}, ...
                                    EXPERIMENT.taxonomy.us.aggregation.list{a}, ...
                                    EXPERIMENT.taxonomy.us.gap.list{gp}, ...
                                    EXPERIMENT.taxonomy.us.weight.list{w});
                                
                                %fprintf('        experiment: %s \n', e);
                                
                                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    trackID,  originalTrackShortID{r},topicSet);
                                
                                apcID = trunc(EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    trackID,  originalTrackShortID{r},topicSet));
                                
                                rmseID = trunc(EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    trackID,  originalTrackShortID{r},topicSet));
                                
                                serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID, topicSet), ...
                                    {apcID, 'apcTmp';
                                    rmseID, 'rmseTmp'});
                                
                                data.apc(currentElement) = apcTmp.mean;
                                data.rmse(currentElement) = rmseTmp.mean;
                                subject{currentElement} = EXPERIMENT.pattern.identifier.kuple(k);
                                factorA{currentElement} = EXPERIMENT.tag.(e).matlab.label;
                                factorB{currentElement} = EXPERIMENT.measure.name{m};
                                factorC{currentElement} = originalTrackShortID{r};
                                
                                currentElement = currentElement + 1;
                                
                                clear apcTmp rmseTmp
                                
                            end % weight
                        end % gap
                    end % aggregation
                end % granularity
                
                %supervised tags
                for w=1:EXPERIMENT.taxonomy.sup.weight.number
                    for gp = 1:EXPERIMENT.taxonomy.sup.gap.number
                    
                        e = EXPERIMENT.taxonomy.sup.tag('aw', 'sup', 'mono', EXPERIMENT.taxonomy.sup.gap.list{gp},EXPERIMENT.taxonomy.sup.weight.list{w});

                        measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},e, EXPERIMENT.pattern.identifier.kuple(k), ...
                            trackID,  originalTrackShortID{r},topicSet);

                        apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                            trackID,  originalTrackShortID{r},topicSet);

                        rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                            trackID,  originalTrackShortID{r},topicSet);

                        serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID, topicSet), ...
                            {apcID, 'apcTmp';
                            rmseID, 'rmseTmp'});

                        data.apc(currentElement) = apcTmp.mean;
                        data.rmse(currentElement) = rmseTmp.mean;
                        subject{currentElement} = EXPERIMENT.pattern.identifier.kuple(k);
                        factorA{currentElement} = EXPERIMENT.tag.(e).matlab.label;
                        factorB{currentElement} = EXPERIMENT.measure.name{m};
                        factorC{currentElement} = originalTrackShortID{r};

                        currentElement = currentElement + 1;

                        clear apcTmp rmseTmp
                    end    
                end % supervised tags
                
                % add the baselines
                for e = 1:EXPERIMENT.taxonomy.baseline.number
                    
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                        trackID,  originalTrackShortID{r},topicSet);
                    
                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                        trackID,  originalTrackShortID{r},topicSet);
                    
                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                        trackID,  originalTrackShortID{r},topicSet);
                    
                    
                    serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).relativePath,  EXPERIMENT.taxonomy.baseline.list{e}, measureID,topicSet), ...
                        {apcID, 'apcTmp';
                        rmseID, 'rmseTmp'});
                    
                    data.apc(currentElement) = apcTmp.mean;
                    data.rmse(currentElement) = rmseTmp.mean;
                    subject{currentElement} = EXPERIMENT.pattern.identifier.kuple(k);
                    factorA{currentElement} =  EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).matlab.label;
                    factorB{currentElement} = EXPERIMENT.measure.name{m};
                    factorC{currentElement} = originalTrackShortID{r};
                    
                    
                    currentElement = currentElement + 1;
                    
                    clear apcTmp rmseTmp;
                    
                end % additional tags
                
            end % kuples
            
            % normalize RMSE data by the maximum for that measure
            %disp(data.rmse)
            %data.rmse(measureStart:currentElement-1) = data.rmse(measureStart:currentElement-1) ./ max(data.rmse(measureStart:currentElement-1));
            %disp(data.rmse)
        end % measures
    end % for each runset
    
        
    fprintf('+ Computing the analyses\n');    

    % the model = Kuple + Approach + Measure + Systems + 
    %             Approach*Measure + Approach*Systems + Measure*Systems
    % 
    m = [1 0 0 0; ...
         0 1 0 0; ...
         0 0 1 0; ...
         0 0 0 1; ...
         0 1 1 0; ...
         0 1 0 1; ...
         0 0 1 1];
    
    % compute a 3-way ANOVA for AP correlation
    [~, apcTbl, apcStats] = anovan(data.apc, {subject, factorA, factorB, factorC}, 'Model', m, ...
        'VarNames', {'K-uple Size', 'Approach', 'Measure', 'Systems'}, ...
        'alpha', 0.05, 'display', 'off');
    
    df_approach = apcTbl{3,3};
    ss_approach = apcTbl{3,2};
    F_approach = apcTbl{3,6};
    
    df_measure = apcTbl{4,3};
    ss_measure = apcTbl{4,2};
    F_measure = apcTbl{4,6};
    
    df_systems = apcTbl{5,3};
    ss_systems = apcTbl{5,2};
    F_systems = apcTbl{5,6};
    
    df_approach_measure = apcTbl{6,3};
    ss_approach_measure = apcTbl{6,2};
    F_approach_measure = apcTbl{6,6};
    
    df_approach_systems = apcTbl{7,3};
    ss_approach_systems = apcTbl{7,2};
    F_approach_systems = apcTbl{7,6};
    
    df_measure_systems = apcTbl{8,3};
    ss_measure_systems = apcTbl{8,2};
    F_measure_systems = apcTbl{8,6};
    
    ss_error = apcTbl{9, 2};
    df_error = apcTbl{9, 3};
    
    ss_total = apcTbl{10, 2};
    
    
    soa.omega2.apc.approach = df_approach * (F_approach - 1) /  (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.measure = df_measure * (F_measure - 1) /     (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.systems = df_systems * (F_systems - 1) /     (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.approach_measure = df_approach_measure * (F_approach_measure - 1) /  (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.approach_systems = df_approach_systems * (F_approach_measure - 1) /  (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.measure_systems = df_measure_systems * (F_approach_measure - 1) /    (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    
    
    soa.omega2p.apc.approach = df_approach * (F_approach - 1) / (df_approach * (F_approach - 1) + N);
    soa.omega2p.apc.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.apc.systems = df_systems * (F_systems - 1) / (df_systems * (F_systems - 1) + N);
    soa.omega2p.apc.approach_measure = df_approach_measure * (F_approach_measure - 1) / (df_approach_measure * (F_approach_measure - 1) + N);
    soa.omega2p.apc.approach_systems = df_approach_systems * (F_approach_systems - 1) / (df_approach_systems * (F_approach_systems - 1) + N);
    soa.omega2p.apc.measure_systems = df_measure_systems * (F_measure_systems - 1) / (df_measure_systems * (F_measure_systems - 1) + N);
    
    soa.eta2.apc.approach = ss_approach / ss_total;
    soa.eta2.apc.measure = ss_measure / ss_total;
    soa.eta2.apc.systems = ss_systems / ss_total;
    soa.eta2.apc.approach_measure = ss_approach_measure / ss_total;
    soa.eta2.apc.approach_systems = ss_approach_systems / ss_total;
    soa.eta2.apc.measure_systems = ss_measure_systems / ss_total;
    
    soa.eta2p.apc.approach = ss_approach / (ss_approach + ss_error);
    soa.eta2p.apc.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.apc.systems = ss_systems / (ss_systems + ss_error);
    soa.eta2p.apc.approach_measure = ss_approach_measure / (ss_approach_measure + ss_error);
    soa.eta2p.apc.approach_systems = ss_approach_systems / (ss_approach_systems + ss_error);
    soa.eta2p.apc.measure_systems = ss_measure_systems / (ss_measure_systems + ss_error);
    

    Slabels = unique(subject, 'stable');
    Alabels = unique(factorA, 'stable');
    Blabels = unique(factorB, 'stable');
    Clabels = unique(factorC, 'stable');
    
    % main effects
    [me.subject.mean, me.subject.ci] = grpstats(data.apc(:), subject(:), {'mean', 'meanci'});
    [me.factorA.mean, me.factorA.ci] = grpstats(data.apc(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data.apc(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data.apc(:), factorC(:), {'mean', 'meanci'});
    
    % interaction between kuple size (x-axis) and approach (y-axis)
    % each row is an approach, columns are kuple size
    ie.factorSA.mean = grpstats(data.apc(:), {subject(:),factorA(:)}, {'mean'});
    ie.factorSA.mean = reshape(ie.factorSA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.kuples.number).';
    
    
    % interaction between measure (x-axis) and approach (y-axis)
    % each row is a approach, columns are measures
    ie.factorBA.mean = grpstats(data.apc(:), {factorB(:), factorA(:)}, {'mean'});
    ie.factorBA.mean = reshape(ie.factorBA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.measure.number).';
        
    % interaction between systems (x-axis) and approach (y-axis)
    % each row is an approch, columns are systems
    ie.factorCA.mean = grpstats(data.apc(:), {factorC(:), factorA(:)}, {'mean'});
    ie.factorCA.mean = reshape(ie.factorCA.mean, EXPERIMENT.taxonomy.approaches, originalTrackNumber).';
    
    currentFigure = figure('Visible', 'off');

        xTick = 1:EXPERIMENT.taxonomy.approaches;
        plotData.mean = me.factorA.mean.';
        plotData.ciLow = me.factorA.ci(:, 1).';
        plotData.ciHigh = me.factorA.ci(:, 2).';        
        [sorted,I]=sort(plotData.mean);
        a=EXPERIMENT.taxonomy.colors;
        for i=1:EXPERIMENT.taxonomy.approaches;
            bar(i, sorted(i),'facecolor', a(I(i),:),'BarWidth', 0.7);
            axis([0 EXPERIMENT.taxonomy.approaches+1 min(sorted)-0.1  max(sorted)+0.1])
            hold on
        end
        
        er = errorbar(xTick,plotData.mean(I),plotData.ciLow(I)-plotData.mean(I),plotData.mean(I)-plotData.ciHigh(I));    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 
        
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = mapLabels(Alabels(I));
        %disp(Alabels)
        ax.XTickLabelRotation = 90;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Approach';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('AP Correlation Marginal Mean');
        figureID = 'apc\maina';

        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 30 20];
        print(currentFigure, '-dpng','-r300', EXPERIMENT.pattern.file.figure2(trackID, factors,figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;        

    currentFigure = figure('Visible', 'off');
    
        xTick = 1:EXPERIMENT.kuples.number;
        plotData.mean = me.subject.mean.';
        plotData.ciLow = me.subject.ci(:, 1).';
        plotData.ciHigh = me.subject.ci(:, 2).';        
    
        bar(xTick, plotData.mean,'BarWidth', 0.7);
        axis([0 EXPERIMENT.kuples.number+1 min(plotData.mean)-0.1  max(plotData.mean)+0.1])
        hold on
        er = errorbar(xTick,plotData.mean,plotData.ciLow-plotData.mean,plotData.mean-plotData.ciHigh);    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 
    
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = Slabels;
        ax.XTickLabelRotation = 90;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Kuple Size';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('AP Correlation Marginal Mean');
        figureID = 'apc\maink';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 30 20];
        print(currentFigure, '-dpng','-r300', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;        
    
   currentFigure = figure('Visible', 'off');
    
        xTick = 1:EXPERIMENT.measure.number;
        plotData.mean = me.factorB.mean.';
        plotData.ciLow = me.factorB.ci(:, 1).';
        plotData.ciHigh = me.factorB.ci(:, 2).';        
    
        bar(xTick, plotData.mean,'BarWidth', 0.5);
        axis([0 EXPERIMENT.measure.number+1 min(plotData.mean)-0.1  max(plotData.mean)+0.1])
        hold on
        er = errorbar(xTick,plotData.mean,plotData.ciLow-plotData.mean,plotData.mean-plotData.ciHigh);    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 
    
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = Blabels;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Measure';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('AP Correlation Marginal Mean');
        figureID = 'apc\mainm';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 15 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;
        
        
   currentFigure = figure('Visible', 'off');
    
        xTick = 1:originalTrackNumber;
        plotData.mean = me.factorC.mean.';
        plotData.ciLow = me.factorC.ci(:, 1).';
        plotData.ciHigh = me.factorC.ci(:, 2).';        
        
         bar(xTick, plotData.mean,'BarWidth', 0.5);
        axis([0 originalTrackNumber+1 min(plotData.mean)-0.1  max(plotData.mean)+0.1])
        hold on
        er = errorbar(xTick,plotData.mean,plotData.ciLow-plotData.mean,plotData.mean-plotData.ciHigh);    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 

       ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = Clabels;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'System';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('AP Correlation Marginal Mean');
        figureID = 'apc\mains';

        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 15 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;
    
%interaction effects APC               
        
    currentFigure = figure('Visible', 'off');
    plotData = ie.factorSA.mean.';
    hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches-EXPERIMENT.taxonomy.baseline.number
            plot(1:EXPERIMENT.kuples.number, plotData(l, :),EXPERIMENT.taxonomy.pattern{mod(l,EXPERIMENT.taxonomy.sup.gap.number)+1,:}, 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', EXPERIMENT.taxonomy.width(mod(l,EXPERIMENT.taxonomy.sup.gap.number)+1,:));                
        end 
        for l = EXPERIMENT.taxonomy.approaches-EXPERIMENT.taxonomy.baseline.number+1:EXPERIMENT.taxonomy.approaches
            plot(1:EXPERIMENT.kuples.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2);                
        end
        ylim([0.19 0.8])
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:EXPERIMENT.kuples.number;
        ax1.XTickLabel = Slabels;
        ax1.XTickLabelRotation = 90;
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'K-uple Size';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
        ax1.XGrid = 'on';
        
        title('K-uple Size*Approach Interaction', 'FontSize', 15, 'Interpreter', 'tex');

        %legend(mapLabels(Alabels), 'Location', 'BestOutside', 'Interpreter', 'none');
        
        figureID = 'apc\intak';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure3(trackID, factors, figureID,topicSet));
        
        close(currentFigure)    
    
        clear plotData;
        
    currentFigure = figure('Visible', 'off');
    plotData = ie.factorBA.mean.';
    hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.us.gap.number
            plot(1:EXPERIMENT.measure.number, plotData(l, :),'k--', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end 
        for l = EXPERIMENT.taxonomy.us.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number
            plot(1:EXPERIMENT.measure.number, plotData(l, :),'k:', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+EXPERIMENT.taxonomy.sup.gap.number
           plot(1:EXPERIMENT.measure.number, plotData(l, :),'-.', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+EXPERIMENT.taxonomy.sup.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+2*EXPERIMENT.taxonomy.sup.gap.number
           plot(1:EXPERIMENT.measure.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+2*EXPERIMENT.taxonomy.sup.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+3*EXPERIMENT.taxonomy.sup.gap.number
           plot(1:EXPERIMENT.measure.number, plotData(l, :),'-.s', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+3*EXPERIMENT.taxonomy.sup.gap.number+1: EXPERIMENT.taxonomy.approaches
            plot(1:EXPERIMENT.measure.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                           
        end 

            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:EXPERIMENT.measure.number;
        ax1.XTickLabel = Blabels;
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Measure';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
        ax1.XGrid = 'on';
        
        title('Measure*Approach Interaction', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(mapLabels(Alabels), 'Location', 'BestOutside', 'Interpreter', 'none');
        
        figureID = 'apc\intam';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)    
    
        clear plotData;
        
        
currentFigure = figure('Visible', 'off');
    plotData = ie.factorCA.mean.';
    hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.us.gap.number
            plot(1:originalTrackNumber, plotData(l, :),'k--', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end 
        for l = EXPERIMENT.taxonomy.us.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number
            plot(1:originalTrackNumber, plotData(l, :),'k:', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+EXPERIMENT.taxonomy.sup.gap.number
           plot(1:originalTrackNumber, plotData(l, :),'-.', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+EXPERIMENT.taxonomy.sup.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+2*EXPERIMENT.taxonomy.sup.gap.number
           plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+2*EXPERIMENT.taxonomy.sup.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+3*EXPERIMENT.taxonomy.sup.gap.number
           plot(1:originalTrackNumber, plotData(l, :),'-.s', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+3*EXPERIMENT.taxonomy.sup.gap.number+1: EXPERIMENT.taxonomy.approaches
            plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                           
        end 

            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:originalTrackNumber;
        ax1.XTickLabel = Clabels;
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Systems';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
        ax1.XGrid = 'on';
        
        title('System*Approach Interaction', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(mapLabels(Alabels), 'Location', 'BestOutside', 'Interpreter', 'none');
        
        figureID = 'apc\intas';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)    
    
        clear plotData;            
        
   % compute a 3-way ANOVA for RMSE
    [~, rmseTbl, rmseStats] = anovan(data.rmse, {subject, factorA, factorB, factorC}, 'Model', m, ...
        'VarNames', {'Kuple', 'Approach', 'Measure', 'Systems'}, ...
        'alpha', 0.05, 'display', 'off');
    
    df_approach = rmseTbl{3,3};
    ss_approach = rmseTbl{3,2};
    F_approach = rmseTbl{3,6};
    
    df_measure = rmseTbl{4,3};
    ss_measure = rmseTbl{4,2};
    F_measure = rmseTbl{4,6};
    
    df_systems = rmseTbl{5,3};
    ss_systems = rmseTbl{5,2};
    F_systems = rmseTbl{5,6};
    
    df_approach_measure = rmseTbl{6,3};
    ss_approach_measure = rmseTbl{6,2};
    F_approach_measure = rmseTbl{6,6};
    
    df_approach_systems = rmseTbl{7,3};
    ss_approach_systems = rmseTbl{7,2};
    F_approach_systems = rmseTbl{7,6};
    
    df_measure_systems = rmseTbl{8,3};
    ss_measure_systems = rmseTbl{8,2};
    F_measure_systems = rmseTbl{8,6};
    
    ss_error = rmseTbl{9, 2};
    df_error = rmseTbl{9, 3};
    
    ss_total = rmseTbl{10, 2};
    
    
    soa.omega2.rmse.approach = df_approach * (F_approach - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.measure = df_measure * (F_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.systems = df_systems * (F_systems - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.approach_measure = df_approach_measure * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.approach_systems = df_approach_systems * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.measure_systems = df_measure_systems * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    
    
    soa.omega2p.rmse.approach = df_approach * (F_approach - 1) / (df_approach * (F_approach - 1) + N);
    soa.omega2p.rmse.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.rmse.systems = df_systems * (F_systems - 1) / (df_systems * (F_systems - 1) + N);
    soa.omega2p.rmse.approach_measure = df_approach_measure * (F_approach_measure - 1) / (df_approach_measure * (F_approach_measure - 1) + N);
    soa.omega2p.rmse.approach_systems = df_approach_systems * (F_approach_systems - 1) / (df_approach_systems * (F_approach_systems - 1) + N);
    soa.omega2p.rmse.measure_systems = df_measure_systems * (F_measure_systems - 1) / (df_measure_systems * (F_measure_systems - 1) + N);
    
    soa.eta2.rmse.approach = ss_approach / ss_total;
    soa.eta2.rmse.measure = ss_measure / ss_total;
    soa.eta2.rmse.systems = ss_systems / ss_total;
    soa.eta2.rmse.approach_measure = ss_approach_measure / ss_total;
    soa.eta2.rmse.approach_systems = ss_approach_systems / ss_total;
    soa.eta2.rmse.measure_systems = ss_measure_systems / ss_total;
    
    soa.eta2p.rmse.approach = ss_approach / (ss_approach + ss_error);
    soa.eta2p.rmse.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.rmse.systems = ss_systems / (ss_systems + ss_error);
    soa.eta2p.rmse.approach_measure = ss_approach_measure / (ss_approach_measure + ss_error);
    soa.eta2p.rmse.approach_systems = ss_approach_systems / (ss_approach_systems + ss_error);
    soa.eta2p.rmse.measure_systems = ss_measure_systems / (ss_measure_systems + ss_error);    

  
    Slabels = unique(subject, 'stable');
    Alabels = unique(factorA, 'stable');
    Blabels = unique(factorB, 'stable');
    Clabels = unique(factorC, 'stable');
    
    % main effects
    [me.subject.mean, me.subject.ci] = grpstats(data.rmse(:), subject(:), {'mean', 'meanci'});
    [me.factorA.mean, me.factorA.ci] = grpstats(data.rmse(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data.rmse(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data.rmse(:), factorC(:), {'mean', 'meanci'});
    
    % interaction between kuple size (x-axis) and approach (y-axis)
    % each row is an approach, columns are kuple size
    ie.factorSA.mean = grpstats(data.rmse(:), {subject(:),factorA(:)}, {'mean'});
    ie.factorSA.mean = reshape(ie.factorSA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.kuples.number).';
    
    
    % interaction between measure (x-axis) and approach (y-axis)
    % each row is a approach, columns are measures
    ie.factorBA.mean = grpstats(data.rmse(:), {factorB(:), factorA(:)}, {'mean'});
    ie.factorBA.mean = reshape(ie.factorBA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.measure.number).';
        
    % interaction between systems (x-axis) and approach (y-axis)
    % each row is an approch, columns are systems
    ie.factorCA.mean = grpstats(data.rmse(:), {factorC(:), factorA(:)}, {'mean'});
    ie.factorCA.mean = reshape(ie.factorCA.mean, EXPERIMENT.taxonomy.approaches, originalTrackNumber).';

    
    currentFigure = figure('Visible', 'off');

        xTick = 1:EXPERIMENT.taxonomy.approaches;
        plotData.mean = me.factorA.mean.';
        plotData.ciLow = me.factorA.ci(:, 1).';
        plotData.ciHigh = me.factorA.ci(:, 2).';        
        [sorted,I]=sort(plotData.mean);
        a=EXPERIMENT.taxonomy.colors;
        for i=1:EXPERIMENT.taxonomy.approaches;
            bar(i, sorted(i),'facecolor', a(I(i),:),'BarWidth', 0.7);
            axis([0 EXPERIMENT.taxonomy.approaches+1 min(sorted)-0.1  max(sorted)+0.1])
            hold on
        end
        
        er = errorbar(xTick,plotData.mean(I),plotData.ciLow(I)-plotData.mean(I),plotData.mean(I)-plotData.ciHigh(I));    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 
        
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = mapLabels(Alabels(I));
        ax.XTickLabelRotation = 90;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Approach';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RMSE Marginal Mean');
        figureID = 'rmse\maina';

        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 30 20];
        print(currentFigure, '-dpng','-r300', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;        

    currentFigure = figure('Visible', 'off');
    
        xTick = 1:EXPERIMENT.kuples.number;
        plotData.mean = me.subject.mean.';
        plotData.ciLow = me.subject.ci(:, 1).';
        plotData.ciHigh = me.subject.ci(:, 2).';        
    
        bar(xTick, plotData.mean,'BarWidth', 0.7);
        axis([0 EXPERIMENT.kuples.number+1 min(plotData.mean)-0.1  max(plotData.mean)+0.1])
        hold on
        er = errorbar(xTick,plotData.mean,plotData.ciLow-plotData.mean,plotData.mean-plotData.ciHigh);    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 
    
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = Slabels;
        ax.XTickLabelRotation = 90;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Kuple Size';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RMSE Marginal Mean');
        figureID = 'rmse\maink';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 30 20];
        print(currentFigure, '-dpng','-r300', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;        
    
   currentFigure = figure('Visible', 'off');
    
        xTick = 1:EXPERIMENT.measure.number;
        plotData.mean = me.factorB.mean.';
        plotData.ciLow = me.factorB.ci(:, 1).';
        plotData.ciHigh = me.factorB.ci(:, 2).';        
    
        bar(xTick, plotData.mean,'BarWidth', 0.5);
        axis([0 EXPERIMENT.measure.number+1 min(plotData.mean)-0.1  max(plotData.mean)+0.1])
        hold on
        er = errorbar(xTick,plotData.mean,plotData.ciLow-plotData.mean,plotData.mean-plotData.ciHigh);    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 
    
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = Blabels;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Measure';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RMSE Marginal Mean');
        figureID = 'rmse\mainm';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 15 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;
        
        
   currentFigure = figure('Visible', 'off');
    
        xTick = 1:originalTrackNumber;
        plotData.mean = me.factorC.mean.';
        plotData.ciLow = me.factorC.ci(:, 1).';
        plotData.ciHigh = me.factorC.ci(:, 2).';        
        
         bar(xTick, plotData.mean,'BarWidth', 0.5);
        axis([0 originalTrackNumber+1 min(plotData.mean)-0.1  max(plotData.mean)+0.1])
        hold on
        er = errorbar(xTick,plotData.mean,plotData.ciLow-plotData.mean,plotData.mean-plotData.ciHigh);    
        er.Color = [0 0 0];                            
        er.LineStyle = 'none'; 

       ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = Clabels;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'System';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RMSE Marginal Mean');
        figureID = 'rmse\mains';

        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 15 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)
        
        clear plotData;
    
%interaction effects RMSE               
        
    currentFigure = figure('Visible', 'off');
    plotData = ie.factorSA.mean.';
    hold on;
        % for each label
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches-EXPERIMENT.taxonomy.baseline.number
            plot(1:EXPERIMENT.kuples.number, plotData(l, :),EXPERIMENT.taxonomy.pattern{mod(l,EXPERIMENT.taxonomy.sup.gap.number)+1,:}, 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', EXPERIMENT.taxonomy.width(mod(l,EXPERIMENT.taxonomy.sup.gap.number)+1,:));                
        end 
        for l = EXPERIMENT.taxonomy.approaches-EXPERIMENT.taxonomy.baseline.number+1:EXPERIMENT.taxonomy.approaches
            plot(1:EXPERIMENT.kuples.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2);                
        end
            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:EXPERIMENT.kuples.number;
        ax1.XTickLabel = Slabels;
        ax1.XTickLabelRotation = 90;
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'K-uple Size';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('RMSE Marginal Mean');
        ax1.XGrid = 'on';
        
        title('K-uple Size*Approach Interaction', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(mapLabels(Alabels), 'Location', 'BestOutside', 'Interpreter', 'none');
        
        figureID = 'rmse\intak';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 30 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)    
    
        clear plotData;
        
    currentFigure = figure('Visible', 'off');
    plotData = ie.factorBA.mean.';
    hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches-EXPERIMENT.taxonomy.baseline.number
            plot(1:EXPERIMENT.kuples.number, plotData(l, :),EXPERIMENT.taxonomy.pattern{mod(l,EXPERIMENT.taxonomy.sup.gap.number)+1,:}, 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', EXPERIMENT.taxonomy.width(mod(l,EXPERIMENT.taxonomy.sup.gap.number)+1,:));                
        end 
        for l = EXPERIMENT.taxonomy.approaches-EXPERIMENT.taxonomy.baseline.number+1:EXPERIMENT.taxonomy.approaches
            plot(1:EXPERIMENT.kuples.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2);                
        end

            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:EXPERIMENT.measure.number;
        ax1.XTickLabel = Blabels;
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Measure';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('RMSE Marginal Mean');
        ax1.XGrid = 'on';
        
        title('Measure*Approach Interaction', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(mapLabels(Alabels), 'Location', 'BestOutside', 'Interpreter', 'none');
        
        figureID = 'rmse\intam';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)    
    
        clear plotData;
        
        
currentFigure = figure('Visible', 'off');
    plotData = ie.factorCA.mean.';
    hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.us.gap.number
            plot(1:originalTrackNumber, plotData(l, :),'k--', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end 
        for l = EXPERIMENT.taxonomy.us.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number
            plot(1:originalTrackNumber, plotData(l, :),'k:', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+EXPERIMENT.taxonomy.sup.gap.number
           plot(1:originalTrackNumber, plotData(l, :),'-.', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+EXPERIMENT.taxonomy.sup.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+2*EXPERIMENT.taxonomy.sup.gap.number
           plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+2*EXPERIMENT.taxonomy.sup.gap.number+1:2*EXPERIMENT.taxonomy.us.gap.number+3*EXPERIMENT.taxonomy.sup.gap.number
           plot(1:originalTrackNumber, plotData(l, :),'-.s', 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
        end
        for l = 2*EXPERIMENT.taxonomy.us.gap.number+3*EXPERIMENT.taxonomy.sup.gap.number+1: EXPERIMENT.taxonomy.approaches
            plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                           
        end 

            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:originalTrackNumber;
        ax1.XTickLabel = Clabels;
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Systems';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('RMSE Marginal Mean');
        ax1.XGrid = 'on';
        
        title('System*Approach Interaction', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(mapLabels(Alabels), 'Location', 'BestOutside', 'Interpreter', 'none');
        
        figureID = 'rmse\intas';
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 20];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure2(trackID, factors, figureID,topicSet));
        
        close(currentFigure)    
    
        clear plotData;        

        
    anovaID = EXPERIMENT.pattern.identifier.anova.id(factors, shortTrackID);
    
    [path,~]=fileparts(EXPERIMENT.pattern.file.anova(trackID, factors, anovaID,topicSet));
    if (exist (path)~=7)
        mkdir(path)
    end
    
    sersave(EXPERIMENT.pattern.file.anova(trackID, factors, anovaID,topicSet), 'apcTbl', 'apcStats', 'rmseTbl', 'rmseStats', 'soa');
                
    fprintf('\n\n######## Total elapsed time for computing %s ANOVA analyses on collection %s (%s): %s ########\n\n', ...
            upper(factors), EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end



