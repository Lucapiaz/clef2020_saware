%% compute_base_measures
% 
% Computes measures for the base pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_base_measures(startKuple,endKuple)
%  
%
% *Parameters*
%
% * *|startKuple|* - the index of the start kuple.
% * *|endKuple|* - the index of the end kuple.
%
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = compute_base_measures(startKuple,endKuple)
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    % turn on logging

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s measures on collection %s (%s) ########\n\n', EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


    start = tic;
    fprintf('  - loading the dataset\n');

    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID));
    for s=startKuple:min(EXPERIMENT.kuples.samples,endKuple)
        serload(EXPERIMENT.pattern.file.goldPoolRed(trackID, EXPERIMENT.(trackID).shortID,s));
        serload(EXPERIMENT.pattern.file.assessorRed(trackID, EXPERIMENT.(trackID).shortID,s));
    end
    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    for s=startKuple:min(EXPERIMENT.kuples.samples,endKuple)
        % Adding the gold pool label and identifier to run everything in one cycle
        poolLabels = [{sprintf('gold_%04d',s)} eval(sprintf('poollabels_%04d',s))];
        poolIdentifiers = [{EXPERIMENT.pattern.identifier.goldPoolRed(EXPERIMENT.(trackID).shortID,s)} eval(sprintf('poolidentifiers_%04d',s))];

        % for each pool
        for p = 1:length(poolLabels)

            startPool = tic;

            fprintf('\n+ Pool: %s\n', poolIdentifiers{p});   

            % for each runset
            for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number

                fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});

                % for each measure
                for m = 1:EXPERIMENT.measure.number

                    start = tic;

                    fprintf('    * computing %s\n', EXPERIMENT.measure.name{m});
                    mid=EXPERIMENT.measure.id{m};               %ap, ndcg,....
                    shortTrack=EXPERIMENT.(trackID).shortID;    %T21
                    shortRunset=EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r}; %T08, T13

                    currentPool=eval(poolIdentifiers{p});

                    runsetId=EXPERIMENT.pattern.identifier.runSet(EXPERIMENT.(trackID).shortID, EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                    currentRunset=eval(runsetId);

                    measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, poolLabels{p} , trackID, shortRunset,[]);
                    % es measureID= ap_base_GOLD_pT21rT08

                    measure=EXPERIMENT.command.measure.(mid)(currentPool,currentRunset, poolLabels{p});

                    [path,~]=fileparts(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]));
                    if (exist (path)~=7)
                        mkdir(path)
                    end
                    sersave2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]), ...
                        'WorkspaceVarNames', {'measure'},'FileVarNames', {measureID});

                    % free space
                    clear('currentPool','currentRunset','measure');

                    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

                end % for measures

                % free the assess cache
                clear assess

            end % for runset


            fprintf('  - elapsed time for pool %s: %s\n', poolIdentifiers{p}, elapsedToHourMinutesSeconds(toc(startPool)));

        end; % for pools

    end
    fprintf('\n\n######## Total elapsed time for computing %s measures on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
