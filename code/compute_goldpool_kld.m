%% compute_goldpool_kld
% 
% Compute the KL-divergence on gold pool and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_rndpool_kld(trackID, tag, topicSet)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|granularity|* - the granularity of the coefficients, either sgl or tpc.
% * *|aggregation|* - the aggregation of the coefficients, either msr or gp.
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_goldpool_kld(trackID, tag, topicSet)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    
    % setup common parameters
    common_parameters;          
    
    % Log file
    delete(EXPERIMENT.pattern.logFile.computePoolMeasuresGap('kld', 'gold', trackID,topicSet));
    diary(EXPERIMENT.pattern.logFile.computePoolMeasuresGap('kld', 'gold', trackID,topicSet));
    
    
    %% start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Computing KL divergence to gold on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));

    
    % local version of the general configuration parameters
    shortTrack = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    
    originalTrackShortID = cell(1, originalTrackNumber);
    
    % for each runset
    for r = 1:originalTrackNumber        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    end
  
    
    %% load assessor labels and assessor number
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'poolLabels', 'P');

    
    fprintf('+ Loading PDF of assessor measures\n');
    
    % the assessor measures 
    assessorMeasures = cell(EXPERIMENT.measure.number, P, originalTrackNumber);
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
                
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            % for each assessor
            for p = 1:P
                
                pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, poolLabels{p}, ...
                    trackID,  originalTrackShortID{r},topicSet);
                                
                serload2(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', pdfID,topicSet),'FileVarNames',{pdfID}, ...
                    'WorkspaceVarNames',{'measure'});
                assessorMeasures{m, p, r}=measure;
                
                clear measure;
                
            end % for assessor
        end % for measure
    end % for runset
    
    clear r m p;
            
    
    fprintf('\n+ Loading PDF of gold measures\n');
    
    goldMeasures = cell(EXPERIMENT.measure.number, originalTrackNumber);  
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        fprintf('    ');

        measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{1}, EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, trackID,  originalTrackShortID{r},[]);
        serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]),'FileVarNames',{ measureID},'WorkspaceVarNames',{'temp'});
        % number of runs
        R=width(temp);
        clear temp;
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
                
            pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, 'base', 'GOLD', trackID, originalTrackShortID{r},topicSet);

            serload2(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', pdfID,topicSet),'FileVarNames',{pdfID}, ...
            'WorkspaceVarNames',{'measure'});
            goldMeasures{m, r} = measure;

            clear measure;
            
            fprintf('.');
            
        end % for measure
        
        fprintf('\n');
        
    end % for runset
    
    clear r m p;
    %%
    fprintf('\n+ Computing KL divergence with gold measures\n');
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        % the total number of topics
        T = size(assessorMeasures{1, 1, r}, 1);
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * %s\n', EXPERIMENT.measure.id{m});
            fprintf('      ');
            
            kldValues=array2table(NaN(1,P));
            kldValues.Properties.VariableNames=poolLabels;
            
            % for each assessor pool
            for ap = 1:P
                
                % the assessor measures
                assessor = assessorMeasures{m, ap, r};
                %disp('gold')
                %disp(goldMeasures{m,r})
                %disp('assessor')
                %disp(assessor)
                kldValues{1,ap}=EXPERIMENT.command.goldPool.gap.kld(assessor, goldMeasures{m, r});
                kldValues{1,ap}=EXPERIMENT.command.normalize.kld(kldValues{1,ap},T,R);
                kldValues{1,ap}=EXPERIMENT.tag.(tag).weight.command(kldValues{1,ap});
            end
                
                gapID = EXPERIMENT.pattern.identifier.pm('kld', EXPERIMENT.measure.id{m}, 'gold', EXPERIMENT.tag.(tag).weight.id, trackID, originalTrackShortID{r},topicSet);
                
                [path,~]=fileparts(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', gapID,topicSet));
                if (exist (path)~=7)
                    mkdir(path);
                end

                sersave2(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', gapID,topicSet), ...
                    'WorkspaceVarNames', {'kldValues' },'FileVarNames', {gapID});
                
                fprintf('.');
                
                clear assessor gapID;
                                                
            fprintf('\n');
            
        end % for each measure
        
    end % for runset
    
    fprintf('\n\n######## Total elapsed time for computing KL divergence to gold on collection %s (%s): %s ########\n\n', ...
             EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end
