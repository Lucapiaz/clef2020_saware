%% majorityVote
% 
% Generates a pool from input ones by using a majority vote algorithm.

%% Synopsis
%
%   [mvPool] = majorityVote(identifier, varargin)
%  
%
%   It assumes binary relevance, i.e. only NotRelevant and Relevant degrees
%   are allowed.
%
%
% *Parameters*
%
% * *|identifier|* - the identifier to be assigned to the majority vote
% pool;
% * *|varargin|* - the pools to be merged into the majority vote one.
%
% *Returns*
%
% * |mvPool|  - the majority vote pool.

%% References
% 
% Please refer to:
%
% * Hosseini, M., Cox, I. J., MilicFrayling, N., Kazai, G., and Vinay, V. (2012).
% On Aggregating Labels from Multiple Crowd Workers to Infer Relevance of Documents.
% In Baeza-Yaetes, R., de Vries, A. P., Zaragoza, H., Cambazoglu, B. B., Murdock,
% V., Lempel, R., and Silvestri, F., editors, 
% _Advances in Information Retrieval. Proc. 32nd European Conference on IR Research (ECIR 2012)_, 
% pages 182-194. Lecture Notes in Computer Sci- ence (LNCS) 7224, Springer, Heidelberg, Germany.
% * Kazai, G., Kamps, J., Koolen, M., and Milic-Frayling, N. (2011). 
% Crowd-sourcing for Book Search Evaluation: Impact of HIT Design on Comparative System Ranking. 
% In Ma, W.-Y., Nie, J.-Y., Baeza-Yaetes, R., Chua, T.-S., and Croft, W. B., editors, 
% _Proc. 34th Annual International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR 2011)_, 
% pages 205-214. ACM Press, New York, USA.
% 
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [mvPool] = majorityVotePool(identifier, varargin)

    % check that identifier is a non-empty string
    validateattributes(identifier,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'Identifier');

     if iscell(identifier)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(identifier) && numel(identifier) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    identifier = char(strtrim(identifier));
    identifier = identifier(:).';

    % check that the identifier is ok according to the matlab rules
    if ~isempty(regexp(identifier, '(^[0-9_])?\W*', 'once'))
        error('MATTERS:IllegalArgument', 'Identifier %s is not valid: identifiers can contain only letters, numbers, and the underscore character and they must start with a letter.', ...
            identifier);
    end  

    % number of pools to be merged
    P = length(varargin);

    % number of topics
    T = height(varargin{1});

    % use one of the input pools as a template for the majority vote one
    mvPool = varargin{1};
    mvPool.Properties.UserData.identifier = identifier;
    mvPool.Properties.VariableNames = {identifier};
    
    for t = 1:T
        
        currentTopic = mvPool{t, 1}{1, 1}; %per wrappare tabella in tabella
        
        % merge the documents of all the pools
        for p = 2:P
            
            % check that all the input pools have the same number of topics
            assert(height(varargin{p}) == T, 'All the input pools must have the same number of topics.');
            
            % merge the documents. 
            % N.B. if a document has different relevance degrees in
            % different pools, it will appear more than once since the
            % pairs (documents, relevance degree) are different.
            currentTopic = union(currentTopic, varargin{p}{t, 1}{1, 1}, 'rows');
            
        end; % for each pool
        
        % remove duplicate documents due to union of the same document with
        % different relevance degrees
        % only doc names, not rel judgements!!
        [~, ia] = unique(currentTopic(:, 1));        
        currentTopic = currentTopic(ia, :);
        
        % a matrix DxP where each row is a document in the merged pool and
        % each column is the relevance degree (0 for NotRelevant, 1 for
        % Relevant) in one of the input pools
        % here we have all relevance judgements according to the crowd pools 
        mv = NaN(height(currentTopic), P);
        
        % collect the data about the pools
        for p = 1:P
            
            % find where the documents of the p-th pool are in the
            % currentTopic (locb)
            [lia, locb] = ismember(varargin{p}{t, 1}{1, 1}{:, 1}, currentTopic{:, 1});
            
            % find the relevant documents of the p-th pool
            rel = varargin{p}{t, 1}{1, 1}.RelevanceDegree == 'Relevant';
            
            % set rows of the current topic corresponding to the documents
            % of the p-th pool to 0 for NotRelevant and 1 for Relevant 
            mv(locb, p) = double(rel(lia));
                        
        end; % for each pool
        
        % NaN indicates that some pool did not contain a vote for a given
        % document
        if any(isnan(mv))
            fprintf('There are documents without votes for topic %s\n', mvPool.Properties.RowNames{t});
        end;
        
        % compute the number of votes for each relevance degree for each
        % document
        mv = histc(mv.', 0:1).';
        
        % find where not relevant wins
        v = (mv(:, 1) > mv(:, 2));
        currentTopic{v, 'RelevanceDegree'} = {'NotRelevant'};
     
           
        % find where relevant wins
        v = (mv(:, 1) < mv(:, 2));
        currentTopic{v, 'RelevanceDegree'} = {'Relevant'};
        
        % find the positions where relevant and not relevant tie
        v = find(mv(:, 1) == mv(:, 2));
        
        % generate random relevant and not relevant assessments for the
        % tied documents
        tmp = randi(2, size(v, 1), 1) - 1;
        
        % find where the relevant documents are
        rel = (tmp == 1);
        
        % assign relevant and not relevant degrees to the tied documents
        currentTopic{v(rel), 'RelevanceDegree'} = {'Relevant'};
        currentTopic{v(~rel), 'RelevanceDegree'} = {'NotRelevant'};
        
        % substitute the current topic into the majority vote pool
        mvPool{t, 1}{1, 1} = currentTopic;
    end; % for each topic

end

