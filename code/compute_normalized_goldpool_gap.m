%% compute_normalized_goldpool_gap
% 
% Compute a gap on gold pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_goldpool_gap(trackID, tag, gap, topicSet)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|gap|* - the gap to be computed.
% * *|topicSet|* - the topic used to compute the gap measure.
%
% *Returns*
%
% Nothing
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_normalized_goldpool_gap(trackID, tag, topicSet)
    
    % setup common parameters
    common_parameters;
    
    gap=EXPERIMENT.tag.(tag).gap.id;
    if nargin==2
        topicSet=[];
    end
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    

    % check that gap is a non-empty string
    validateattributes(gap,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'gap');

    if iscell(gap)
        % check that gap is a cell array of strings with one element
        assert(iscellstr(gap) && numel(gap) == 1, ...
            'MATTERS:IllegalArgument', 'Expected gap to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    gap = char(strtrim(gap));
    gap = gap(:).';
    

    
    % Log file
    delete(EXPERIMENT.pattern.logFile.computePoolMeasuresGap(gap, 'gold', trackID,topicSet));
    diary(EXPERIMENT.pattern.logFile.computePoolMeasuresGap(gap, 'gold', trackID, topicSet));
    
    
    shortTrack=EXPERIMENT.(trackID).shortID;
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'T');
    
    if isempty(topicSet)
        excludedTopics=[];
    else
        excludedTopics=1:T;
        excludedTopics(ismember(excludedTopics,topicSet))=[];
    end
    %% start of overall computations
    
    startComputation = tic;
    
    
    fprintf('\n\n######## Computing %s gap to gold standard on collection %s (%s) ########\n\n', gap, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    % local version of the general configuration parameters
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    % for each runset
    for r = 1:originalTrackNumber        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    end
  
    %% load assessor labels and assessor number
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'poolLabels', 'P');
    
    fprintf('+ Loading assessor measures\n');
    
    %% load the assessor measures 
    assessorMeasures = cell(EXPERIMENT.measure.number, P, originalTrackNumber); %measure-assessor-runset
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
                
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            mid=EXPERIMENT.measure.id{m};
            % for each assessor
            for p = 1:P
                
                measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, poolLabels{p}, trackID,  originalTrackShortID{r},[]);
                serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]),'FileVarNames',{measureID},'WorkspaceVarNames',{'measure'});
                
                assessorMeasures{m, p, r}=measure{:, :};
                assessorMeasures{m, p, r}(excludedTopics,:)=[]; %remove topics not incuded in the set

                clear measure;
                
            end % for assessor
        end % for measure
    end % for runset
    
    clear r m p measureID
    
    %% load the gold measures
    
    fprintf('\n+ Loading gold measures\n');
    
    goldMeasures = cell(EXPERIMENT.measure.number, originalTrackNumber);
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        fprintf('    ');
           % for each measure
        for m = 1:EXPERIMENT.measure.number

            measureID = EXPERIMENT.pattern.identifier.measure(mid,EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool , trackID, originalTrackShortID{r},[]);
            serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]),'FileVarNames',{measureID},'WorkspaceVarNames',{'measure'});
            goldMeasures{m, r}=measure{:, :};
            goldMeasures{m, r}(excludedTopics,:)=[]; %remove topics not incuded in the set

            clear measure;
        end % for measure
        fprintf('\n');
    end % for runset
    
    clear r m p measureID
    
    %% gap computation
    
    fprintf('\n+ Computing %s gap with gold measures\n', gap);
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        % number of topics
        T = size(goldMeasures{1, r}, 1);
        
        % number of runs
        R = size(goldMeasures{1, r}, 2);
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            mid=EXPERIMENT.measure.id{m};
            fprintf('    * %s\n', mid);
            fprintf('      ');
                
            gold(:, :) = goldMeasures{m, r};
            gapValues=array2table(NaN(1,P));
            gapValues.Properties.VariableNames=poolLabels;
            
            % for each assessor pool
            for ap = 1:P

                % the assessor measures
                assessor = assessorMeasures{m, ap, r};
                gapValues{1,ap}=EXPERIMENT.command.goldPool.gap.(gap)(assessor, gold);
                gapValues{1,ap}=EXPERIMENT.tag.(tag).gap.normalization(gapValues{1,ap},T,R);
                gapValues{1,ap}=EXPERIMENT.tag.(tag).weight.command(gapValues{1,ap});
                fprintf('.');

                clear assessor;
                    
            end % for each assessor

            gapID = EXPERIMENT.pattern.identifier.pm(gap, mid, 'gold', EXPERIMENT.tag.(tag).weight.id, trackID, originalTrackShortID{r},topicSet);
            [path,~]=fileparts(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', gapID,topicSet));
            if (exist (path)~=7)
                mkdir(path);
            end
            sersave2(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', gapID,topicSet), 'WorkspaceVarNames', {'gapValues' },'FileVarNames', {gapID});
            
            
            clear gold gapValues;   
            fprintf('\n');      
        end % for each measure
    end % for runset
    
    fprintf('\n\n######## Total elapsed time for computing %s gap to gold standard on collection %s (%s): %s ########\n\n', gap, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;
    
end
