%% compute_aware_unsupervised_mono_measures
% 
% Computes and saves AWARE mono-feature supervised measures.

%% Synopsis
%
%   [] = compute_aware_mono_unsupervised_measures(trackID, tag, topicSet)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|topicset| - set of considered topic indexes
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_aware_supervised_mono_measures(trackID, tag, topicSet)
    
    % check that topicset is a subset of topics                           
    assert(size(topicSet,1)>0, 'MATTERS:IllegalArgument', 'Expected topiSet to be a subset of topics, cannot be empty or contain all topics');
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that identifier is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

     if iscell(tag)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % setup common parameters
    common_parameters;
    gap=EXPERIMENT.tag.(tag).gap.id;
    weight=EXPERIMENT.tag.(tag).weight.id;
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.computeMeasures(trackID,tag, topicSet));
    diary(EXPERIMENT.pattern.logFile.computeMeasures(trackID,tag,topicSet));

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s aware supervised mono-feature measures on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - scoring scheme %s\n', tag);

    % local version of the common parameters
    relativePath = EXPERIMENT.tag.(tag).relativePath;
    shortTrack = EXPERIMENT.(trackID).shortID;
        
    % Load the number of pools and topics
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'P', 'T', 'poolLabels');
    
    
    % load the kuples
    serload(EXPERIMENT.pattern.file.kuples(trackID, shortTrack));

    
    if isempty(topicSet)
        excludedTopics=[];
        topicSetFull=1:T;
    else
        topicSetFull=topicSet;
        excludedTopics=1:T;
        excludedTopics(ismember(excludedTopics,topicSet))=[];
    end

    
    
    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number

        originalTrackID = EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r};
        fprintf('\n+ Original track of the runs: %s\n', originalTrackID);
        originalTrackShortID = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            start = tic;
            
            fprintf('  - computing %s %s\n', tag, EXPERIMENT.measure.name{m});
            
            fprintf('    * loading base measures\n');
            
            measures = cell(1, P);
            for p = 1:P
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, poolLabels{p}, trackID,  originalTrackShortID,[]);
                serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]), measureID);            
                eval(sprintf('measures{%1$d} = %2$s(%3$s,:);', p, measureID,'topicSetFull'));   
                %disp('misure su topicset')
                %disp(measures{p});           
                clear(measureID);
            end
            clear measureID
            
            % number of topics
            T=height(measures{1});
            % number of runs
            R = width(measures{1});

            fprintf('    * computing assessor scores\n'); %use weight computed on excluded topics
            
            gapID = EXPERIMENT.pattern.identifier.pm(gap, EXPERIMENT.measure.id{m}, 'gold', weight, trackID,  originalTrackShortID,excludedTopics);
            
            serload2(EXPERIMENT.pattern.file.pre_sup(trackID,'supervised', gapID,excludedTopics), 'WorkspaceVarNames', {'gapValues' },'FileVarNames', {gapID})
            
            %replicate weights for the number of topics in the topicSet 
            A=repmat(gapValues{:,:},T,1);
            %disp('pesi ripetuti')
            %disp(A);
            assert(size(A, 1) == T, 'The assessor weight matrix has %s rows instead of %d.', size(A, 1), T);
            assert(size(A, 2) == P, 'The assessor weight matrix has %s columns instead of %d.', size(A, 2), P);
                        
            fprintf('    * processing measures\n');
            
            %% merge assesors
            % for each k-uple
            %% merge assesors
            % for each k-uple
            for kk = 1:EXPERIMENT.kuples.number
                
                fprintf('      # k-uples: k%02d \n', EXPERIMENT.kuples.sizes(kk));
                
                
                
                %evalf(EXPERIMENT.command.aware, {'A', kuplesIdentifiers{k}, 'm', 'measures'}, {measureID});  
                
                
                % generate the weights for the assessors 
                % 1) move the a plane to the third dimension with reshape(a, T, 1, M)
                % 2) repeat the moved a plane for each run with repmat(*, 1, R, 1) 
                w = repmat(reshape(A, T, 1, P), 1, R, 1); %dimensions: topic-run-crowdassessor
                eval(strcat('K=',kuplesIdentifiers{kk},';'));
                awareMeasures = NaN(T,R, size(K,1));
                
                for k = 1:size(K,1)
                    currentKuple=NaN(T,R,P);
                    serload(EXPERIMENT.pattern.file.assessorRed(trackID, EXPERIMENT.(trackID).shortID,k));
                    poolLabels = eval(sprintf('poollabels_%04d',k));
                    poolIdentifiers = eval(sprintf('poolidentifiers_%04d',k));
                    %fprintf('    * loading %d base measures\n',k);
                    for p = 1:P
                        if(ismember(p,K(k, :)))
                            measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, poolLabels{p}, ...
                                    EXPERIMENT.(trackID).id,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r},[]);
                            serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID,[]), measureID);            
                            eval(sprintf('currentKuple(:,:,p) = %2$s{%3$s,:};', p, measureID,'topicSet'));                                  
                            clear(measureID);
                        end
                    end
                    % the current k-uple, i.e. the k-th subplane
                    currentKuple = currentKuple(:, :, K(k, :));

                    % The weights w are taken in the same order as the kuple so
                    % that each assessor always receives the same relative weight,
                    % whatever his/her position in the kuple
                    %disp('w')
                    currentW = w(:, :, K(k, :));

                    % Normalize the weight along 3rd dimension so that they sum up
                    % to 1
                    % 1) compute the sum along the third dimension sum(currentW, 3)
                    % 2) replicate it by the number of actually combined assessors in
                    % the third dimension repmat(*, 1, 1, A)
                    % 3) divide
                    currentW = currentW ./ repmat(sum(currentW, 3), 1, 1, size(K, 2));
                    %disp(currentW(1,1,:));
                    % create an weighted aggregated measure across the planes
                    currentKuple = dot(currentKuple, currentW, 3);

                    % save the current kuple for output
                    awareMeasures(:, :, k) = currentKuple;
                end % for each kuple
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(kk), ...
                    trackID,  originalTrackShortID,topicSet);
                
                %evalf(EXPERIMENT.command.aware, {'A', kuplesIdentifiers{k}, 'measures'}, {measureID});  
                
                [path,~]=fileparts(EXPERIMENT.pattern.file.measure(trackID, relativePath, tag, measureID,topicSet));
                if (exist (path)~=7)
                    mkdir(path)
                end    
                sersave2(EXPERIMENT.pattern.file.measure(trackID, relativePath, tag, measureID, topicSet),'WorkspaceVarNames',{'awareMeasures'},'FileVarNames',{measureID});
               
                clear(measureID);
            end % k-uple size
            
            fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            
        end % for measures
        
    end % for runset


    fprintf('\n\n######## Total elapsed time for computing %s aware mono-feature unsupervised measures on collection %s (%s): %s ########\n\n', ...
            tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));    
    
    diary off;     
    

end
