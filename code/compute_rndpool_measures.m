%% compute_rndpool_measures
% 
% Compute measure based on random pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_rndpool_measures(tag, startSample, endSample)
%  
%
% *Parameters*
%
% * *|tag|* - the tag of the conducted experiment.
% * *|startSample|* - the index of the start random pool. Optional.
% * *|endSample|* - the index of the end random pool. Optional.
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rndpool_measures(tag, startSample, endSample)
    nt=maxNumCompThreads(15);
    % check that tag is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

     if iscell(tag)
        % check that tag is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    if nargin == 3
        validateattributes(startSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.analysis.rndPoolsSamples}, '', 'startSample');
        
        validateattributes(endSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startSample, '<=', EXPERIMENT.analysis.rndPoolsSamples}, '', 'endSample');
    else 
        startSample = 1;
        endSample = EXPERIMENT.analysis.rndPoolsSamples;
    end
    
    
   % turn on logging
    delete(EXPERIMENT.pattern.logFile.computePoolMeasures(tag, startSample, endSample, 0, 0, trackID,[]));
    diary(EXPERIMENT.pattern.logFile.computePoolMeasures(tag, startSample, endSample, 0, 0, trackID,[]));


    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s measures on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start sample %d\n', startSample);
    fprintf('    * end sample %d\n', endSample);

    start = tic;
    fprintf('  - loading data sets \n');

   % local version of the general configuration parameters
    relativePath = EXPERIMENT.tag.(tag).relativePath;
    shortTrack = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    
    runSetIdentifiers = cell(1, originalTrackNumber);
    originalTrackShortID = cell(1, originalTrackNumber);
    
    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        runSetIdentifiers{r} = EXPERIMENT.pattern.identifier.runSet(shortTrack, originalTrackShortID{r});
        
        % load the run set
        serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), runSetIdentifiers{r});
    end
     
    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));


    % for each pool
    for p = startSample:endSample
        
        startPool = tic;
        sampledPool = EXPERIMENT.pattern.identifier.sampledRndPool(p);
        shortNameSuffix = sprintf('%1$s_s%2$04d_%3$s', tag, p, trackID);
        
        poolID = EXPERIMENT.pattern.identifier.pool(tag, sampledPool, shortTrack);
        
        fprintf('\n+ Pool: %s\n', poolID);
        
        % load the pool
        serload(EXPERIMENT.pattern.file.pool(trackID, tag, poolID), poolID);
        
         % for each runset
            for r = 1:originalTrackNumber
                
                fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});

                % for each measure
                for m = 1:EXPERIMENT.measure.number
                   
                    start = tic;
                    fprintf('    * computing %s %s\n', tag, EXPERIMENT.measure.name{m});
                    mid=EXPERIMENT.measure.id{m};
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(mid, tag, sampledPool, trackID, originalTrackShortID{r},[]);

                    currentPool=eval(poolID);
                    currentRunset=eval(runSetIdentifiers{r});
                    measure=EXPERIMENT.command.measure.(mid)(currentPool,currentRunset, shortNameSuffix);
                
                    [path,~]=fileparts(EXPERIMENT.pattern.file.measure(trackID, relativePath, tag, measureID,[]));
                    if (exist (path)~=7)
                        mkdir(path)
                    end
                    sersave2(EXPERIMENT.pattern.file.measure(trackID, relativePath, tag, measureID,[]), ...
                        'WorkspaceVarNames', {'measure'},'FileVarNames', {measureID});
                    
                    % free space
                    clear('currentPool','currentRunset','measure');
                    
                    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                    
                end % for measures
                
                % free the assess cache
                clear assess
        
            end % for runset
        
        fprintf('  - elapsed time for pool %s: %s\n', poolID, elapsedToHourMinutesSeconds(toc(startPool)));
        
        clear(poolID);

    end % for pool


    fprintf('\n\n######## Total elapsed time for computing %s measures on collection %s (%s): %s ########\n\n', ...
            tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
