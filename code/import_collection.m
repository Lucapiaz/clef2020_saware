%% import_collection
%
% Imports the requested collection and saves it to a |.mat| file.
%
%% Synopsis
%
%   [] = import_collection(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = import_collection()
    % set up the common parameters
    common_parameters
    trackID=EXPERIMENT.fast.trackID;
    % disable warnings, not needed for bulk import
    warning('off');

    % turn on logging
    delete(EXPERIMENT.pattern.logFile.importCollection(trackID));
    diary(EXPERIMENT.pattern.logFile.importCollection(trackID));

    % start of overall import
    startImport = tic;

    fprintf('\n\n######## Importing collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - imported on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - path %s\n\n', EXPERIMENT.(trackID).collection.path.base);


    % import the gold standard pool
    start = tic;

    fprintf('+ Importing the gold standard pool\n');
    
    % creating local input parameters for import
    fileName = EXPERIMENT.(trackID).collection.file.goldPool;
    id = EXPERIMENT.pattern.identifier.goldPool(EXPERIMENT.(trackID).shortID);%id='pool_base_GOLD_T21'
    
    % importing the pool
    % executes importPoolFromFileTrecFormat(<filename>, <id>,<binary rel>,<delimiter=space>) as specified in common_parameters
    evalf(EXPERIMENT.command.(trackID).importGoldPool, {'fileName', 'id'}, {id});

    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    % the total number of topics in the collection
    %-----------------------------------------%
    % height returns the # of rows, evalf evaluates the function height
    % with parameter id, and puts the result into T
    evalf(@height, {id}, {'T'}) 
    %-----------------------------------------%
    fprintf('  - total number of topics: %d\n\n', T);

    % the list of required topics
    evalf(@(x) x.Properties.RowNames, {id}, {'requiredTopics'})
    
    disp('required topics')
     disp(size(requiredTopics))
    disp(requiredTopics)
    % import the participant pools
    start = tic;

    fprintf('+ Importing the participant pools\n');

    % return the list of text files in the directory. It ignores non-text
    % files.
    files = dir(sprintf('%1$s%2$s', EXPERIMENT.(trackID).collection.path.crowd, '*.txt'));

    % the total number of pools/assessors
    P = length(files);

    % extract an horizontal cell array of file names
    [poolFiles{1:P}] = deal(files.name);

    % the labels used to prefix the pool identifiers (same as file name but
    % without extension)
    poolLabels = cellfun(@(s) {s(1:end-4)}, poolFiles, 'UniformOutput', true);

    % the identifiers of the imported pools
    poolIdentifiers = cellfun(@(s) {EXPERIMENT.pattern.identifier.pool(EXPERIMENT.tag.base.id, s, EXPERIMENT.(trackID).shortID)}, poolLabels, 'UniformOutput', true);

    % concatenate the file names with the directory
    poolFiles = cellfun(@(s) {sprintf('%1$s%2$s', EXPERIMENT.(trackID).collection.path.crowd, s)}, poolFiles, 'UniformOutput', true);

    % import the pools from participants
    for p = 1:P

        % creating local input parameters for import
        fileName = poolFiles{p};
        id =  poolIdentifiers{p};
        
        evalf(EXPERIMENT.command.(trackID).importCrowdPool, {'fileName','id', 'requiredTopics'}, {id});
        
    end;

    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
    fprintf('  - total number of pools: %d\n\n', P);


    % import the runs in trec_eval document ordering
    fprintf('+ Importing the run set according to the standard trec_eval document ordering\n');

    runSetIdentifiers = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    % import each run set
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number

        start = tic;

        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        runSetIdentifiers{r} = EXPERIMENT.pattern.identifier.runSet(EXPERIMENT.(trackID).shortID, EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});

        % creating local input parameters for import
        fileName = EXPERIMENT.(trackID).collection.path.runSet(EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        id =  runSetIdentifiers{r};
        
        evalf(EXPERIMENT.command.(trackID).importRunSet, {'fileName', 'id', 'requiredTopics'}, {id});
          
        fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

        evalf(@width, {id}, {'R'})    
        fprintf('    * total number of runs: %d\n', R);
    end;
    fprintf('\n');

    start = tic;

    fprintf('+ Saving the data set\n');
    [path,~] =fileparts(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID));
    if (exist (path)~=7)
        mkdir(path)
    end
    sersave(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'poolLabels', 'poolIdentifiers', 'T', 'P', ...
            runSetIdentifiers{:}, ...
            EXPERIMENT.pattern.identifier.goldPool(EXPERIMENT.(trackID).shortID), ...
            poolIdentifiers{:});

    fprintf('  - elapsed time: %s\n\n', elapsedToHourMinutesSeconds(toc(start)));


    fprintf('\n\n######## Total elapsed time for importing collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startImport)));

    % stop logging
    diary off;

    % re-enable warnings
    warning('on');

end


