%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



function RGB = hex(HEX,n)
    HEX=HEX(2:end);
    HEX = cellstr(HEX) ;
    HEX = upper(HEX) ;
    tf = cellfun(@(S) numel(S)==3, HEX) ;
    HEX(tf) = cellfun(@(S) S([1 1 2 2 3 3]), HEX(tf),'un',0) ;
    HEX = double(char(HEX)) ;
    tf = HEX > 64 ; % letters
    HEX(~tf) = HEX(~tf) - '0' ;     % numbers:  0-9
    HEX(tf)  = HEX(tf) - 'A' + 10 ; % letters: 10-15
    % reshape in two rows, each column is a byte (xy)
    % the first 3 columns are the RGB values of the first column, etc.
    HEX = reshape(HEX.',2,[]) ;
    % [x ; y] -> [16*x ; y] 
    HEX(1,:) = 16 * HEX(1,:) ;
    % -> 16*x+y
    RGBc = sum(HEX,1) ; 
    % reshape in RGB triplet
    RGBc = reshape(RGBc,3,[]) ; % color triplets
    RGBc = RGBc.' ; % we want color as rows
    RGBc= RGBc./255;
    RGBc=[RGBc(1),RGBc(2),RGBc(3)];
    RGB=[];
    for i=1:n
        RGB=cat(1,RGB,RGBc);
    end
end
