%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


function[]=run_all_gold_precompute(topicSet)
    if nargin==0
        topicSet=[];
    end
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    gap = EXPERIMENT.taxonomy.sup.gap.list;
    excl={'pdf','kld'};
    gap=setdiff(gap,excl);
    weight=EXPERIMENT.taxonomy.sup.weight.list;
    
    tag = @(g,w) sprintf('aw_sup_mono_%s_%s', g, w);
    for gp = 1:length(gap)
        for w = 1:length(weight)
            compute_normalized_goldpool_gap(trackID, tag(gap{gp},weight{w}) ,topicSet)
        end
    end
    
    clear aggregation granularity gap gp rnd agg rp grt;
end