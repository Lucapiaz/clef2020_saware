%% compute_em_pools
% 
% Compute the expectation maximization pools for and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_em_pools(tag)
%  
%
% *Parameters*
%
% * *|tag|* - the tag of the conducted experiment.
%
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = compute_em_pools(tag)
    % check that identifier is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

     if iscell(tag)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.computePools(tag, trackID));
    diary(EXPERIMENT.pattern.logFile.computePools(tag, trackID));


    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s pools on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - max iterations %d\n', EXPERIMENT.em.maxIterations);
    fprintf('  - tolerance %1.0e\n\n', EXPERIMENT.em.tolerance);

    start = tic;

    fprintf('+ Loading data\n');
    
    % load the kuples
    serload(EXPERIMENT.pattern.file.kuples(trackID, EXPERIMENT.(trackID).shortID));

    % load the pools identifiers
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'P', 'poolIdentifiers');
    
    %load gold pool
    serload2(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'WorkspaceVarNames',{'goldpool'},'FileVarNames',{EXPERIMENT.pattern.identifier.goldPool(EXPERIMENT.(EXPERIMENT.fast.trackID).shortID)});
    % load the pools
    for s=1:EXPERIMENT.kuples.samples
        serload(EXPERIMENT.pattern.file.goldPoolRed(trackID, EXPERIMENT.(trackID).shortID,s));
        serload(EXPERIMENT.pattern.file.assessorRed(trackID, EXPERIMENT.(trackID).shortID,s));
    end
    
    fprintf('    * %d pool(s) loaded\n', P);
    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
    fprintf('+ Computing %s pools\n', tag);
    
    general=tic; %general timer
    
    % for each k-uple
    for k = 1:EXPERIMENT.kuples.number
        start = tic;

        currentKuples=eval(kuplesIdentifiers{k});
        
        remaining=(toc(general))/(k-1)*(EXPERIMENT.kuples.number-k+1);
        fprintf('  - k-uples: k%02d -  remaining time: %s \n', EXPERIMENT.kuples.sizes(k),elapsedToHourMinutesSeconds(remaining));
        
        for kk = 1:size(currentKuples, 1)
            poolLabels = eval(sprintf('poollabels_%04d',kk));
            poolIdentifiers = eval(sprintf('poolidentifiers_%04d',kk));
            
            poolID = EXPERIMENT.pattern.identifier.pool(tag, ...
                   EXPERIMENT.pattern.identifier.sampledKuple(EXPERIMENT.kuples.sizes(k), kk), ...
                   EXPERIMENT.(trackID).shortID);
            disp(poolIdentifiers(currentKuples(kk, :)));
            accuracyID = EXPERIMENT.pattern.identifier.accuracy(tag, ...
                   EXPERIMENT.pattern.identifier.sampledKuple(EXPERIMENT.kuples.sizes(k), kk), ...
                   EXPERIMENT.(trackID).shortID);
            if(strcmp(tag,'emsemi2'))
            goldtrainID=strcat('training',EXPERIMENT.pattern.identifier.goldPoolRed(EXPERIMENT.(trackID).shortID,kk));
            
            evalf(EXPERIMENT.command.emPools.compute, ...
                [{'tag', 'poolID',goldtrainID}, poolIdentifiers(currentKuples(kk, :))], ...
                {poolID, accuracyID});
            else
                evalf(EXPERIMENT.command.emPools.compute, ...
                [{'tag', 'poolID'}, poolIdentifiers(currentKuples(kk, :))], ...
                {poolID, accuracyID});
            end
            [path,~]=fileparts(EXPERIMENT.pattern.file.pool(trackID, tag, poolID));
            if (exist (path)~=7)
                mkdir(path)
            end
            sersave(EXPERIMENT.pattern.file.pool(trackID, tag, poolID), poolID(:), accuracyID(:));
            clear(poolID, accuracyID);
        end

        fprintf('     * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));


    end


    fprintf('\n\n######## Total elapsed time for computing %s pools on collection %s (%s): %s ########\n\n', ...
            tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
