%% analyse_base_measures
% 
% Analyses the base measures at different k-uples sizes and saves 
% them to a |.mat| file.
%
%% Synopsis
%
%   [] = analyse_base_measures()
%
% *Returns*
%
% Nothing
%
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = analyse_base_measures()
% setup common parameters
common_parameters;
trackID=EXPERIMENT.fast.trackID;
tag=EXPERIMENT.tag.base.id;
shortTrack= EXPERIMENT.(trackID).shortID;
relativePath=EXPERIMENT.tag.base.relativePath;

% turn on logging
delete(EXPERIMENT.pattern.logFile.analyseMeasures(trackID, tag, NaN, NaN,[]));
diary(EXPERIMENT.pattern.logFile.analyseMeasures(trackID, tag, NaN, NaN,[]));

% start of overall computations
startComputation = tic;

fprintf('\n\n######## Analysing %s measures on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

fprintf('+ Settings\n');
fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
fprintf('  - confidence interval alpha %f\n', EXPERIMENT.analysis.ciAlpha);
fprintf('  - AP correlation ties samples %d\n', EXPERIMENT.analysis.apcorrTiesSamples);

% load data
serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'poolLabels', 'poolIdentifiers', 'T', 'P');

% for each runset
for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
    shortRunset=EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    
    fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number
        mid=EXPERIMENT.measure.id{m};
     
        start = tic;        
        fprintf('  - analysing %s %s\n', tag, EXPERIMENT.measure.name{m});
        
        % loading the gold standard measure
        goldMeasureID = EXPERIMENT.pattern.identifier.measure(mid, tag, EXPERIMENT.label.goldPool, trackID,  shortRunset,[]);
        
        fprintf('    * loading gold standard measure: %s\n', goldMeasureID);
        serload(EXPERIMENT.pattern.file.measure(trackID, relativePath, tag, goldMeasureID,[]), goldMeasureID);
        
        % determine the total number of runs
        evalf(@width, {goldMeasureID}, {'R'})
        
        
        % the raw data, each TxR plane is a measure for T topics and R runs
        % to be weighted by assessors' scores and the P planes correspond
        % to the different assessors
        data = NaN(T, R, P);
        
        fprintf('    * loading assessor measures\n');
        % for each pool
        for p = 1:P
            assessorMeasureID = EXPERIMENT.pattern.identifier.measure(mid, tag, poolLabels{p}, trackID,  shortRunset,[]);
            serload(EXPERIMENT.pattern.file.measure(trackID, relativePath, tag, assessorMeasureID,[]), assessorMeasureID);
            
            data(:, :, p)=eval(sprintf('%1$s{:, :};', assessorMeasureID));
            clear(assessorMeasureID);
        end;
        clear assessorMeasureID;
        
        
        fprintf('    * analysing measures\n');
        
        %compute analysis (calls analyse_kuples)
        goldMeasures=eval(goldMeasureID);
        [rmse,tau,apc]=EXPERIMENT.command.analyse(goldMeasures,data);

        % tags for saving data
        measureID = EXPERIMENT.pattern.identifier.measure(mid, tag, tag, trackID,  shortRunset,[]);
        tauID =  EXPERIMENT.pattern.identifier.pm('tau', mid, tag, tag, trackID,  shortRunset,[]);
        apcID =  EXPERIMENT.pattern.identifier.pm('apc', mid, tag, tag, trackID,  shortRunset,[]);
        rmseID = EXPERIMENT.pattern.identifier.pm('rmse', mid, tag, tag, trackID,  shortRunset,[]);
        
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.analysis(trackID, relativePath, tag, measureID,[]));
        if (exist (path)~=7)
            mkdir(path)
        end
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, relativePath, tag, measureID,[]), ...
            'WorkspaceVarNames', {'rmse', 'tau', 'apc' },'FileVarNames', {rmseID,tauID, apcID});
        
        clear(goldMeasureID, 'tau', 'apc', 'rmse','data');
        clear('tauID','rmseID','apcID','measureID','goldMeasures');
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end; % for each measure
    
end; % for each run set


    fprintf('\n\n######## Total elapsed time for analysing %s measures on collection %s (%s): %s ########\n\n', ...
        tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

end
