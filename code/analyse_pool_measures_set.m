%% analyse_pool_measures_set
% 
% Analyses measures based on learned pools, e.g. majority vote at different 
% k-uples sizes, using a subset of the topics. Saves them to a |.mat| file.

%% Synopsis
%
%   [] = analyse_pool_measures_set(tag,topicSet,startKupleSet,endKupleSet)
%  
%
% *Parameters*
%
% * *|tag|* - the tag of the conducted experiment.
% * *|topicSet|* - list of topic indexes used for analysis
% * *|startKupleSet|* - the index of the start kuples set. Optional.
% * *|endKupleSet|* - the index of the end kuples set. Optional.
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = analyse_pool_measures_set(tag, topicSet, startKupleSet,endKupleSet)  
    % check that identifier is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');
    
    if iscell(tag)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    if nargin==2
        startKupleSet = 1;
        endKupleSet = EXPERIMENT.kuples.number;
    end
    
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.analyseMeasures(trackID,tag,startKupleSet+1,endKupleSet +1,topicSet));
    diary(EXPERIMENT.pattern.logFile.analyseMeasures(trackID, tag,startKupleSet+1,endKupleSet +1,topicSet));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Analysing %s measures on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - confidence interval alpha %f\n', EXPERIMENT.analysis.ciAlpha);
    fprintf('  - AP correlation ties samples %d\n', EXPERIMENT.analysis.apcorrTiesSamples);
    
    % local version of the general configuration parameters
    shortTrack = EXPERIMENT.(trackID).shortID;
    tagBase= EXPERIMENT.tag.base.id;
    relativePath=EXPERIMENT.tag.base.relativePath;
    
    % load the kuples
    serload(EXPERIMENT.pattern.file.kuples(trackID, shortTrack));
    
    % load data
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'T');
    
    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
        shortRunset= EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            mid=EXPERIMENT.measure.id{m};
            
            startMeasure = tic;
            fprintf('  - analysing %s %s\n', tagBase, EXPERIMENT.measure.name{m});
            
            % loading the gold standard measure            
            kp=1;
            measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, sprintf('gold_%04d',kp) , trackID, shortRunset,[]);
            serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]), ...
                        'WorkspaceVarNames', {'test'},'FileVarNames', {measureID});
            goldmeasuresTot=NaN(size(test,1),size(test,2),EXPERIMENT.kuples.samples);
            goldmeasuresTot(:,:,kp)=test{:,:};
            R=width(test);
            
            for kp = 2:EXPERIMENT.kuples.samples
                measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, sprintf('gold_%04d',kp) , trackID, shortRunset,[]);
                if (exist(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]), 'file') == 2)
                    serload2(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id , measureID,[]), ...
                        'WorkspaceVarNames', {'test'},'FileVarNames', {measureID});
                    goldmeasuresTot(:,:,kp)=test{:,:};
                end
            end 
            
            % for each k-uple size
            for k = startKupleSet:endKupleSet
                currentKupleSize=EXPERIMENT.kuples.sizes(k);
                currentKuples=eval(kuplesIdentifiers{k});
                
                start = tic;
                
                fprintf('    * k-uples: k%02d \n', currentKupleSize);
                
                % determine the size of the current kuple 
                % (how many samples)
                KK=EXPERIMENT.command.computePoolMeasures.kupleSize(currentKuples);
                
                % the raw data, each TxR plane is a measure for T topics and R runs
                % to be weighted by assessors' scores and the KK planes correspond
                % to the different pools (one for every kuple sample) 
                data = NaN(T, R, KK);
                
                % for each k-uple in the set copy the corresponding measure
                for kk = 1:KK
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(mid, tag, ...
                    EXPERIMENT.pattern.identifier.sampledKuple(currentKupleSize, kk), trackID, shortRunset,[]);
                    
                    serload(EXPERIMENT.pattern.file.measure(trackID,relativePath, tag, measureID,[]), measureID);
                    
                    data(:, :, kk)=eval(sprintf('%1$s{:, :};', measureID));
                    clear(measureID);
                end
                
                [rmse,tau,apc]=EXPERIMENT.command.analyse_set(goldmeasuresTot(:,:,1:KK),data,topicSet);
                
                % tags for saving data
                measureID = EXPERIMENT.pattern.identifier.measure(mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);
                tauID = EXPERIMENT.pattern.identifier.pm('tau', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);
                apcID = EXPERIMENT.pattern.identifier.pm('apc', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);
                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', mid, tag, EXPERIMENT.pattern.identifier.kuple(k), trackID,  shortRunset,topicSet);
                               
                [path,~]=fileparts(EXPERIMENT.pattern.file.analysis(trackID, relativePath, tag, measureID,topicSet));
                if (exist (path)~=7)
                    mkdir(path)
                end
                sersave2(EXPERIMENT.pattern.file.analysis(trackID, relativePath, tag, measureID,topicSet), ...
                    'WorkspaceVarNames', {'rmse', 'tau', 'apc' },'FileVarNames', {rmseID,tauID, apcID});

                clear('tau', 'apc', 'rmse','data');
                clear('tauID','rmseID','apcID','measureID','goldMeasures');
                               
                fprintf('      # elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            end % k-uple size
            
            clear('goldmeasuresTot');
            
            fprintf('  - elapsed time for measure %s %s: %s\n', tag, EXPERIMENT.measure.name{m}, elapsedToHourMinutesSeconds(toc(startMeasure)));
            
        end % for each measure
        
    end % for each runset
    

    fprintf('\n\n######## Total elapsed time for analysing %s measures on collection %s (%s): %s ########\n\n', ...
        tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;


end
