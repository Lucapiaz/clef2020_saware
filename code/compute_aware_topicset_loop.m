%% compute_aware_topicset_loop
% 
% Computes aware loop for each topicset
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



function []=compute_aware_topicset_loop(gran,startTopicset, endTopicset, gap, measure, analysis)
    common_parameters;
    fprintf('\n\n######## Computing aware loop for topicset_idx %d-%d(%s) ########\n\n', startTopicset,endTopicset, EXPERIMENT.label.paper);
    fprintf('+ Settings\n');
    fprintf('  - gap %d\n', gap);
    fprintf('  - measure %d \n', measure);
    fprintf('  - analysis %d \n', analysis);
    startTL = tic;
    trackID=EXPERIMENT.fast.trackID;
    serload(EXPERIMENT.pattern.file.topicset(EXPERIMENT.(trackID).id, EXPERIMENT.(trackID).shortID));
    serload(EXPERIMENT.pattern.file.dataset(EXPERIMENT.(trackID).id, EXPERIMENT.(trackID).shortID), 'T');
    for t= startTopicset:endTopicset

        currentTopicset=topicSets(t,:);
        fprintf('##### computing unsupervised loop for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
        
        excludedTopics=1:T;
        excludedTopics(ismember(excludedTopics,currentTopicset))=[];

        if (gap==1)
            fprintf('# computing gap measures for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
            run_all_precompute(currentTopicset);
            run_all_pdf(currentTopicset);
            run_all_kld(currentTopicset);
            fprintf('\n\n###Elapsed time: %s ###\n', elapsedToHourMinutesSeconds(toc(startTL)));
        end
        if (measure==1)
            fprintf('# computing aware loop for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
            compute_aware_loop(currentTopicset);
            fprintf('\n\n###Elapsed time: %s ###\n', elapsedToHourMinutesSeconds(toc(startTL)));
        end
        if (analysis==1)
            fprintf('# computing aware analysis loop for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
            compute_aware_analysis_loop(gran,currentTopicset);
        end
    end
    fprintf('\n\n######## Total elapsed time for computing aware loop for topicset_idx %d-%d (%s): %s ########\n\n', ...
            startTopicset,endTopicset, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startTL)));
end