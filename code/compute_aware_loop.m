%% compute_aware_loop
% 
% executes all the merged measures given a topicset
%
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

function []=compute_aware_loop(topicSet)
    if nargin==0
        topicSet=[];
    end
    common_parameters
    trackID=EXPERIMENT.fast.trackID;
    aggregation = EXPERIMENT.taxonomy.us.aggregation.list;
    granularity = EXPERIMENT.taxonomy.us.granularity.list;
    gap = EXPERIMENT.taxonomy.us.gap.list;
    weight = EXPERIMENT.taxonomy.us.weight.list;

    name = @(granularity, aggregation, gap, weight, left_rnd, right_rnd) ...
        sprintf('aw_us_mono_%s_%s_%s_%s', granularity, aggregation, gap, weight);

    for grt = 1:length(granularity)
        for agg = 1:length(aggregation)
            for w = 1:length(weight)
                for gp = 1:length(gap)
                    tag = name(granularity{grt}, aggregation{agg}, gap{gp}, weight{w});
                    compute_aware_unsupervised_mono_measures(trackID, tag, topicSet);
                end
            end
        end
    end
end
