%% aw_uni
% 
% Compute aw_uni weights.

%% Synopsis
%
%   [A] = aw_uni(DATA,topicset)
%  
%
% *Parameters*
%
% * *|DATA|* - a struct containing all the needed information.
% * *|topicset|* - the list of topic indexes to be used for acuracy computation
%
% *Returns*
%
% * *|A|* - a TxP matrix indicating the weight of P assessors for T topics.

%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [A] = aw_uni(DATA,topicSet)

    % check that the function is invoked by the expected caller
    st = dbstack(1);
    %assert(strcmpi(st.name, DATA.EXPERIMENT.tag.(DATA.tag).score.callerFunction), '%s must be invoked by %s and not by %s.', DATA.tag, DATA.EXPERIMENT.tag.(DATA.tag).score.callerFunction, st.name);
        
    % uniform weights to all assessors for all topics
    A = ones(DATA.T, DATA.P);
        
    % save the computed weights        
    eval(sprintf('%1$s = array2table(A);', DATA.weightID));        
    eval(sprintf('%1$s.Properties.RowNames = DATA.topics;', DATA.weightID));        
    eval(sprintf('%1$s.Properties.VariableNames = DATA.poolLabels;', DATA.weightID));
    [path,~]=fileparts(DATA.EXPERIMENT.pattern.file.measure(DATA.trackID, DATA.relativePath, DATA.tag, DATA.weightID,topicSet));
    if (exist (path)~=7)
        mkdir(path)
    end    
    sersave(DATA.EXPERIMENT.pattern.file.measure(DATA.trackID, DATA.relativePath, DATA.tag, DATA.weightID,topicSet), DATA.weightID(:));
end
