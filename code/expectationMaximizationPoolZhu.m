%% expectationMaximizationPool
% 
% Generates a pool from input ones by using an expectation maximization
% algorithm.

%% Synopsis
%
%   [emPool, accuracy] = expectationMaximizationPoolNeu(tolerance, maxIterations, identifier, varargin)
%  
%
%   It assumes binary relevance, i.e. only NotRelevant and Relevant degrees
%   are allowed.
%
%
% *Parameters*
%
% * *|tolerance|* tolerance to decide when the Expectation Maximization
% converges;
% * *|maxIterations|* maximum number of iterations;
% * *|identifier|* - the identifier to be assigned to the expectation
% maximization pool;
% * *|varargin|* - the pools to be merged into the expectation maximization 
% one.
%
% *Returns*
%
% * *|emPool|* - the expectation maximization pool.
% * *|accuracy|* - the accuracy of the assessors.

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [emPool, accuracy] = expectationMaximizationPoolZhu(tolerance, maxIterations, identifier, varargin)

    % check that tolerance is a nonempty "real" scalar greater than 0 
    validateattributes(tolerance, {'numeric'}, ...
        {'nonempty', 'real', '>', 0}, '', 'tolerance');

    % check that the maximum number of iterations is a nonempty  
    % integer value greater than 0
    validateattributes(maxIterations, {'numeric'}, ...
            {'nonempty', 'integer', '>', 0}, '', 'maxIterations');

    % check that identifier is a non-empty string
    validateattributes(identifier,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'Identifier');

     if iscell(identifier)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(identifier) && numel(identifier) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    identifier = char(strtrim(identifier));
    identifier = identifier(:).';

    % check that the identifier is ok according to the matlab rules
    if ~isempty(regexp(identifier, '(^[0-9_])?\W*', 'once'))
        error('MATTERS:IllegalArgument', 'Identifier %s is not valid: identifiers can contain only letters, numbers, and the underscore character and they must start with a letter.', ...
            identifier);
    end  

    % number of pools to be merged, i.e. number of workers
    % P = length(varargin);
    M = length(varargin);

    % number of topics
    T = height(varargin{1});

    % use one of the input pools as a template for the expectation maximization one
    emPool = varargin{1};
    emPool.Properties.UserData.identifier = identifier;
    emPool.Properties.VariableNames = {identifier};

    
    % initialize the cell array that contains the accuracies
    accuracy = varargin{1};
    accuracy.Properties.UserData.identifier = identifier;
    accuracy.Properties.VariableNames = {identifier};
    
    for t = 1:T
        
        currentTopic = emPool{t, 1}{1, 1};
        
        % merge the documents of all the pools
        % for p = 2:P
        for j = 2:M
            
            % check that all the input pools have the same number of topics
            assert(height(varargin{j}) == T, 'All the input pools must have the same number of topics.');
            
            % merge the documents. 
            % N.B. if a document has different relevance degrees in
            % different pools, it will appear more than once since the
            % pairs (documents, relevance degree) are different.
            currentTopic = union(currentTopic, varargin{j}{t, 1}{1, 1}, 'rows');
            
        end; % for each pool
        
        % remove duplicate documents due to union of the same document with
        % different relevance degrees
        [~, ia] = unique(currentTopic(:, 1));        
        currentTopic = currentTopic(ia, :);
        
        % a matrix DxP where each row is a document in the merged pool and
        % each column is the relevance degree (0 for NotRelevant, 1 for
        % Relevant) in one of the input pools
        em = NaN(height(currentTopic), M);
                
        % Number of documents
        N = height(currentTopic);        
                
        % Number of relevance degrees, not relevant are not considered
        G = 1;        
        
        % n (G+1)xNxM matrix, n_gij = 1 if the worker j labels the document 
        % i as g
        n = zeros(G+1, N, M);
        
        % collect the data about the pools
        for j = 1:M
            
            % find where the documents of the p-th pool are in the
            % currentTopic (locb)
            [lia, locb] = ismember(varargin{j}{t, 1}{1, 1}{:, 1}, currentTopic{:, 1});
            
            % find the relevant documents of the p-th pool
            rel = varargin{j}{t, 1}{1, 1}.RelevanceDegree == 'Relevant';
            
            % set rows of the current topic corresponding to the documents
            % of the p-th pool to 0 for NotRelevant and 1 for Relevant 
            em(locb, j) = double(rel(lia));
            
            % definition of the matrix n, only for two relevance grades
            n(1, :, j) = ones(1, N) - em(:, j).';
            n(2, :, j) = em(:, j).';            
                        
        end; % for each pool
        
        em(isnan(em)) = 0;
      
        % substitute NaN values with zeros, because if the worker j does 
        % not judge the document i n(:, i, j) = 0
        n(isnan(n)) = 0;
        
        % Permute the dimensions of n for coputations in while loop
        nPerm = permute(n, [3 1 2]);
        
        % NaN indicates that some pool did not contain a vote for a given
        % document
        if sum(sum(isnan(em)))
            fprintf('There are documents without votes for topic %s\n', emPool.Properties.RowNames{t});
        end;
        
        % initialize worker confidence
        cwplus=ones(1,M);
        cwminus=ones(1,M);
        softlabels=NaN(height(currentTopic), 2); %first column for positive soft labels, second for negative
        hardlabels=NaN(height(currentTopic), 1);
        % EM Algorithm
        iteration = 0;
        convergence = 100;
        while ( (convergence > tolerance) && (iteration < maxIterations) )
            iteration = iteration + 1;
            
            % STEP 1
            % Compute aggregated soft labels With EQ2: discrimination
            % between positive and negative label quality
            oldcplus=cwplus;
            oldcminus=cwminus;
            %sum confidence for assessors who voted Relevant, for each doc
            softlabels(:,1)=em*cwplus.';
            %sum confidence for assessors who voted notRelevant,foreach doc
            negsum=(1-em)*cwminus.';
            %compute positive labels as ratio between relevant votes and
            %total, weighted by worker confidence
            softlabels(:,1)=softlabels(:,1)./(softlabels(:,1)+negsum);
            %compute negative labels
            softlabels(:,2)=1-softlabels(:,1);
            %compute hard labels (most probable label between positive and
            %negative soft labels)
            hardlabels(:,1)=softlabels(:,1)>=softlabels(:,2);
            
            % STEP 2
            % compute workers confidence according to EQ 4-5
            tp=softlabels(:,1).'*em;
            fp=softlabels(:,2).'*em;
            tn=softlabels(:,2).'*(1-em);
            fn=softlabels(:,1).'*(1-em);
            
            cwplus=tp./(tp+fp);
            cwminus=tn./(tn+fn);
            
           
            absoluteError = [abs(oldcplus-cwplus),abs(oldcminus-cwminus)];
            % Set the convergenge parameter to the maximal error
            convergence = max(absoluteError(:));
        end % end of while

        %fprintf('Topic: %d\n', t);
        %fprintf('Number of iterations: %d\n', iteration);
        %fprintf('Convergence parameter: %g\n', convergence);
        if (iteration >= maxIterations)
            fprintf('While loop terminates before reaching the tolerance threshold for topic %s\n', emPool.Properties.RowNames{t});
            fprintf('Convergence parameter: %g\n', convergence);
        end;

           
        currentTopic{:, 'RelevanceDegree'} = {'NotRelevant'};     
           
        % find relevant documents
        v = find(hardlabels==1);
        currentTopic{v, 'RelevanceDegree'} = {'Relevant'};
                
        % substitute the current topic into the majority vote pool
        emPool{t, 1}{1, 1} = currentTopic;
        
        % store the accuracy weights
        accuracy{t, 1}{1, 1} = (cwplus+cwminus)/2;
        
    end; % for each topic

end

