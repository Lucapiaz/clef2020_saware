%% generate_topicset
% 
% Generates the topicset to be used for supervised experimentation and saves
% them to a |.mat| file.
%
%% Synopsis
%
%   [] = generate_topicset(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = generate_topicset

   
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.generateTopicsets(trackID));
    diary(EXPERIMENT.pattern.logFile.generateTopicsets(trackID));

    % start of overall computations
    startComputation = tic;
    
    % Load the total number of pools
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'T');
    
    fprintf('\n\n######## Computing and sampling topicset on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - computing %s topicsets fo size %s \n', num2str(EXPERIMENT.topicset.number),EXPERIMENT.topicset.size);
    fprintf('  - total number of topics %d \n', T);

    fprintf('\n+ Computing topicsets\n');

    % the identifiers of the generated k-uples
    topicsets = cell(1, EXPERIMENT.topicset.number);

    % the indexes of the pools for generating k-uples
    topicIdx = 1:T;

    start = tic;

    % total number of possible topicsets of the selected size
    allT = nchoosek(T, EXPERIMENT.topicset.size);
    % number of topicsets to be actually sampled
    samples = min([EXPERIMENT.topicset.number allT]);
    fprintf('  -  topicset size %d, total available topics %d, total possible topocsets %d, topicset to be sampled %d\n', EXPERIMENT.topicset.size, T, allT, samples);
    % generate all the possible topicsets and sample\
    topicsets= EXPERIMENT.command.topicset.generate(topicIdx,EXPERIMENT.topicset.size);
    samp=EXPERIMENT.command.topicset.sample(topicsets,allT,samples);
    r=randperm(size(samp,1));
    topicSets=samp(r,:);


    fprintf('     * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    
    fprintf('  -  saving the computed topicsets\n');
    [path,~]=fileparts(EXPERIMENT.pattern.file.topicset(trackID, EXPERIMENT.(trackID).shortID));
    if (exist (path)~=7)
       mkdir(path)
    end
    sersave(EXPERIMENT.pattern.file.topicset(trackID, EXPERIMENT.(trackID).shortID),topicSets);


    fprintf('\n\n######## Total elapsed time for computing and sampling topicsets on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
