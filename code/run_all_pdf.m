%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



function[]=run_all_pdf(topicSet)
    if nargin==0
        topicSet=[];
    end
    common_parameters
    trackID=EXPERIMENT.fast.trackID;
    aggregation = EXPERIMENT.taxonomy.us.aggregation.list;
    granularity = EXPERIMENT.taxonomy.us.granularity.list;
    rnd = {'rnd005','rnd050', 'rnd095'};


    for agg = 1:length(aggregation)
        for grt = 1:length(granularity)
            for rp = 1:length(rnd)
                compute_rndpool_pdf(trackID, rnd{rp}, granularity{grt}, aggregation{agg},topicSet);
            end
            compute_base_pdf(trackID, granularity{grt}, aggregation{agg},topicSet);
        end
    end

    clear aggregation granularity rnd agg rp grt;
end