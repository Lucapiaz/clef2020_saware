%% generate_kuples
% 
% Generates the k-uples to be used for subsequent experimentation and saves
% them to a |.mat| file.
%
%% Synopsis
%
%   [] = generate_kuples(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = generate_kuples
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.generateKuples(trackID));
    diary(EXPERIMENT.pattern.logFile.generateKuples(trackID));

    % start of overall computations
    startComputation = tic;
    
    % Load the total number of pools
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'P');
    
    fprintf('\n\n######## Computing and sampling k-uples on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - pools to be merged %s \n', num2str(EXPERIMENT.kuples.sizes, '%d '));
    fprintf('  - total number of pools %d \n', P);
    fprintf('  - maximum number of k-uples to be sampled %d\n', EXPERIMENT.kuples.samples);

    fprintf('\n+ Computing and sampling k-uples\n');

    % the identifiers of the generated k-uples
    kuplesIdentifiers = cell(1, EXPERIMENT.kuples.number);

    % the indexes of the pools for generating k-uples
    poolsIdx = 1:P;

    % for each k-uple size
    for k = 1:EXPERIMENT.kuples.number

        start = tic;

        % total number of possible kuples of the selected size
        K = nchoosek(P, EXPERIMENT.kuples.sizes(k));
        
        % number of kuples to be actually sampled
        samples = min([EXPERIMENT.kuples.samples K]);

        fprintf('  -  k-uple size %d, total available pools %d, total k-uples %d, k-uples to be sampled %d\n', EXPERIMENT.kuples.sizes(k), P, K, samples);

        kuplesIdentifiers{k} = EXPERIMENT.pattern.identifier.kuples(EXPERIMENT.kuples.sizes(k), EXPERIMENT.(trackID).shortID);

        % generate all the possible k-uples
        kupleSize = EXPERIMENT.kuples.sizes(k);
        evalf(EXPERIMENT.command.kuples.generate, ...
              {'poolsIdx', 'kupleSize'}, ...
              kuplesIdentifiers(k));
        
        % randomly sample kuples
        evalf(EXPERIMENT.command.kuples.sample, ...
            {kuplesIdentifiers{k}, 'K', 'samples'}, ...
            kuplesIdentifiers(k));

        fprintf('     * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    end; % k-uple size

    fprintf('  -  saving the computed k-uples\n');
    [path,~]=fileparts(EXPERIMENT.pattern.file.kuples(trackID, EXPERIMENT.(trackID).shortID));
    if (exist (path)~=7)
       mkdir(path)
    end
    sersave(EXPERIMENT.pattern.file.kuples(trackID, EXPERIMENT.(trackID).shortID), 'kuplesIdentifiers', kuplesIdentifiers{:})


    fprintf('\n\n######## Total elapsed time for computing and sampling k-uples on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
