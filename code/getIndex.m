%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>



function idx = getIndex( topicsetpath,topicset,numtopics )
    idx="";
    serload(topicsetpath);
    if size(topicSets,2)==size(topicset,2)
        idx = sprintf('%03d',find(ismember(topicSets, topicset,'rows')));
    end
    if idx==""
        excludedTopics=(1:numtopics);
        excludedTopics(ismember(excludedTopics,topicset))=[];
        if size(topicSets,2)==size(excludedTopics,2)
            idx = sprintf('%03d_comp',find(ismember(topicSets, excludedTopics,'rows')));
            if idx==""
                idx=int2str(-2);
            end
        else
            %disp (topicset)
            %disp(excludedTopics)
            idx=int2str(-1);
        end
    end
    
    if isempty(topicset)
        idx="";
    end
end

