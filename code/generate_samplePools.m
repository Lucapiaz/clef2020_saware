%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = generate_samplePools(trainingrate)
 
    % setup common parameters
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    
    % start of overall computations
    startComputation = tic;
    
    % Load the total number of pools
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID));
    
    fprintf('\n\n######## Computing and sampled pools (gold and base) on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - total number of topics %d \n', T);

    numkuples=EXPERIMENT.kuples.samples;
    for i=1:numkuples
        fprintf('\n+ Computing goldpool\n');
        goldPoolID=EXPERIMENT.pattern.identifier.goldPool(EXPERIMENT.(trackID).shortID);
        training=eval(goldPoolID);
        test=eval(goldPoolID);
        for t=1:T
            N=size(training{t,1}{1,1},1);       %number of documents in gold pool
            randidx=randperm(N);            %random permutation of doc indexes
            trainidx=randidx(1:fix(N*trainingrate/100));  %select rate% of documents for training
            testidx=randidx(fix(N*trainingrate/100)+1:N); %remaining documents for test
            training{t,1}{1,1}=training{t,1}{1,1}(trainidx,:);
            test{t,1}{1,1}=test{t,1}{1,1}(testidx,:);
        end
        
        fprintf('  -  saving the computed goldpool\n');
        [path,~]=fileparts(EXPERIMENT.pattern.file.goldPoolRed(trackID,EXPERIMENT.(trackID).shortID,i));
        if (exist (path)~=7)
           mkdir(path)
        end
        testID=EXPERIMENT.pattern.identifier.goldPoolRed(EXPERIMENT.(trackID).shortID,i);
        sersave2(EXPERIMENT.pattern.file.goldPoolRed(trackID,EXPERIMENT.(trackID).shortID,i),'WorkspaceVarNames',{'training','test'},'FileVarNames',{strcat('training',testID),testID});
    
        fprintf('\n+ Computing assessors\n');
        poolid=cell(1,P);
        poolidtrain=cell(1,P);
        poollab=cell(1,P);
        poollabtrain=cell(1,P);
        for p=1:P
            poolID=EXPERIMENT.pattern.identifier.pool('base',poolLabels{p},EXPERIMENT.(trackID).shortID);
            poollab{1,p}=sprintf('%1$s%2$03d',poolLabels{p},i);
            poollabtrain{1,p}=sprintf('train%1$s%2$03d',poolLabels{p},i);
            poolid{1,p}=EXPERIMENT.pattern.identifier.assessorRed(poolID,i);
            poolidtrain{1,p}=strcat('training',EXPERIMENT.pattern.identifier.assessorRed(poolID,i));
            asstraining=eval(poolID);
            asstest=eval(poolID);
            for t=1:T
                [lia, locb] = ismember(training{t, 1}{1,1}{:,1},asstraining{t,1}{1,1}{:,1});
                asstraining{t,1}{1,1}=asstraining{t,1}{1,1}(locb,:);
                [lia, locb] = ismember(test{t, 1}{1,1}{:,1},asstest{t,1}{1,1}{:,1});
                asstest{t,1}{1,1}=asstest{t,1}{1,1}(locb,:);
                
            end
            eval(strcat(poolid{1,p},'=asstest'));
            eval(strcat(poolidtrain{1,p},'=asstraining')); 
        end
        [path,~]=fileparts(EXPERIMENT.pattern.file.assessorRed(trackID,EXPERIMENT.(trackID).shortID,i));
        if (exist (path)~=7)
           mkdir(path)
        end
       sersave2(EXPERIMENT.pattern.file.assessorRed(trackID,EXPERIMENT.(trackID).shortID,i), ...
           'WorkspaceVarNames',{poolid{:}, 'poollab', 'poolid',poolidtrain{:},'poollabtrain','poolidtrain','P'}, ...
           'FileVarNames',{poolid{:}, sprintf('poollabels_%04d',i),sprintf('poolidentifiers_%04d',i),poolidtrain{:},sprintf('trainingpoollabels_%04d',i),sprintf('trainingpoolidentifiers_%04d',i),'P'});
    end

    fprintf('\n\n######## Total elapsed time for computing and sampling topicsets on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

end
