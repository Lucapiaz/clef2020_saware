%% compute_baseline_topicset_loop
% 
% Analyses measures based on learned pools, e.g. majority vote at different 
% k-uples sizes, using a subset of the topics. Saves them to a |.mat| file.

%% Synopsis
%
%   [] = compute_baseline_topicset_loop(tag, startTopicset, endTopicset)
%  
%
% *Parameters*
%
% * *|tag|* - the tag of the conducted experiment.
% * *|startTopicSet|* - first topicset
% * *|endTopicSet|* - last topicset
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function []=compute_baseline_topicset_loop(tag, startTopicset, endTopicset)
    common_parameters;
    trackID=EXPERIMENT.fast.trackID;
    serload(EXPERIMENT.pattern.file.topicset(EXPERIMENT.(trackID).id, EXPERIMENT.(trackID).shortID));
    serload(EXPERIMENT.pattern.file.dataset(EXPERIMENT.(trackID).id, EXPERIMENT.(trackID).shortID), 'T');
    for t= startTopicset:endTopicset

        currentTopicset=topicSets(t,:);
        fprintf('##### computing %s loop for topicset: %s \n', tag, EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
        fprintf('# computing analysis for topicset: %s \n', EXPERIMENT.pattern.identifier.topicidx.folder(trackID,currentTopicset));
        analyse_pool_measures_set(tag,currentTopicset);
    end
end