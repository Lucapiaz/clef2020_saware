%% aw_us_mono_tpc_msr
% 
% Compute aware unsupervised mono-feature weights with granularity topic
% and aggregation measure.

%% Synopsis
%
%   [A] = aw_us_mono_tpc_msr(DATA, topicset)
%  
%
% *Parameters*
%
% * *|DATA|* - a struct containing all the needed information.
% * *|topicset|* - the list of topic indexes to be used for acuracy computation
%
%
% *Returns*
%
% * *|A|* - a TxP matrix indicating the weight of P assessors for T topics.

%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [A] = aw_us_mono_tpc_msr(DATA,topicSet)

    fprintf('    * weighting %s gap with %s, %s, and %s measures\n', DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.EXPERIMENT.tag.(DATA.tag).und, DATA.EXPERIMENT.tag.(DATA.tag).uni, DATA.EXPERIMENT.tag.(DATA.tag).ovr);
    
    % same weight for all topics
    und = ones(DATA.T, DATA.P);
    uni = ones(DATA.T, DATA.P);
    ovr = ones(DATA.T, DATA.P);
    
     % for each assessor pool
    for ap = 1:DATA.P
        
        undID = DATA.EXPERIMENT.pattern.identifier.pm(DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.measure, DATA.EXPERIMENT.tag.(DATA.tag).und, DATA.poolLabels{ap}, DATA.trackID, DATA.originalTrackShortID,topicSet);
        serload(DATA.EXPERIMENT.pattern.file.pre(DATA.trackID,'unsupervised', DATA.EXPERIMENT.tag.(DATA.tag).granularity.id, DATA.EXPERIMENT.tag.(DATA.tag).aggregation.id, DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.EXPERIMENT.tag.(DATA.tag).und, undID,topicSet), undID);
        
        uniID = DATA.EXPERIMENT.pattern.identifier.pm(DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.measure, DATA.EXPERIMENT.tag.(DATA.tag).uni, DATA.poolLabels{ap}, DATA.trackID, DATA.originalTrackShortID,topicSet);
        serload(DATA.EXPERIMENT.pattern.file.pre(DATA.trackID,'unsupervised',DATA.EXPERIMENT.tag.(DATA.tag).granularity.id, DATA.EXPERIMENT.tag.(DATA.tag).aggregation.id, DATA.EXPERIMENT.tag.(DATA.tag).gap.id,  DATA.EXPERIMENT.tag.(DATA.tag).uni, uniID,topicSet), uniID);
        
        ovrID = DATA.EXPERIMENT.pattern.identifier.pm(DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.measure, DATA.EXPERIMENT.tag.(DATA.tag).ovr, DATA.poolLabels{ap}, DATA.trackID, DATA.originalTrackShortID,topicSet);
        serload(DATA.EXPERIMENT.pattern.file.pre(DATA.trackID,'unsupervised', DATA.EXPERIMENT.tag.(DATA.tag).granularity.id, DATA.EXPERIMENT.tag.(DATA.tag).aggregation.id, DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.EXPERIMENT.tag.(DATA.tag).ovr, ovrID,topicSet), ovrID);

        eval(sprintf('und(:, ap) = %1$s(:);', undID));
        eval(sprintf('uni(:, ap) = %1$s(:);', uniID));
        eval(sprintf('ovr(:, ap) = %1$s(:);', ovrID));

        clear(undID, uniID, ovrID);
        
    end;
    
    % normalize the gap in the [0, 1] range: 0 no similarity with random
    % assessor; 1 equal to the random assessor.
    und = DATA.EXPERIMENT.tag.(DATA.tag).gap.normalization(und, 1, DATA.R);
    uni = DATA.EXPERIMENT.tag.(DATA.tag).gap.normalization(uni, 1, DATA.R);
    ovr = DATA.EXPERIMENT.tag.(DATA.tag).gap.normalization(ovr, 1, DATA.R);
    
    % the higher the gap, the closer the assessor to a random
    % one. Weights must be reversed
    und = 1 - und;
    uni = 1 - uni;
    ovr = 1 - ovr;

    % compute the weights
    A = DATA.EXPERIMENT.tag.(DATA.tag).weight.command(und, uni, ovr, DATA.T);
      
    % save the computed weights
    eval(sprintf('%1$s = array2table(A);', DATA.weightID));        
    eval(sprintf('%1$s.Properties.RowNames = DATA.topics;', DATA.weightID));        
    eval(sprintf('%1$s.Properties.VariableNames = DATA.poolLabels;', DATA.weightID));        
    [path,~]=fileparts(DATA.EXPERIMENT.pattern.file.measure(DATA.trackID, DATA.relativePath, DATA.tag, DATA.weightID,topicSet));
    if (exist (path)~=7)
        mkdir(path)
    end    
    sersave(DATA.EXPERIMENT.pattern.file.measure(DATA.trackID, DATA.relativePath, DATA.tag, DATA.weightID,topicSet), DATA.weightID(:));
end
