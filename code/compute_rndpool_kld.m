%% compute_rndpool_kld
% 
% Compute the KL-divergence on random pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_rndpool_kld(trackID, tag, granularity, aggregation,  topicSet)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|granularity|* - the granularity of the coefficients, either sgl or tpc.
% * *|aggregation|* - the aggregation of the coefficients, either msr or gp.
% * *|topicSet| - list of considered topic indexes.
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rndpool_kld(trackID, tag, granularity, aggregation,  topicSet)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that tag is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

    if iscell(tag)
        % check that tag is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % check that granularity is a non-empty string
    validateattributes(granularity,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'granularity');
    
    if iscell(granularity)
        % check that granularity is a cell array of strings with one element
        assert(iscellstr(granularity) && numel(granularity) == 1, ...
            'MATTERS:IllegalArgument', 'Expected granularity to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    granularity = char(strtrim(granularity));
    granularity = granularity(:).';
    
    % check that aggregation is a non-empty string
    validateattributes(aggregation,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'aggregation');
    
    if iscell(aggregation)
        % check that aggregation is a cell array of strings with one element
        assert(iscellstr(aggregation) && numel(aggregation) == 1, ...
            'MATTERS:IllegalArgument', 'Expected aggregation to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    aggregation = char(strtrim(aggregation));
    aggregation = aggregation(:).';
        
    % setup common parameters
    common_parameters;          
    
    % Log file
    delete(EXPERIMENT.pattern.logFile.computePoolMeasuresGap('kld', tag, trackID,topicSet));
    diary(EXPERIMENT.pattern.logFile.computePoolMeasuresGap('kld', tag, trackID,topicSet));
    
    
    %% start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Computing KL divergence to %s %s %s on collection %s (%s) ########\n\n', granularity, aggregation, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - granularity %s\n', granularity);
    fprintf('  - aggregation %s\n\n', aggregation);

    
    % local version of the general configuration parameters
    shortTrack = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    
    originalTrackShortID = cell(1, originalTrackNumber);
    
    % for each runset
    for r = 1:originalTrackNumber        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    end
  
    
    %% load assessor labels and assessor number
    serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'poolLabels', 'P');

    
    fprintf('+ Loading PDF of assessor measures\n');
    
    % the assessor measures 
    assessorMeasures = cell(EXPERIMENT.measure.number, P, originalTrackNumber);
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
                
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            % for each assessor
            for p = 1:P
                
                pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, poolLabels{p}, ...
                    trackID,  originalTrackShortID{r},topicSet);
                                
                serload2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised',granularity, aggregation, 'pdf',  EXPERIMENT.tag.base.id, pdfID,topicSet),'FileVarNames',{pdfID}, ...
                    'WorkspaceVarNames',{'measure'});
                assessorMeasures{m, p, r}=measure;
                
                clear measure;
                
            end % for assessor
        end % for measure
    end % for runset
    
    clear r m p;
            
    
    fprintf('\n+ Loading PDF of %s measures\n', tag);
    
    % average random measures before further processing
    if strcmpi(aggregation, 'msr')
        % the random measures
        randomMeasures = cell(EXPERIMENT.measure.number, originalTrackNumber);
    else    
        % the random measures
        randomMeasures = cell(EXPERIMENT.measure.number, EXPERIMENT.analysis.rndPoolsSamples, originalTrackNumber);    
    end
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        fprintf('    ');

        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            % average random measures before further processing
            if strcmpi(aggregation, 'msr')
                  
                    pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, tag, aggregation, trackID, originalTrackShortID{r},topicSet);
                    
                    serload2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised',granularity, aggregation, 'pdf',  tag, pdfID,topicSet),'FileVarNames',{pdfID}, ...
                    'WorkspaceVarNames',{'measure'});
                    randomMeasures{m, r} = measure;
                    
                    clear measure;
    
            else
                
                % for each random pool
                for p = 1:EXPERIMENT.analysis.rndPoolsSamples
                    
                    sampledPool = EXPERIMENT.pattern.identifier.sampledRndPool(p);
                    
                    pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, tag, sampledPool, trackID, originalTrackShortID{r},topicSet);
                    
                    serload2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised',granularity, aggregation, 'pdf',  tag, pdfID,topicSet),'FileVarNames',{pdfID}, ...
                    'WorkspaceVarNames',{'measure'});
                    randomMeasures{m, p, r} = measure;
                    
                    clear measure;
                    
                end % for random pool
                
            end
            
            fprintf('.');
            
        end % for measure
        
        fprintf('\n');
        
    end % for runset
    
    clear r m p;
    %%
    fprintf('\n+ Computing KL divergence with %s measures\n', tag);
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        % the total number of topics
        T = size(assessorMeasures{1, 1, r}, 1);
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * %s\n', EXPERIMENT.measure.id{m});
            fprintf('      ');
            
            % average random measures before further processing
            if strcmpi(aggregation, 'msr')
           
                % make it a cell for each topic
                if strcmpi(granularity, 'tpc')
                    
                    % the random pool measures
                    random = cell(1, T);
                    
                    % for each topic
                    for i = 1:T
                        random{i} = randomMeasures{m, r}(i, :);
                    end
                    
                else
                    % the random pool measures
                    random = randomMeasures(m, r);
                end
                
            else
                
                % make it a cell for each topic
                if strcmpi(granularity, 'tpc')
                    % the random pool measures
                    random = cell(T, EXPERIMENT.analysis.rndPoolsSamples);
                    
                    % for each topic
                    for i = 1:T
                        % for each random pool
                        for j = 1:EXPERIMENT.analysis.rndPoolsSamples
                            random{i, j} = randomMeasures{m, j, r}(i, :);
                        end
                    end
                else
                    
                    % the random pool measures
                    random = randomMeasures(m, :, r);
                end
                
            end
            
            
            % for each assessor pool
            for ap = 1:P
                
                % the assessor measures
                assessor = assessorMeasures{m, ap, r};
                
                
                % average random measures before further processing
                if strcmpi(aggregation, 'msr')
                    
                     % single gap for all the topics
                    if strcmpi(granularity, 'sgl')
                        
                    kldValues=EXPERIMENT.command.rndPools.gap.(granularity).kld(assessor, random);
                   
                    else
                        
                          % one gap for each topic
                        kldValues= NaN(1, T);
                        
                        for t = 1:T
                            
                            assessorTopic = assessor(t, :);
                            randomTopic = random(t);
                            kldValues(t)=EXPERIMENT.command.rndPools.gap.(granularity).kld(assessorTopic, randomTopic);
                            
                        end
                    end
                    
                else
                    
                    % single gap for all the topics
                    if strcmpi(granularity, 'sgl')
                        
                        kldValues=EXPERIMENT.command.rndPools.gap.(granularity).kld(assessor,random);
                        
                    else
                        
                        % one gap for each topic
                        kldValues = NaN(T, EXPERIMENT.analysis.rndPoolsSamples);
                        
                        for t = 1:T
                            
                            assessorTopic = assessor(t);
                            randomTopic = random(t, :);
                            
                            kldValues(t,:)=EXPERIMENT.command.rndPools.gap.(granularity).kld(assessorTopic,randomTopic);
                                                       
                        end
                    end
                end
                
                gapID = EXPERIMENT.pattern.identifier.pm('kld', EXPERIMENT.measure.id{m}, tag, poolLabels{ap}, trackID, originalTrackShortID{r},topicSet);
                
                [path,~]=fileparts(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, 'kld', tag, gapID,topicSet));
                if (exist (path)~=7)
                    mkdir(path);
                end

                sersave2(EXPERIMENT.pattern.file.pre(trackID,'unsupervised', granularity, aggregation, 'kld', tag, gapID,topicSet), ...
                    'WorkspaceVarNames', {'kldValues' },'FileVarNames', {gapID});
                
                fprintf('.');
                
                clear assessor gapID;
            end % for each assessor
                                                
            fprintf('\n');
            
        end % for each measure
        
    end % for runset
    
    fprintf('\n\n######## Total elapsed time for computing KL divergence to %s %s %s on collection %s (%s): %s ########\n\n', ...
            granularity, aggregation, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end
