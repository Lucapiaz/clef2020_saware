%% plot_base_analysis
%
%  generates the plots relative to the base measures 
%
%% References
% 
% Please refer to:
%
% * M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.
%
%% Information
% 
% * *Authors*: <mailto:piazzonl@dei.unipd.it Luca Piazzon>,<mailto:ferro@dei.unipd.it Nicola Ferro>  
% * *Version*: 1.0
% * *Since*: 1.0
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2020 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}
common_parameters;
trackID=EXPERIMENT.fast.trackID;
relativePath=EXPERIMENT.tag.base.relativePath;
shortTrack=EXPERIMENT.(trackID).shortID;
tag=EXPERIMENT.tag.base.id;
serload(EXPERIMENT.pattern.file.topicset(trackID,shortTrack));
ntopicset=EXPERIMENT.topicset.number;
serload(EXPERIMENT.pattern.file.dataset(trackID, shortTrack), 'poolLabels', 'poolIdentifiers', 'T', 'P');

rmse=NaN(EXPERIMENT.measure.number,EXPERIMENT.(trackID).collection.runSet.originalTrackID.number,P);
apc=NaN(EXPERIMENT.measure.number,EXPERIMENT.(trackID).collection.runSet.originalTrackID.number,P);
 for m = 1:EXPERIMENT.measure.number
        mid=EXPERIMENT.measure.id{m};
        for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
            shortRunset=EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};   
            measureIDtot = EXPERIMENT.pattern.identifier.measure(mid, tag,tag , trackID,  shortRunset,[]);
            apcIDtot = EXPERIMENT.pattern.identifier.pm('apc', mid, tag, tag, trackID,  shortRunset,[]);              
            rmseIDtot = EXPERIMENT.pattern.identifier.pm('rmse', mid, tag, tag, trackID,  shortRunset,[]);
            serload2(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.base.relativePath, tag, measureIDtot,[]),'FileVarNames',{apcIDtot,rmseIDtot},'WorkspaceVarNames',{'apct','rmset'});
            rmse(m,r,:)=rmset.data;
            apc(m,r,:)=apct.data;
        end
 end

%rmse normalization
%maxRmse=max(rmse,[],3);
%rmse(1,1,:) = rmse(1,1,:) ./ repmat(maxRmse(1,1),1,1,31);
%rmse(1,2,:) = rmse(1,2,:) ./ repmat(maxRmse(1,2),1,1,31);
%rmse(2,1,:) = rmse(2,1,:) ./ repmat(maxRmse(2,1),1,1,31);
%rmse(2,2,:) = rmse(2,2,:) ./ repmat(maxRmse(2,2),1,1,31);

apc=permute(apc,[3 1 2]); %assessor measure system
            
rmse=permute(rmse,[3 1 2]);

ms=(permute(mean(apc,1),[2,3,1])); %mean over assessors -->measurexsystem
as=(permute(mean(apc,2),[1,3,2])); %mean over measure-->assessorxsystem
am=(mean(apc,3));%mean over system --> assessorxmeasure



colors=[winter(10); lines(10); cool(11)];
currentFigure = figure('Visible', 'off');
    plotData = as;
    hold on;
        % for each label
        for l = 1:P
            plot(1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number, plotData(l, :), 'Color', colors(l, :), 'LineWidth', 0.9);                
        end
        
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:2;
        ax1.XTickLabel = ['T08';'T13'];
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'System';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
        ax1.XGrid = 'on';
        
        title('Approach*Runset', 'FontSize', 15, 'Interpreter', 'tex');
        legend(poolLabels, 'Location', 'BestOutside', 'Interpreter', 'none'); 
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure(trackID, 'base', 'apc_as',[]));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 30];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure(trackID, 'base', 'apc-as',[]));
        
        close(currentFigure)    
    
        clear plotData;
       
    
currentFigure = figure('Visible', 'off');
    plotData = am;
    hold on;
        % for each label
        for l = 1:P
            plot(1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number, plotData(l, :), 'Color', colors(l, :), 'LineWidth', 0.9);                
        end 

            
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 15;
        ax2.XTick = 1:EXPERIMENT.measure.number;
        ax2.XTickLabel = [' AP ';'nDCG'];
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Measure';
        ax2.YLabel.Interpreter = 'tex';
        ax2.YLabel.String = sprintf('AP Correlation Marginal Mean');
        ax2.XGrid = 'on';
        
        title('Approach*Measure Interaction', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(poolLabels, 'Location', 'BestOutside', 'Interpreter', 'none'); 
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure(trackID, 'base', 'apc_am',[]));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 30];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure(trackID, 'base', 'apc_am',[]));
        
        close(currentFigure)    
    
        clear plotData;
 

 currentFigure = figure('Visible', 'off');

        xTick = 1:P;
        plotData = mean(am,2);       
        [sorted,I]=sort(plotData);
        a=colormap([rgb('Black');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Red');rgb('Red');rgb('Red');rgb('LightSeaGreen');rgb('LightSeaGreen');rgb('LightSeaGreen');rgb('LightSeaGreen')]);
        for i=1:P;
            bar(i, sorted(i),'facecolor', a(I(i),:),'BarWidth', 0.7);
            axis([0 P+1 min(sorted)-0.1  max(sorted)+0.1])
            hold on
        end 
        
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = poolLabels(I);
        ax.XTickLabelRotation = 90;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Approach';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('AP Correlation Marginal Mean');
        

        [path,~]=fileparts(EXPERIMENT.pattern.file.figure(trackID, 'base', 'apc_a',[]));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 30 20];
        print(currentFigure, '-dpng','-r300', EXPERIMENT.pattern.file.figure(trackID, 'base', 'apc_a',[]));
        
        close(currentFigure)
        
        clear plotData;      
   
ms=(permute(mean(rmse,1),[2,3,1])); %mean over assessors -->measurexsystem
as=(permute(mean(rmse,2),[1,3,2])); %mean over measure-->assessorxsystem
am=(mean(rmse,3));%mean over system --> assessorxmeasure       
        
currentFigure = figure('Visible', 'off');
    plotData = as;
    hold on;
        % for each label
        for l = 1:P
            plot(1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number, plotData(l, :), 'Color', colors(l, :), 'LineWidth', 0.9);                
        end
        
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 15;
        ax1.XTick = 1:2;
        ax1.XTickLabel = ['T08';'T13'];
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'System';
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('RMSE Marginal Mean');
        ax1.XGrid = 'on';
        
        title('Approach*Runset', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(poolLabels, 'Location', 'BestOutside', 'Interpreter', 'none');
        
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure(trackID, 'base', 'rmse_as',[]));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 30];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure(trackID, 'base', 'rmse-as',[]));
        
        close(currentFigure)    
    
        clear plotData;
       
    
currentFigure = figure('Visible', 'off');
    plotData = am;
    hold on;
        % for each label
        for l = 1:P
            plot(1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number, plotData(l, :), 'Color', colors(l, :), 'LineWidth', 0.9);                
        end 
        
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 15;
        ax2.XTick = 1:EXPERIMENT.measure.number;
        ax2.XTickLabel = [' AP ';'nDCG'];

        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Measure';
        ax2.YLabel.Interpreter = 'tex';
        ax2.YLabel.String = sprintf('RMSE Marginal Mean');
        ax2.XGrid = 'on';
        
        title('Approach*Measure Interaction', 'FontSize', 15, 'Interpreter', 'tex');
        
        legend(poolLabels, 'Location', 'BestOutside', 'Interpreter', 'none'); 
        [path,~]=fileparts(EXPERIMENT.pattern.file.figure(trackID, 'base', 'rmse_am',[]));
        if (exist (path)~=7)
            mkdir(path)
        end
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 20 30];
        print(currentFigure, '-dpng', EXPERIMENT.pattern.file.figure(trackID, 'base', 'rmse_am',[]));
        
        close(currentFigure)    
    
        clear plotData;
        
currentFigure = figure('Visible', 'off');

        xTick = 1:P;
        plotData = mean(am,2);       
        [sorted,I]=sort(plotData);
        a=colormap([rgb('Black');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Blue');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Orange');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Green');rgb('Red');rgb('Red');rgb('Red');rgb('LightSeaGreen');rgb('LightSeaGreen');rgb('LightSeaGreen');rgb('LightSeaGreen')]);
        for i=1:P;
            bar(i, sorted(i),'facecolor', a(I(i),:),'BarWidth', 0.7);
            axis([0 P+1 min(sorted)-0.1  max(sorted)+0.1])
            hold on
        end
        
        ax = gca;
        ax.TickLabelInterpreter = 'none';
        ax.FontSize = 15;
        ax.XTick = xTick;
        ax.XTickLabel = poolLabels(I);
        ax.XTickLabelRotation = 90;
        ax.XLabel.Interpreter = 'tex';
        ax.XLabel.String = 'Approach';
        ax.XGrid = 'on';
        ax.YLabel.Interpreter = 'tex';
        ax.YLabel.String = sprintf('RMSE Marginal Mean');
        

        [path,~]=fileparts(EXPERIMENT.pattern.file.figure(trackID, 'base', 'rmse_a',[]));
        if (exist (path)~=7)
            mkdir(path)
        end
        
        fig=gcf;
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = [0 0 30 20];
        print(currentFigure, '-dpng','-r300', EXPERIMENT.pattern.file.figure(trackID, 'base', 'rmse_a',[]));
        
        close(currentFigure)
        
        clear;      