This is the Matlab code for running the experiments described in the paper

M. Ferrante, N. Ferro, L. Piazzon (2020). s-AWARE: supervised measure-based methods for crowd-assessors combination.

This code is based on the MATTERS Matlab library available at: http://matters.dei.unipd.it/